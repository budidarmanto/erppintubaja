<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockMovementCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stock_movement_comment', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('stock_movement', 25);
          $table->text('comment');
          $table->string('attachment', 200)->nullable();
          $table->boolean('active')->default(true);

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('stock_movement')
                ->references('id')->on('inventory_stock_movement')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stock_movement_comment');
    }
}
