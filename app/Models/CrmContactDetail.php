<?php

namespace App\Models;

use Devplus\Model\DevplusModel;

class CrmContactDetail extends DevplusModel
{
    protected $table = 'crm_contact_detail';
    protected $fillable = ['id', 'contact', 'type', 'name', 'position','address', 'city', 'postal_code', 'state', 'country', 'website', 'phone', 'mobile', 'fax', 'email'];

    public function application()
    {
      return $this->belongsTo('App\Models\CrmContact', 'contact');
    }
}
