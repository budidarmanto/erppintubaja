/* Setup blank page controller */
angular.module('DevplusApp').controller('FormUomController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', function($rootScope, $scope, settings, response, $http, $location, $uibModal) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataUom = response.dataUom;

    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    $scope.showPopupUom = function(dataUom){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalUom.html',
        controller: 'ModalUomCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataUom || {}),
              optionUomType: $scope.response.optionUomType
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataUom){
          $scope.dataUom[$scope.dataUom.indexOf(dataUom)] = data;
        }else{
          $scope.dataUom.push(data);
        }

      });
    }

    $scope.deleteUom = function(uom){
      $scope.dataUom.splice($scope.dataUom.indexOf(uom), 1);
    }

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.dataUom = $scope.dataUom;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
                return;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataUom = resp.data.dataUom;

              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);

angular.module('DevplusApp').controller('ModalUomCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionUomType = params.optionUomType;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'code', label: 'Kode', type: 'text', attributes: {required: '', maxlength: 100}},
    {name: 'name', label: 'Nama', type: 'text', attributes: {required: '', maxlength: 100}},
    {name: 'type', label: 'Tipe', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionUomType'}},
    {name: 'ratio', label: 'Rasio', type: 'number', attributes: {required: ''}},
    {name: 'active', label: 'Aktif', type: 'switch'},
  ];


  $scope.save = function(){
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
