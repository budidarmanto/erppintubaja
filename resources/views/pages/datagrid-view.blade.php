<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-filter')
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              @include('pages.partials.datagrid.datagrid-table-view')
              <div class="row margin-top-10">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
