<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

use App\Models\ProductMaster;
use App\Models\ProductSupplier;
use App\Models\ProductAlternative;
use App\Models\ProductUom;
use App\Models\ProductAttributeValue;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use Carbon\Carbon;

class ProductProduct extends DevplusModel
{
  protected $table = 'product_product';
  protected $fillable = ['id', 'product', 'attribute', 'price', 'active'];

  public function product()
  {
      return ProductMaster::find($this->product);
  }

  public static function getQuery($modifierFn = null, $onlyActiveProduct = true){

    $product = ProductProduct::select([
      'product_master.name AS name',
      'product_master.code AS code',
      'product_master.category AS category',
      'product_master.uom AS uom',
      'product_master.revision AS revision',
      'product_uom.uom_category AS uom_category',
      'product_master.type AS type',
      'product_master.barcode AS barcode',
      'product_master.picture AS picture',
      'product_master.sale_price AS sale_price',
      'product_master.cost_price AS cost_price',
      'product_master.pos AS pos',
      'product_master.tax AS tax',
      'product_master.discount AS discount',
      'product_master.active AS active',
      'product_product.id',
      'product_master.id AS product_id',
      'product_product.attribute',
      'product_master.updated_at',
    ])
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'product_master.uom');

    if (is_a($modifierFn, '\Closure')) {
      $product = call_user_func_array($modifierFn, [$product]);
    }
    if($onlyActiveProduct){
      $product->where([
        'product_master.active' => true,
      ]);
    }

    return $product;
  }

  public static function renderProductAttribute($dataProduct){
    $attributeValue = ProductAttributeValue::all()->toArray();
    $attributeValue = collect($attributeValue)->pluck('name', 'id')->toArray();

    if(!empty($dataProduct)){
      foreach($dataProduct as $key => $val){
        $attrJson = json_decode($val['attribute'], true);
        $attr = collect($attrJson)->pluck('attribute_value')->map(function($value, $key) use ($attributeValue){
          return (isset($attributeValue[$value])) ? $attributeValue[$value] : '';
        })->implode(', ');
        $attr = (!empty($attr)) ? ' ('.$attr.')' : '';

        $dataProduct[$key]['attribute'] = $attrJson;
        $dataProduct[$key]['name'] = $val['name'].$attr;
      }
    }
    return $dataProduct;
  }

  public static function getData($modifierFn = null, $onlyActiveProduct = true){
    $product = self::getQuery($modifierFn, $onlyActiveProduct);
    $dataProduct = $product->get()->toArray();
    return self::renderProductAttribute($dataProduct);
  }

  public static function getRelatedData($dataProduct, $warehouse = null){

    // Get Data ALL UOM Group BY UOM Category
    $dataUom = [];
    $uom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    if(count($uom) > 0){
      foreach($uom as $val){
        $dataUom[$val['uom_category']][] = $val->toArray();
      }
    }

    if(!empty($dataProduct)){
      foreach($dataProduct as $key => $val){
        // Basic Unit

        // Get Option Supplier
        $optSupplier = [];
        $supplier = ProductSupplier::select([
          'product_supplier.supplier',
          'product_supplier.price',
          'product_supplier.discount',
          'product_supplier.tax',
          'crm_contact.name',
          'crm_contact.pkp',
        ])
        ->join('crm_contact', 'crm_contact.id', '=', 'product_supplier.supplier')
        ->where('product', $val['product_id'])->orderBy('product_supplier.price', 'ASC')->get();
        if(count($supplier) > 0){
          foreach($supplier as $vsupplier){
            $optSupplier[] = [
              'id' => $vsupplier['supplier'],
              'text' => $vsupplier['name'],
              'price' => $vsupplier['price'],
              'pkp' => $vsupplier['pkp'],
              'discount' => $vsupplier['discount'],
              'tax' => $vsupplier['tax'],
            ];
          }
        }
        // End Get Option Supplier

        // Get Option Related Product
        $alternative = [];
        $productAlternative = ProductAlternative::where('product_master', $val['product_id'])->get();
        if(count($productAlternative) > 0){
          $alternative = $productAlternative->pluck('product')->toArray();
        }
        // End Get Option Related Product

        $filterUom = (isset($dataUom[$val['uom_category']])) ? $dataUom[$val['uom_category']] : [];
        $optionUom = [];
        if(!empty($filterUom)){
          foreach($filterUom as $vuom){
            $optionUom[] = [
              'id' => $vuom['id'],
              'text' => $vuom['name'],
              'code' => $vuom['code'],
              'ratio' => $vuom['ratio'],
              'type' => $vuom['type'],
            ];
          }
        }
        $dataProduct[$key]['optionUom'] = $optionUom;
        $dataProduct[$key]['optionSupplier'] = $optSupplier;
        $dataProduct[$key]['alternative'] = $alternative;

        // Get Data Stock
        if(!empty($warehouse)){
          $dataWarehouseLocation = InventoryWarehouseLocation::getOption();
          $stock = ProductProduct::getDataStock($val['id'], $warehouse, $dataWarehouseLocation, $uom);

          $dataProduct[$key]['forecast'] = $stock['forecast'];
          $dataProduct[$key]['stock'] = $stock['stock'];
          $dataProduct[$key]['reserved'] = $stock['reserved'];
          $dataProduct[$key]['dataForecast'] = $stock['dataForecast'];
          $dataProduct[$key]['dataStock'] = $stock['dataStock'];
          $dataProduct[$key]['dataReserved'] = $stock['dataReserved'];

          $basicUnit = collect($dataUom[$val['uom_category']])->where('type', 'd')->first();
          if(!empty($basicUnit)){
            $dataProduct[$key]['basic_unit'] = $basicUnit['name'];
          }
        }
      }
    }

    return $dataProduct;
  }

  public static function getDataStock($productId, $warehouse = null, $dataWarehouseLocation = null, $dataUom = null){
    $warehouseId = (!empty($warehouse)) ? $warehouse->id : null;
    if(empty($dataWarehouseLocation)){
        $dataWarehouseLocation = InventoryWarehouseLocation::getOption();
    }

    if(empty($dataUom)){
        $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    }

    $basicUnit = collect($dataUom)->where('type', 'd')->first()->toArray();


    $purchase = 0;
    $purchaseReceipt = 0;
    $sales = 0;
    $salesDelivered = 0;
    $outstandingReceipt = 0;
    $outstandingDelivery = 0;
    $receipt = 0;
    $delivered = 0;
    $stock = 0;
    $externalTransfer = 0;
    $chasier = 0;
    $adjustment = 0;

    $dataForecast = [];
    $dataStock = [];
    $dataReserved = [];

    $today = date('Y-m-d');
    // $yesterday = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));
    // $today = $yesterday;

    // $stockMovement = ProductProduct::getDataStockMovement($productId, ['PO', 'RC', 'SO', 'DO', 'AD', 'ET', 'IT', 'TF', 'CH', 'RT']);

    $stockMovement = ProductProduct::getDataStockMovement($productId, ['PO', 'RC', 'SO', 'DO', 'AD', 'ET', 'IT', 'TF', 'CH', 'RT'], function($q) use($today){
      return $q->whereDate('inventory_stock_movement.date', $today);
    });
// dd($stockMovement->toArray());

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->where('inventory_stock.product', $productId)
    ->whereDate('inventory_stock.date', $today);
    if(!empty($warehouseId)){
      $inventoryStock->where('inventory_warehouse_location.warehouse', $warehouseId);
    }
    $inventoryStock = $inventoryStock->get();
    // dd($dataUom);
    // dd($basicUnit);
    // dd($dataWarehouseLocation);

    foreach($dataWarehouseLocation as $val){
      $dataStock[$val['id']] = [
          'qty' => 0,
          'uom' => $basicUnit['id'],
          'uom_name' => $basicUnit['name'],
          'warehouse_location' => $val['id'],
          'warehouse_location_name' => $val['text']
      ];
    }

    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        $stock += $val->begin_qty;

        if(isset($dataStock[$val->warehouse_location])){
          $dataStock[$val->warehouse_location]['qty'] += $val->begin_qty;
        }else{
          $dataStock[$val->warehouse_location]['qty'] = $val->begin_qty;
        }

      }
    }

    if(!empty($stockMovement)){

      foreach($stockMovement as $val){

        if(!empty($warehouseId)){
          $wh_id = InventoryStockMovement::getWarehouseTransaction($val, $dataWarehouseLocation);
          if(empty($wh_id) || $wh_id != $warehouseId){
            continue;
          }
        }

        $type = $val['type'];
        $status = $val['status'];
        // Convert Quantity to Basic Unit
        $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
        $qty = $val['qty'] * $currentUom['ratio'];
        $rcp_qty = $val['rcp_qty'] * $currentUom['ratio'];
        $do_qty = $val['do_qty'] * $currentUom['ratio'];
        // End Convert Quantity

        $statusIncoming = ['3PO', '5PR'];
        $statusPurchase = ['3PO'];
        $statusReceipt = ['3RA'];
        $statusCancelReceipt = ['1RR'];

        // Barang Dibeli yang masih outstanding
        $isPurchase = ($type == 'PO' && in_array($status, $statusIncoming)) ? true : false;
        // Barang Dijual yang masih outstanding
        $isSales = ($type == 'SO' && $status == '3SO') ? true : false;
        // Barang yang sudah diterima
        $isReceipt = ($type == 'RC' && in_array($status, $statusReceipt)) ? true : false;
        // Barang yang sudah dikirim
        $isDelivered = ($type == 'DO' && $status == '4DD') ? true : false;
        // Barang Dieterima yang masih outstanding
        $isOutstandingReceipt = ($type == 'RC' && in_array($status, ['1RR'])) ? true : false;
        // Barang Dikirim yang masih outstanding
        $isOutstandingDelivery = ($type == 'DO' && in_array($status, ['1DR', '3DO'])) ? true : false;
        // Barang Penyesuaian
        $isAdjustment = ($type == 'AD' && $status == '3AA') ?  true : false;
        // Barang Mutasi Eksternal
        $isExternalTransfer = ($type == 'ET' && $status == '3ED') ?  true : false;
        // Barang Mutasi Internal
        $isInternalTransfer = ($type == 'IT' && $status == '3IT') ?  true : false;
        // Barang Transfer
        $isTransfer = ($type == 'TF' && in_array($status, ['1TO', '2TR'])) ? true : false;

        $isChasier = ($type == 'CH' && $status == '1CS') ? true : false;

        $purchaseQty = $val['qty'] - $val['rcp_qty'];

        // Purchase
        if(($isPurchase || $isTransfer) && $purchaseQty > 0){
          $purchase += $qty;
          $purchaseReceipt += $rcp_qty;
          $dataForecast[] = [
            'expected_date' => Carbon::parse($val['product_expected_date'])->format('d/m/Y'),
            'stock_movement' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => ($isTransfer) ? $val['company_source'] : $val['contact'],
            'qty' => $purchaseQty,
            'uom' => $currentUom['name'],
          ];
        }

        $salesQty = $val['qty'] - $val['do_qty'];

        // Sales
        if(($isSales || $isExternalTransfer) && $salesQty > 0){
          $sales += $qty;
          $salesDelivered += $do_qty;
          $dataReserved[] = [
            'expected_date' => Carbon::parse($val['expected_date'])->format('d/m/Y'),
            'stock_movement' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => $val['contact'],
            'qty' => $salesQty,
            'uom' => $currentUom['name'],
          ];
        }

        // Outstanding Receipt
        if($isOutstandingReceipt){
          $outstandingReceipt += $qty;
          $dataRef = collect($stockMovement)->where('stock_movement', $val['reference'])->first();

          if(empty($dataRef)){
            $dataRef = InventoryStockMovementProduct::select([
              'inventory_stock_movement.id AS stock_movement',
              'inventory_stock_movement.code',
              'inventory_stock_movement_product.expected_date AS product_expected_date',
              'crm_contact.name AS contact',
            ])
            ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
            ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
            ->where('inventory_stock_movement.id', $val['reference'])->first();
          }

          $dataForecast[] = [
            'expected_date' => (!empty($dataRef)) ? Carbon::parse($dataRef->product_expected_date)->format('d/m/Y') : '-',
            'stock_movement' => (!empty($dataRef)) ? $dataRef->code : '-',
            'receipt_code' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => (!empty($dataRef)) ? $dataRef->contact : '-',
            'qty' => $qty,
            'uom' => $currentUom['name'],
          ];
        }

        // Outstanding Delivery
        if($isOutstandingDelivery){
          $outstandingDelivery += $qty;
          $dataStock[$val['source']]['qty'] -= $qty;
          $dataRef = collect($stockMovement)->where('stock_movement', $val['reference'])->first();
          if(empty($dataRef)){
            InventoryStockMovement::select([
              'inventory_stock_movement.expected_date',
              'inventory_stock_movement.code',
              'crm_contact.name AS contact',
            ])->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
            ->where('inventory_stock_movement.id', $val['reference'])->first();
          }

          $dataReserved[] = [
            'expected_date' => (!empty($dataRef)) ? Carbon::parse($dataRef->expected_date)->format('d/m/Y') : '-',
            'stock_movement' => (!empty($dataRef)) ? $dataRef->code : '-',
            'delivery_code' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => (!empty($dataRef)) ? $dataRef->contact : '-',
            'qty' => $qty,
            'uom' => $currentUom['name'],
          ];
        }

        $currentDate = Carbon::parse($val['date'])->format('Y-m-d');
        if($currentDate == $today){
          if($isReceipt){
            $receipt += $qty;
            if(isset($dataStock[$val['destination']]))
              $dataStock[$val['destination']]['qty'] += $qty;
          }

          if($isDelivered){
            $delivered += $qty;
            if(isset($dataStock[$val['source']]))
              $dataStock[$val['source']]['qty'] -= $qty;
          }

          if($isAdjustment){
            $adjustment += $qty;
            $dataStock[$val['source']]['qty'] += $qty;
          }

          if($isInternalTransfer){
            $dataStock[$val['source']]['qty'] -= $qty;
            $dataStock[$val['destination']]['qty'] += $qty;
          }

          if($isChasier){
            $chasier += $qty;
            $dataStock[$val['source']]['qty'] -= $qty;
          }
        }
      }
    }


    $forecast = $purchase - $purchaseReceipt + $outstandingReceipt;

    $reserved = $sales - $salesDelivered + $outstandingDelivery;

    $inventory = $stock + $receipt - $delivered + $adjustment - $chasier - $reserved;// - $chasier;

    $data = [
      'forecast' => $forecast,
      'stock' => $inventory,
      'reserved' => $reserved,
      'dataForecast' => $dataForecast,
      'dataReserved' => $dataReserved,
      'dataStock' => $dataStock
    ];

    return $data;
  }

  public static function getWarehouseLocationStock($listProductId = [], $warehouseLocation = null){
    $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    $today = date('Y-m-d');
    $data = [];

    $products = ProductProduct::getQuery(function($q) use ($listProductId){
      return $q->whereIn('product_product.id', $listProductId);
    })->get();

    if(count($products) > 0){

      foreach($products as $product){
        $productId = $product->id;
        $stock = 0;
        $receipt = 0;
        $delivered = 0;
        $adjustment = 0;
        $chasier = 0;
        $minInternalTransfer = 0;
        $plusInternalTransfer = 0;

        $inventoryStock = InventoryStock::select([
          'inventory_stock.product',
          'inventory_stock.uom',
          'inventory_stock.warehouse_location',
          'inventory_stock.begin_qty',
          'inventory_stock.plus_qty',
          'inventory_stock.min_qty',
          'inventory_stock.end_qty'
        ])
        ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
        ->where('inventory_stock.product', $productId)
        ->where('inventory_stock.warehouse_location', $warehouseLocation)
        ->whereDate('inventory_stock.date', $today);

        $inventoryStock = $inventoryStock->get();

        if(count($inventoryStock) > 0){
          foreach($inventoryStock as $val){
            $stock += $val->begin_qty;
          }
        }

        $stockMovement = ProductProduct::getDataStockMovement($productId, ['RC', 'DO', 'AD', 'IT', 'CH'], function($q) use($today){
          return $q->whereDate('inventory_stock_movement.date', $today);
        });
        if(!empty($stockMovement)){
          foreach($stockMovement as $val){

            $type = $val['type'];
            $status = $val['status'];
            // Convert Quantity to Basic Unit
            $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
            $qty = $val['qty'] * $currentUom['ratio'];
            $rcp_qty = $val['rcp_qty'] * $currentUom['ratio'];
            $do_qty = $val['do_qty'] * $currentUom['ratio'];


            $isReceipt = ($type == 'RC' && in_array($status, ['3RA'])) ? true : false;
            $isDelivered = ($type == 'DO' && in_array($status, ['4DD', '1DR', '3DO'])) ? true : false;
            $isAdjustment = ($type == 'AD' && $status == '3AA') ?  true : false;
            $isInternalTransfer = ($type == 'IT' && $status == '3IT') ?  true : false;
            $isChasier = ($type == 'CH' && $status == '1CS') ? true : false;

            if($isReceipt && $val['destination'] == $warehouseLocation){
              $receipt += $qty;
            }

            if($isDelivered && $val['source'] == $warehouseLocation){
              $delivered += $qty;
            }

            if($isAdjustment && $val['source'] == $warehouseLocation){
              $adjustment += $qty;
            }

            if($isInternalTransfer){
              if($val['source'] == $warehouseLocation){
                $minInternalTransfer += $qty;
              }
              if($val['destination'] == $warehouseLocation){
                $plusInternalTransfer += $qty;
              }
            }

            if($isChasier && $val['source'] == $warehouseLocation){
              $chasier += $qty;
            }

          }
        }

        $stock = $stock + $receipt - $delivered + $adjustment - $chasier - $minInternalTransfer + $plusInternalTransfer;
        $currentUom = collect($dataUom)->where('id', $product->uom)->first();
        $defaultUom = collect($dataUom)->where('uom_category', $currentUom['uom_category'])->where('type', 'd')->first();

        $data[] = [
          'product' => $productId,
          'stock' => $stock,
          'uom' => $defaultUom['id'],
          'uom_name' => $defaultUom['name']
        ];
      }
    }

    return $data;

  }


  public static function getDataStockMovement($productId, $type = [], $query = null){

    $stockMovement = InventoryStockMovementProduct::select([
      'inventory_stock_movement.id AS stock_movement',
      'inventory_stock_movement.code',
      'inventory_stock_movement.company',
      'inventory_stock_movement.source',
      'inventory_stock_movement.destination',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      'inventory_stock_movement.status',
      'inventory_stock_movement.expected_date',
      'inventory_stock_movement.reference',
      'crm_contact.name AS contact',
      'inventory_stock_movement_product.expected_date AS product_expected_date',
      'inventory_stock_movement_product.id',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement_product.qty',
      'inventory_stock_movement_product.rcp_qty',
      'inventory_stock_movement_product.do_qty',
      'inventory_stock_movement_product.status AS statusprod',
      'setting_company.name AS company_source'
    ])->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->leftJoin('setting_company', 'setting_company.id', '=', 'inventory_stock_movement.company_destination')
    ->where([
      'inventory_stock_movement_product.product' => $productId,
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    // ->whereDate('inventory_stock_movement.date', $today)
    ->whereIn('inventory_stock_movement.type', $type)
    ->orderBy('inventory_stock_movement.created_at', 'ASC');
    if (is_a($query, '\Closure')) {
      $stockMovement = call_user_func_array($query, [$stockMovement]);
    }
    $stockMovement = $stockMovement->get();

    return $stockMovement;
  }



}
