<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_warehouse', function (Blueprint $table) {
          $table->string('id',25);
          $table->string('code', 50);
          $table->string('name', 100);
          $table->text('address')->nullable();
          $table->string('city', 50)->nullable();
          $table->string('postal_code', 5)->nullable();
          $table->string('state', 50)->nullable();
          $table->string('country', 50)->nullable();
          $table->string('phone', 50)->nullable();
          $table->string('fax', 50)->nullable();
          $table->json('company')->nullable();
          $table->boolean('active')->default(true);

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_warehouse');
    }
}
