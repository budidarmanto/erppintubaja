<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class ProductDetail extends Controller
{

  public function index($id) {

    $query = ProductProduct::getQuery();
    $query->where('product_product.id', $id);
    $dataProduct = $query->get()->toArray();
    $data = ProductProduct::renderProductAttribute($dataProduct);
    // $data = ProductProduct::getRelatedData($data);
    $data = collect($data)->first();
// dd($data);
    $xml = '<page>';
    if(!empty($data)){
      $productCode = htmlspecialchars($data['code'], ENT_XML1, 'UTF-8');
      $productName = htmlspecialchars($data['name'], ENT_XML1, 'UTF-8');
      $image = (empty($data['picture'])) ? '' : Request::root().'/img/product/'.$data['picture'];

      $xml .=  '<product>
                  <id>'.$data['id'].'</id>
                  <code>'.$productCode.'</code>
                  <name>'.$productName.'</name>
                  <image>'.$image.'</image>
                  <barcode>'.$data['barcode'].'</barcode>
                  <description/>
                  <price>'.$data['sale_price'].'</price>
                </product>';
    }
    $xml .= '</page>';


    header('Content-type: text/xml');
    echo $xml;
    exit;
  }

  public function json($id) {

    $query = ProductProduct::getQuery();
    $query->where('product_product.id', $id);
    $dataProduct = $query->get()->toArray();
    $data = ProductProduct::renderProductAttribute($dataProduct);
    // $data = ProductProduct::getRelatedData($data);
    $data = collect($data)->first();
	//dd($data);
    $responseArray = [];
    if(!empty($data)){
      $productCode = htmlspecialchars($data['code'], ENT_XML1, 'UTF-8');
      $productName = htmlspecialchars($data['name'], ENT_XML1, 'UTF-8');
      $image = (empty($data['picture'])) ? '' : Request::root().'/img/product/'.$data['picture'];

      $responseArray[] = [
        'id' => $data['id'],
        'code' => $productCode,
        'name' => $productName,
        'image' => $image,
        'barcode' => $data['barcode'],
        'price' => $data['sale_price']
      ];
    }
	$responseArrayData =[];
	$responseArrayData['data'] = $responseArray;

    return response()->json($responseArrayData);
  }
}
