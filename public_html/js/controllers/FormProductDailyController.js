/* Setup blank page controller */
angular.module('DevplusApp').controller('FormProductDailyController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', function($rootScope, $scope, settings, response, $http, $location) {

    $scope.response = response;
    // console.log(response);
    $scope.optionCategory = response.category;
    $scope.optionCompany = response.company;
    $scope.data = {
      company: '',
      category: '',
      datefrom: '',
      dateto: ''
    };

    $scope.response.action = '';
    $scope.response.fields = [
      {name: 'company', label: 'Kantor/Cabang', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionCompany', required: ''}},
      {name: 'category', label: 'Kategori', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionCategory'}},
      {name: 'datefrom', label: 'Dari Tanggal', type: 'date', attributes: {required: ''}},
      {name: 'dateto', label: 'Sampai Tanggal', type: 'date', attributes: {required: ''}},
    ];
    // $scope.data = response.data.length == 0 ? {} : response.data;
    // if(response.action == ''){
    //   window.location.hash = 'no-access';
    // }


    $scope.download = function($event){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        window.location = 'report/productdaily/download'+$scope.queryString();
      }
    }
    $scope.queryString = function(){

      var params = [];
      angular.forEach($scope.data, function(value, key){
        params.push(key+'='+value);
      });
      params = params.join('&');

      return '?'+params;

    }


    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);
