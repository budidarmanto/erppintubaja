<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-4">
              <div class="text-center margin-bottom-20">
                <form-builder field="picture"></form-builder>
              </div>
              <div style="padding-left: 60px;">
                <form-builder field="active"></form-builder>
                <form-builder field="pkp"></form-builder>
              </div>
            </div>
            <div class="col-md-8">
              <form-builder field="name"></form-builder>
              <form-builder field="type"></form-builder>
              <form-builder field="address"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="city"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="postal_code"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="state"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="country"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <form-builder field="phone"></form-builder>
                </div>
                <div class="col-md-4">
                  <form-builder field="mobile"></form-builder>
                </div>
                <div class="col-md-4">
                  <form-builder field="fax"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="email"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="website"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="tax_no"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="term_of_payment"></form-builder>
                </div>
              </div>
            </div>
          </div>


          <!-- CONTACT DETAIL -->
		  <!--
          <div class="portlet" style="margin-bottom: 200px;">
            <div class="portlet-title">
              <div class="caption">Kontak Lainnya</div>
              <div class="actions">
                <a href="#" class="btn btn-sm btn-success ng-scope" ng-click="addContact()"     ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i>
                </a>
              </div>
            </div>
            <div class="portlet-body" style="display: table; width: 100%;">
              <div class="panel-group accordion" id="accordion" style="padding-top: 20px;">
                <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" ng-repeat="contact in dataContact" ng-class="($index == 0) ? 'active' : ''">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title" style="position: relative;">
                          <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="#contact@{{ $index }}" role="button" onclick="return false;"><i class="fa fa-user"></i>&nbsp;&nbsp;@{{ contact.name }}</a>
                          <a href="#" onclick="return false" style=" position: absolute; top: 11px; right: 45px; font-size: 16px; color: #888;" delete-confirm on-delete="deleteContact(contact)"><i class="fa fa-trash"></i></a>
                        </h4>
                    </div>
                    <div id="contact@{{ $index }}" class="panel-collapse" ng-class="($index == 0) ? 'in' : ''">
                      <div class="panel-body">
                        <div class="col-md-12">
                          <form-builder field="name" data="contactFields" ng-model="contact.name"></form-builder>
                          <div class="row">
                            <div class="col-md-6">
                              <form-builder field="type" data="contactFields" ng-model="contact.type"></form-builder>
                            </div>
                            <div class="col-md-6">
                              <form-builder field="position" data="contactFields" ng-model="contact.position"></form-builder>
                            </div>
                          </div>
                          <form-builder field="address" data="contactFields" ng-model="contact.address"></form-builder>
                          <div class="row">
                            <div class="col-md-6">
                              <form-builder field="city" data="contactFields" ng-model="contact.city"></form-builder>
                            </div>
                            <div class="col-md-6">
                              <form-builder field="postal_code" data="contactFields" ng-model="contact.postal_code"></form-builder>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <form-builder field="state" data="contactFields" ng-model="contact.state"></form-builder>
                            </div>
                            <div class="col-md-6">
                              <form-builder field="country" data="contactFields" ng-model="contact.country"></form-builder>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <form-builder field="phone" data="contactFields" ng-model="contact.phone"></form-builder>
                            </div>
                            <div class="col-md-4">
                              <form-builder field="mobile" data="contactFields" ng-model="contact.mobile"></form-builder>
                            </div>
                            <div class="col-md-4">
                              <form-builder field="fax" data="contactFields" ng-model="contact.fax"></form-builder>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <form-builder field="email" data="contactFields" ng-model="contact.email"></form-builder>
                            </div>
                            <div class="col-md-6">
                              <form-builder field="website" data="contactFields" ng-model="contact.website"></form-builder>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
        </div>
       -->
	  </div>
    </form>

  </div>
</div>
<!-- END MAIN CONTENT -->
