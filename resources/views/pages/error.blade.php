<div class="row">
    <div class="col-md-12 page-404">
      <div class="number font-red"> @lang('system.error.500.title') </div>
        <div class="details">
            <h3>@lang('system.error.500.oops')</h3>
            <p> @lang('system.error.500.comeback') </p>
        </div>
    </div>
</div>
