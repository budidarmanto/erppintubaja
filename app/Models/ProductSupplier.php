<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductSupplier extends DevplusModel
{
    protected $table = 'product_supplier';
    protected $fillable = ['id', 'product', 'supplier', 'price', 'tax', 'discount'];
}
