<?php

namespace App\Http\Controllers\Product;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryWarehouse;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Auth;

class ProductFinder extends Controller
{
  public function index($productId = null){

    $listCategory = Auth::user()->authCategory();
    $defaultCategory = Input::get('cat');
    if(!empty($defaultCategory)){
      $listCategory = [$defaultCategory];
    }

    $keyword = Input::get('keyword');
    $supplier = Input::get('supplier');

    $query = ProductProduct::getQuery();
    if(!empty($productId)){
      $query->whereIn('product_product.id', explode('.', $productId));
    }
    if(!empty($supplier)){
      $query->join('product_supplier', 'product_supplier.product', '=', 'product_master.id');
      $query->where('product_supplier.supplier', $supplier);
    }
    $query->whereIn('product_master.category', $listCategory);
    $query->orderBy('product_master.name', 'ASC');
    
    // die("aa");
    $dg = Datagrid::source($query);
    $dg->perPage = 10;
    $dg->optionPerpage = [10, 20, 50, 100];
    $dg->title('Barang/Jasa');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value){
          $q->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('category', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.category', $value);

      return $query;
    });
    $dg->filter('type', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.type', $value);

      return $query;
    });
    $dg->add('code', 'Kode', true);
    $dg->add('name', 'Nama', true);


    $datagrid = $dg->build();

    $optionWarehouse = InventoryWarehouse::getOption();
    $warehouse = Input::get('warehouse');

    if(empty($warehouse)){
      $warehouse = InventoryWarehouse::find($optionWarehouse[0]['id']);
    }


    $data = ProductProduct::renderProductAttribute($datagrid['data']);
    $data = ProductProduct::getRelatedData($data, $warehouse);
    $datagrid['data'] = $data;
    $queryCat = ProductCategory::whereIn('id', $listCategory);
    $category = ProductCategory::getOption($queryCat, true);
    $datagrid['optionCategory'] = $category;
    $datagrid['optionType'] = Params::getOption('OPTION_PRODUCT_TYPE');
    $datagrid['supplier'] = $supplier;
    $datagrid['optionWarehouse'] = $optionWarehouse;
    $datagrid['defaultCategory'] = $defaultCategory;
    return response()->json($datagrid);

  }

}
