<span class="caption10">@lang('system.datagrid.massButton.withSelected'):&nbsp;&nbsp; </span>
<button class="btn btn-xs btn-danger tooltips" data-original-title="@lang('system.datagrid.massButton.delete')" data-placement="right" delete-confirm-ajax="mass" ng-if="response.role.indexOf('delete') != -1">
  <i class="fa fa-trash"></i>
</button>
