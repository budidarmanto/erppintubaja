/* Setup blank page controller */
angular.module('DevplusApp').controller('FormProductController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$filter', '$uibModal', function($rootScope, $scope, settings, response, $http, $location, $filter, $uibModal) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    }
    $scope.optionAllUom = angular.copy($scope.response.optionUom);
    $scope.dataSupplier = response.dataSupplier;
    $scope.dataVariant = response.dataVariant;
    $scope.dataBom = response.dataBom;
    $scope.dataAlternative = response.dataAlternative;

    $scope.useVariant = response.useVariant;

    $scope.changeUomCategory = function(){
      $scope.response.optionUom = [];
      if(typeof $scope.data.uom_category != 'undefined'){
        $scope.response.optionUom = $filter('filter')($scope.optionAllUom, {
          uom_category : $scope.data.uom_category
        }, true);
        if($scope.response.optionUom.length > 0 && $scope.data.uom == null){
            $scope.data.uom = $scope.response.optionUom[0].id;
        }
      }

      setTimeout(function(){
          $('.bs-select').selectpicker('refresh');
      }, 100);
    }
    $scope.changeUomCategory();

    // Handle Supplier
    $scope.showPopupSupplier = function(dataSupplier){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalSupplier.html',
        controller: 'ModalSupplierCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataSupplier || {}),
              optionTax: response.optionTax
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataSupplier){
          $scope.dataSupplier[$scope.dataSupplier.indexOf(dataSupplier)] = data;
        }else{
          $scope.dataSupplier.push(data);
        }
      });
    }

    $scope.deleteSupplier = function(supplier){
      $scope.dataSupplier.splice($scope.dataSupplier.indexOf(supplier), 1);
    }
    // End Handle Supplier


    // Handle Variant
    $scope.showPopupVariant = function(dataVariant){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalVariant.html',
        controller: 'ModalVariantCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataVariant || {}),
              optionAttribute: $scope.response.optionAttribute,
              optionAttributeValue: $scope.response.optionAttributeValue
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataVariant){
          $scope.$apply(function(){
              $scope.dataVariant[$scope.dataVariant.indexOf(dataVariant)] = data;
          });
        }else{
          $scope.dataVariant.push(data);
        }
      });
    }

    $scope.deleteVariant = function(variant){
      $scope.dataVariant.splice($scope.dataVariant.indexOf(variant), 1);
    }
    // End Handle Variant

    // Handle Product BOM
    $scope.showPopupBom = function(dataBom){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalBom.html',
        controller: 'ModalBomCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataBom || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataBom){
          $scope.$apply(function(){
              $scope.dataBom[$scope.dataBom.indexOf(dataBom)] = data;
          });
        }else{
          $scope.dataBom.push(data);
        }
      });
    }

    $scope.deleteBom = function(bom){
      $scope.dataBom.splice($scope.dataBom.indexOf(bom), 1);
    }
    // End Handle Product BOM

    // Handle Product Alternative
    $scope.showPopupAlternative = function(dataAlternative){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalAlternative.html',
        controller: 'ModalAlternativeCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataAlternative || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataAlternative){
          $scope.$apply(function(){
              $scope.dataAlternative[$scope.dataAlternative.indexOf(dataAlternative)] = data;
          });
        }else{
          $scope.dataAlternative.push(data);
        }
      });
    }

    $scope.deleteAlternative = function(alternative){
      $scope.dataAlternative.splice($scope.dataAlternative.indexOf(alternative), 1);
    }
    // End Handle Product Alternative

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.dataSupplier = $scope.dataSupplier;
        data.dataVariant = $scope.dataVariant;
        data.dataBom = $scope.dataBom;
        data.dataAlternative = $scope.dataAlternative;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataSupplier = resp.data.dataSupplier;
              $scope.dataVariant = resp.data.dataVariant;
              $scope.dataBom = resp.data.dataBom;
              $scope.dataAlternative = resp.data.dataAlternative;
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);


// Supplier Popup Controller
angular.module('DevplusApp').controller('ModalSupplierCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionTax = params.optionTax;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'supplier', label: 'Pemasok', type: 'select-ajax', attributes: {required: '', maxlength: 100, 'data-live-search': true, 'data-source': '/options/supplier'}},
    {name: 'price', label: 'Harga', type: 'number', attributes: {required: ''}},
    {name: 'tax', label: 'Pajak', type: 'select', attributes: {'ng-options' : 'item.id as item.text for item in optionTax', 'default-option' : 'Pilihan Pajak'}},
    {name: 'discount', label: 'Diskon', type: 'number'},
  ];

  $scope.save = function(data, $event){
    var form = $($event.currentTarget).parents('form');

    if(form.valid()){
      $scope.data.name = $('select[name="supplier"] option:selected').text();
      $scope.data.tax = (typeof $scope.data.tax == 'undefined') ? null : $scope.data.tax;
      $scope.data.discount = (typeof $scope.data.discount == 'undefined') ? null : $scope.data.discount;
      $uibModalInstance.close($scope.data);
    }

  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End Supplier Popup Controller

// Variant Popup Controller
angular.module('DevplusApp').controller('ModalVariantCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', function($scope,  $uibModalInstance, $filter, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionAttribute = params.optionAttribute;
  $scope.optionAttributeValue = params.optionAttributeValue;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'attribute', label: 'Varian', type: 'select', attributes: {required: '', 'ng-options': 'item.id as item.text for item in optionAttribute', 'ng-change': 'changeAttribute()'}},
    {name: 'attribute_value', label: 'Nilai', type: 'multiple', attributes: {required: '', 'ng-options': 'item.id as item.text for item in optionAttributeValue'}},
  ];

  $scope.optionAllAttributeValue = angular.copy(params.optionAttributeValue);

  $scope.changeAttribute = function(){

    $scope.optionAttributeValue = [];
    if(typeof $scope.data.attribute != 'undefined'){
      $scope.optionAttributeValue = $filter('filter')($scope.optionAllAttributeValue, {
        attribute : $scope.data.attribute
      }, true);
    }

    setTimeout(function(){
        $('.bs-select').selectpicker('refresh');
    }, 100);
  }
  $scope.changeAttribute();

  $scope.save = function(){
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End Variant Popup Controller

// BOM Popup Controller
angular.module('DevplusApp').controller('ModalBomCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', function($scope,  $uibModalInstance, $filter, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionUom = {};

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'product', label: 'Bahan Baku', type: 'select-ajax', attributes: {required: '', maxlength: 100, 'data-live-search': true, 'data-source': '/options/product', 'ng-change': 'changeProduct()', 'id': 'productBom'}},
    {name: 'qty', label: 'Jumlah', type: 'number', attributes: {required: ''}},
    {name: 'uom', label: 'Satuan Ukuran', type: 'select', attributes: {required: '', 'ng-options': 'item.id as item.text for item in optionUom', 'id': 'uomBom'}},
  ];



  $scope.changeProduct = function(){

    $scope.optionUom = [];
    if(typeof $scope.data.product != 'undefined'){
      $.get('/options/product-uom/'+$scope.data.product).then(function(res){
        $scope.$apply(function(){
            $scope.optionUom = res;
        });

        setTimeout(function(){
            $('.bs-select').selectpicker('refresh');
        }, 100);
      });

    }

  }
  $scope.changeProduct();

  $scope.save = function(){
    $scope.data.product_name = $('select#productBom option:selected').text();
    $scope.data.uom_name = $('select#uomBom option:selected').text();
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End BOM Popup Controller

// BOM Popup Controller
angular.module('DevplusApp').controller('ModalAlternativeCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', function($scope,  $uibModalInstance, $filter, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'product', label: 'Barang/Jasa', type: 'select-ajax', attributes: {required: '', maxlength: 100, 'data-live-search': true, 'data-source': '/options/product', 'id': 'productAlternative'}}
  ];


  $scope.save = function(){
    $scope.data.product_name = $('select#productAlternative option:selected').text();
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End BOM Popup Controller
