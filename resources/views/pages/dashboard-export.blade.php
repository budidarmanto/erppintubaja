<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .page-break {
        page-break-after: always;
    }
    body, table {
      font-size: 12px;
    }
    table h1 {
      font-size: 14px;
    }
    table th {
      font-weight: normal;
    }
    @media print {
      table {page-break-inside: avoid;}
    }
    @page {
      margin: 1.5cm;    
    }
    </style>
  </head>
  <body>
    <table style="width:100%">
      <tr>
        <td style="text-align:center">
          <h1 style="margin: 0; padding: 0;">Laporan Penjualan</h1>
        </td>
      </tr>
      @if($listCompany)
      <tr>
        <td><span>Kantor: {{ $listCompany }}</span></td>
      </tr>
      @endif
      <tr>
        <td><span>Tanggal: {{ date('d/m/Y', strtotime($dateFrom)) }} - {{ date('d/m/Y', strtotime($dateTo)) }}</span></td>
      </tr>
    </table>
    <br>
    <table class="table-detail" style="width:100%">
        <thead>
            <tr>
                <th>No. Sales Order</th>
                <th>Tgl. Sales Order</th>
                <th>Customer</th>
                <th>Salesman</th>
                <th>Product</th>
                <th>Jumlah</th>
                <th>Status Sales Order</th>
            </tr>
        </thead>
      <tbody>
        @foreach($data as $dt)
        <tr>
            <td>{{ $dt['code'] }}</td>
            <td>{{ $dt['date'] }}</td>
            <td>{{ $dt['customer_name'] }}</td>
            <td>{{ $dt['fullname'] }}</td>
            <td>{{ $dt['product_name'] }}</td>
            <td>{{ $dt['qty'] }}</td>
            <td>{{ $dt['status'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>