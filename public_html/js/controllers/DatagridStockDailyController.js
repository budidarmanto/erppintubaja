/* Setup blank page controller */
angular.module('DevplusApp').controller('DatagridStockDailyController', ['$rootScope', '$scope', 'settings', 'response', '$http', function($rootScope, $scope, settings, response, $http) {

    $scope.loading = false;
    $scope.response = response;
    $scope.checkToggle = false;

    $scope.params = {
      sort: '',
      keyword: '',
      perpage: '',
      page: '',
      category: '',
      type: '',
      warehouse: '',
      date: ''
    };
    $scope.currentLink = angular.copy($scope.response.link);
    $scope.queryString = '';

    $scope.selected = [];
    $scope.allChecked = function() {
      $scope.checkToggle = !$scope.checkToggle;

      if($scope.response.data && $scope.response.data.length <= 0){
        return false;
      }

      var i;
      for (i = 0; i < $scope.selected.length; ++i) {
          $scope.selected[i].isChecked = $scope.checkToggle;
      }
    };

    $scope.initSelect = function(data){
      $scope.selected.push({
        isChecked: false,
        value: data.id
      });
    }

    $scope.getSortingStatus = function(column){
      var status = '';
      if(column.sortable){
        status = 'sorting'
        if(column.sorting == 'asc'){
          status = 'sorting_asc';
        }else if(column.sorting == 'desc'){
          status = 'sorting_desc';
        }
      }

      return status;
    }

    $scope.perpageChanged = function(){
      $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
      $scope.setParams('page', $scope.response.currentPage);
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.setParams('keyword', $scope.keyword);
      }, 500);
    }

    $scope.setParams = function(params, value){
      var queryString = [];
      angular.forEach($scope.params, function(val, key){

        if(params == key){
          $scope.params[key] = value;
        }
        if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
          queryString.push(key+'='+$scope.params[key]);
        }
      });
      if(queryString.length > 0){
        $scope.queryString = queryString.join('&');
        queryString = '?'+$scope.queryString;

      }

      $scope.currentLink = $scope.response.link+queryString;
      $scope.refresh();
    }

    $scope.refresh = function(){
      if($scope.loading){
        return;
      }

      $scope.loading = true;
      $scope.selected = [];
      $scope.checkToggle = false;
      $http.get($scope.currentLink).then(function(resp){
        $scope.loading = false;
        $scope.response = resp.data;
      });
    }

    $scope.onDelete = function(){
      $scope.selected = [];
      $scope.refresh();
    }

    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
          $('#dateStockDaily').val(moment().format("DD/MM/YYYY"));
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;

    });

    $scope.handleDoubleClick = function(id){
      window.location = '#/'+$scope.response.modify+'/'+id;
    }

    $scope.categoryChanged = function(){
      $scope.setParams('category', $scope.category);
    }

    $scope.typeChanged = function(){
      $scope.setParams('type', $scope.type);
    }

    $scope.warehouseChanged = function(){
      $scope.setParams('warehouse', $scope.warehouse);
    }

    $scope.dateChanged = function($event){

      var date = '';
      if($scope.date != ''){
        date = moment($scope.date, 'DD/MM/YYYY').format('YYYY-MM-DD');
      }

      $scope.setParams('date', date);
    }

}]);
