<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class SettingCurrency extends DevplusModel
{
    protected $table = 'setting_currency';
    protected $fillable = ['id','code','name','default','active'];

    public static function getOption($model = null){
      if($model == null){
          $data = SettingCurrency::orderBy('code')->get();
      }else{
        $data = $model->orderBy('code')->get();
      }

      return Helper::toOption('code', 'name', $data, 'id', 'text', function($data){
        $data['name'] = $data['code'].' ('.$data['name'].')';

        return $data['name'];
      });
    }

    public static function getDefaultCurrency(){
      $default = SettingCurrency::where('default', true)->first();
      if(!empty($default)){
        $default = $default->code;
      }

      return $default;

    }
}
