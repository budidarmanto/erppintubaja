<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('product', ['as' => 'ProductApi', 'uses' => 'Api\Product@index']);
Route::get('optproduct', ['as' => 'ProductOptProduct', 'uses' => 'Api\Product@optproduct']);
Route::get('optdoordirection', ['as' => 'ProductOptProduct', 'uses' => 'Api\Product@optpDoorDirection']);
Route::get('optdoorinstallation', ['as' => 'ProductOptProduct', 'uses' => 'Api\Product@optpDoorInstallation']);
Route::get('optuom', ['as' => 'ProductOptProduct', 'uses' => 'Api\Product@optUom']);

Route::get('product-favorite', ['as' => 'ProductFavoriteApi', 'uses' => 'Api\ProductFavorite@index']);
Route::get('product-detail/{id}', ['as' => 'ProductDetailApi', 'uses' => 'Api\ProductDetail@index']);
Route::get('category', ['as' => 'CategoryApi', 'uses' => 'Api\Category@index']);
Route::get('customer', ['as' => 'CustomerApi', 'uses' => 'Api\Customer@index']);
Route::get('cashier-session/{company}', ['as' => 'CashiersessionApi', 'uses' => 'Api\CashierSession@index']);
Route::any('transaction', ['as' => 'Transaction', 'uses' => 'Api\Transaction@index']);
Route::any('order-reference', ['as' => 'OrderReferenceApi', 'uses' => 'Api\System@reference']);
Route::any('bank', ['as' => 'BankApi', 'uses' => 'Api\System@bank']);
Route::any('edc', ['as' => 'EdcApi', 'uses' => 'Api\System@edc']);
Route::any('surcharge', ['as' => 'SurchargeApi', 'uses' => 'Api\System@surcharge']);
Route::any('login', ['as' => 'LoginApi', 'uses' => 'Api\Login@index']);


Route::any('login-json', ['as' => 'LoginApijson', 'uses' => 'Api\Login@json']);
Route::get('product-json', ['as' => 'ProductApijson', 'uses' => 'Api\Product@json']);
Route::get('product-favorite-json', ['as' => 'ProductFavoriteApijson', 'uses' => 'Api\ProductFavorite@json']);
Route::get('product-detail-json/{id}', ['as' => 'ProductDetailApijson', 'uses' => 'Api\ProductDetail@json']);
Route::get('category-json', ['as' => 'CategoryApijson', 'uses' => 'Api\Category@json']);
Route::get('customer-json', ['as' => 'CustomerApijson', 'uses' => 'Api\Customer@json']);
Route::get('cashier-session-json/{company}', ['as' => 'CashiersessionApijson', 'uses' => 'Api\CashierSession@json']);
Route::any('order-reference-json', ['as' => 'OrderReferenceApijson', 'uses' => 'Api\SystemJson@reference']);
Route::any('bank-json', ['as' => 'BankApijson', 'uses' => 'Api\SystemJson@bank']);
Route::any('edc-json', ['as' => 'EdcApijson', 'uses' => 'Api\SystemJson@edc']);
Route::any('surcharge-json', ['as' => 'SurchargeApijson', 'uses' => 'Api\SystemJson@surcharge']);


//Route::get('customer', ['as' => 'CustomerApi', 'uses' => 'Api\Customer@index']);
//Route::get('customer-detail/{id}', ['as' => 'ProductDetailApi', 'uses' => 'Api\ProductDetail@detail']);


Route::any('cashier-json', ['as' => 'CashierApijson', 'uses' => 'Api\Cashier@json']);
Route::any('shift-json', ['as' => 'CashierShiftjson', 'uses' => 'Api\Shift@json']);

Route::group(['namespace' => 'Api'], function(){
	Route::group(['prefix' => 'customer'], function(){
		Route::post('/', ['as' => 'Customer', 'uses' => 'Customer@index']);
		Route::get('detail/{id}', ['as' => 'CustomerDetail', 'uses' => 'Customer@detail']);
		Route::post('/create', ['as' => 'CustomerCreate', 'uses' => 'Customer@create']);
		Route::get('/optcustomer', ['as' => 'OptionCustomer', 'uses' => 'Customer@optcustomer']);
		//Route::post('modify/{id}', ['as' => 'SalesModify', 'uses' => 'Customer@modify']);
		//Route::any('delete/{id?}', ['as' => 'SalesDelete', 'uses' => 'Customer@delete']);
		Route::post('/update/{id}', ['as' => 'sales_Customer_update', 'uses' => 'Customer@update']);
		Route::get('/get-cutomer/{id}', ['as' => 'sales_get_customer', 'uses' => 'Customer@getCustomer']);
	});
});

Route::group(['namespace' => 'Api'], function(){
	Route::group(['prefix' => 'sales-order'], function(){
		Route::post('/', ['as' => 'sales_order', 'uses' => 'SalesOrder@index']);
		Route::get('detail/{id}', ['as' => 'sales_order_detail', 'uses' => 'SalesOrder@detail']);
		Route::post('/create', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@create']);
		Route::post('/create-detail', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@createdetail']);
		
		Route::post('update-sales/{id}', ['as' => 'sales_order_update', 'uses' => 'SalesOrder@updateSales']);
		Route::post('update-detail/{id}', ['as' => 'sales_order_update_detail', 'uses' => 'SalesOrder@updatedetail']);
		
		Route::get('get-order/{id}', ['as' => 'sales_get_order', 'uses' => 'SalesOrder@getorder']);
		Route::get('get-detail/{id}', ['as' => 'sales_get_detail', 'uses' => 'SalesOrder@getdetail']);
		
		Route::post('/create-detail-doorheightpic', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@createdetailDoorHeightPic']);
		Route::post('/create-detail-doorwidthpic', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@createdetailDoorWidthPic']);
		Route::post('/create-detail-frameheightpic', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@createdetailFrameHeightPic']);
		Route::post('/create-detail-framewidthpic', ['as' => 'Sales_Order_Create', 'uses' => 'SalesOrder@createdetailFrameWidthPic']);
		
		Route::post('/delete-detail-doorheightpic', ['as' => 'Sales_Order_delete', 'uses' => 'SalesOrder@deleteDoorHeightPic']);
		Route::post('/delete-detail-doorwidthpic', ['as' => 'Sales_Order_delete', 'uses' => 'SalesOrder@deleteDoorWidthPic']);
		Route::post('/delete-detail-frameheightpic', ['as' => 'Sales_Order_delete', 'uses' => 'SalesOrder@deleteFrameHeightPic']);
		Route::post('/delete-detail-framewidthpic', ['as' => 'Sales_Order_delete', 'uses' => 'SalesOrder@deleteFrameWidthPic']);
		
		Route::post('change-status-sales', ['as' => 'sales_order_change_status', 'uses' => 'SalesOrder@changeStatus']);
		
		Route::get('history/{customer}', ['as' => 'sales_order_detail', 'uses' => 'SalesOrder@history']);
	});
});
