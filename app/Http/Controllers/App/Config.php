<?php

namespace App\Http\Controllers\App;

use App\Models\AppConfig;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Config extends Controller
{

  public function index() {

    $dg = Datagrid::source(AppConfig::query());
    $dg->title(__('app.app.config.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('value', 'ilike', '%'.$value.'%')->orWhere('description', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', __('app.app.config.code'));
    $dg->add('value', __('app.app.config.value'));
    $dg->add('description', __('app.app.config.description'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new AppConfig());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(AppConfig::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    AppConfig::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.app.config.title'));
    $form->add('code', __('app.app.config.code'), 'text')->rule('required|max:100');
    $form->add('value', __('app.app.config.value'), 'textarea')->rule('required');
    $form->add('description', __('app.app.config.description'), 'textarea');
    return $form;
  }

}
