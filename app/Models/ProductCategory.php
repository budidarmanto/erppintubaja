<?php

namespace App\Models;

use Devplus\Model\DevplusModelTree;
use Helper;
use Auth;

class ProductCategory extends DevplusModelTree
{
  protected $table = 'product_category';
  protected $fillable = ['id', 'name','parent', 'revision', 'reference'];

  public static function getOption($model = null, $all = false){
    $listCategory = Auth::user()->authCategory();
    if($model == null){
      if(!$all){
        $data = ProductCategory::whereIn('id', $listCategory)->orderBy('left')->get();
      }else{
        $data = ProductCategory::orderBy('left')->get();
      }
    }else{
      if(!$all){
        $model->whereIn('id', $listCategory);
      }
      $data = $model->orderBy('left')->get();
    }

    return Helper::toOption('id', 'name', $data, 'id', 'text', function($data){
      return str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $data['depth']).$data['name'];
    });
  }
}
