@include('auth.login-header')
<!-- BEGIN FORGOT PASSWORD FORM -->
<h1>Lupa Password?</h1>
<p> Silahkan masukan alamat email Anda. Kami akan mengirimkan tautan untuk memperbaharui kata sandi Anda. </p>
<form class="forget-form" action="forgot" method="post">
    {{ csrf_field() }}
    @include('alerts')
    <div class="form-group">
        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
    <div class="form-actions">
        <a href="login" class="btn green btn-outline">Kembali</a>
        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
@include('auth.login-footer')
