<?php
namespace App\Http\Controllers\Inventory;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Carbon\Carbon;
use Helper;

class StockCard{

  // Params: Product ID
  public function index($id, $warehouse = null){

    $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

    $fromDate = (empty(Input::get('datestart'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('datestart'));
    $toDate = (empty(Input::get('dateend'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('dateend'));
    $toDate = $toDate->addDay();
    $fromDate = $fromDate->toDateTimeString();
    $toDate = $toDate->toDateTimeString();


    $query = ProductProduct::getQuery();
    if(!empty($id)){
      $query->where('product_product.id', $id);
    }
    $query->orderBy('product_master.name', 'ASC');
    $data = $query->get()->toArray();

    $optionWarehouse = InventoryWarehouse::getOption();
    if(empty($warehouse)){
      $warehouse = $optionWarehouse[0]['id'];
    }
    $warehouse = InventoryWarehouse::find($warehouse);
    $warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse->id)->get();
    $listIdWarehouseLocation = collect($warehouseLocation)->pluck('id')->toArray();
    // dd($listIdWarehouseLocation);

    $data = ProductProduct::renderProductAttribute($data);
    $data = ProductProduct::getRelatedData($data, $warehouse);
    $dataProduct = collect($data)->first();


    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->where('inventory_stock.product', $dataProduct['id'])
    ->whereDate('inventory_stock.date', $fromDate);
    if(!empty($warehouse)){
      $inventoryStock->where('inventory_warehouse_location.warehouse', $warehouse->id);
    }
    $inventoryStock = $inventoryStock->first();
    $beginQty = $this->getStock($dataProduct['id'], $warehouse->id, $fromDate);
    $endQty = $this->getStock($dataProduct['id'], $warehouse->id, $toDate);
    $beginQty = (empty($beginQty)) ? 0 : $beginQty['begin_qty'];
    $endQty = (empty($endQty)) ? 0 : $endQty['end_qty'];


    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.qty',
      'inventory_stock_movement_product.qty AS qty_in',
      'inventory_stock_movement_product.qty AS qty_out',
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement.type',
      'inventory_stock_movement.type AS type_name',
      'inventory_stock_movement.status',
      'inventory_stock_movement.source',
      'inventory_stock_movement.destination',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'product_uom.name AS uom_name'
    ])
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    // ->whereDate('inventory_stock_movement.date', $yesterday)
    ->where('inventory_stock_movement_product.product', $dataProduct['id'])
    ->where(function($q){
      $q
      ->where(function($q){
        return $q->where('inventory_stock_movement.type', 'CH')->whereIn('inventory_stock_movement.status', ['1CS']);
      })
      ->orWhere(function($q){
        return $q->where('inventory_stock_movement.type', 'RC')->whereIn('inventory_stock_movement.status', ['3RA']);
      })
      ->orWhere(function($q){
        return $q->where('inventory_stock_movement.type', 'AD')->whereIn('inventory_stock_movement.status', ['3AA']);
      })
      ->orWhere(function($q){
        return $q->where('inventory_stock_movement.type', 'DO')->whereIn('inventory_stock_movement.status', ['4DD', '3DO']);
      })
      ->orWhere(function($q){
        return $q->where('inventory_stock_movement.type', 'IT')->whereIn('inventory_stock_movement.status', ['3IT']);
      });
    })
    ->where(function($q) use ($listIdWarehouseLocation){
      return $q->where(function($q) use ($listIdWarehouseLocation){
        return $q->whereIn('inventory_stock_movement.type', ['AD', 'DO', 'CH', 'IT'])->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation);
      })
      ->orWhere(function($q) use ($listIdWarehouseLocation){
        return $q->whereIn('inventory_stock_movement.type', ['RC'])->whereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
      });
    })
    ->whereBetween('inventory_stock_movement.updated_at', [$fromDate, $toDate])
    ->orderBy('inventory_stock_movement.date', 'ASC');
    $dg = Datagrid::source($query);
    $dg->title($dataProduct['name']);
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value){
           $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%');
           return $q;
        });
      }

      return $query;
    });
    $dg->filter('datestart', function($query, $value){
      return $query;
    });
    $dg->filter('dateend', function($query, $value){
      return $query;
    });

    $dg->add('date', 'Tanggal', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('code', 'Kode', false);
    $dg->add('type_name', 'Tipe', false)->options([
      'RC' => 'Penerimaan',
      'AD' => 'Penyesuaian Persediaan',
      'IT' => 'Mutasi Internal',
      'CH' => 'Kasir',
      'DO' => 'Pengiriman',
    ]);
    $dg->add('qty_in', 'Masuk', false)->render(function($data){
      return (in_array($data['type'], ['RC', 'AD'])) ? $data['qty_in'] : '-';
    });
    $dg->add('qty_out', 'Keluar', false)->render(function($data){
      return (in_array($data['type'], ['DO', 'CH'])) ? $data['qty_out'] : '-';
    });
    $dg->add('qty', 'Stok', false);
    $dg->add('uom_name', 'Satuan Ukuran', true);

    $datagrid = $dg->build();

    $datagrid['beginQty'] = $beginQty;
    $datagrid['endQty'] = $endQty;

    $countStock = $beginQty;

// dd($datagrid['data']);
    foreach($datagrid['data'] as $key => $value){

      $currentUom = collect($dataUom)->where('id', $value['uom'])->first();

      $value['qty'] = $value['qty'] * $currentUom['ratio'];
      $value['qty_in'] = ($value['qty_in'] != '-') ? $value['qty'] : '-';
      $value['qty_out'] = ($value['qty_out'] != '-') ? $value['qty'] : '-';

      $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
      if(!empty($basicUnit)){
        $value['uom_name'] = $basicUnit['name'];
      }


      if(in_array($value['type'], ['RC', 'AD'])){
        $countStock += $value['qty'];
      }else if(in_array($value['type'], ['DO', 'CH'])){
        $countStock -= $value['qty'];
      }else if($value['type'] == 'IT'){
        // dd("sini");
        $value['qty_in'] = $value['qty'];
        $value['qty_out'] = $value['qty'];
      }
      $value['count_stock'] = $countStock;
      $datagrid['data'][$key] = $value;
    }

    foreach($datagrid['data'] as $key => $value){
      $datagrid['data'][$key]['qty'] = number_format($value['qty'], 2);
      $datagrid['data'][$key]['qty_in'] = ($value['qty_in'] != '-') ? number_format($value['qty_in'], 2) : '-';
      $datagrid['data'][$key]['qty_out'] = ($value['qty_out'] != '-') ? number_format($value['qty_out'],2) : '-';
    }
    $datagrid['endQty'] = $countStock;

    return response()->json($datagrid);
  }

  public function getStock($product, $warehouse = null, $date){

    $beginQty = 0;
    $endQty = 0;

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->where('inventory_stock.product', $product)
    ->whereDate('inventory_stock.date', $date);
    if(!empty($warehouse)){
      $inventoryStock->where('inventory_warehouse_location.warehouse', $warehouse);
    }
    $inventoryStock = $inventoryStock->get();
    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        $beginQty += $val['begin_qty'];
        $endQty += $val['end_qty'];
      }
    }
    return [
      'begin_qty' => $beginQty,
      'end_qty' => $endQty
    ];

  }

}
