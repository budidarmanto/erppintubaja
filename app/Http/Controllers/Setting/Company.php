<?php

 namespace App\Http\Controllers\Setting;

 use App\Models\SettingCompany;
 use App\Http\Controllers\Controller;
 use FormBuilder;
 use Datagrid;
 use Request;
 use Input;

class Company extends Controller{

    public function index() {

      $dg = Datagrid::source(SettingCompany::orderBy('left'));
      $dg->title(__('app.setting.company.title'));
      $dg->filter('keyword', function($query, $value){
        if($value != '')
            return $query->where('name', 'ilike', '%'.$value.'%');

        return $query;
      });
      $dg->add('name', __('app.setting.company.name'), false)->render(function($data){
        return str_repeat('&nbsp;&nbsp;', $data['depth']).' '.$data['name'];
      });
      $dg->add('city', __('app.setting.company.city'), false);
      $dg->add('active', __('app.setting.company.active'), false)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
      $datagrid = $dg->build();
      return response()->json($datagrid);
    }

    public function create(){
      $form = $this->anyForm(new SettingCompany());
      $dataForm = $form->build();

      if($form->hasRequest()){
        if($form->saved()){
          return response()->json([
            'status' => true
          ]);
        }else{
          return response()->json([
            'errorMessage' => $form->validatorMessages
          ]);
        }
      }
      $dataForm['optionCompany'] = SettingCompany::getOption();
      return response()->json($dataForm);
    }

    public function modify($id = null) {

      $form = $this->anyForm(SettingCompany::find($id));
      $dataForm = $form->build();

      if($form->hasRequest()){
        if($form->saved()){
          return response()->json([
            'status' => true
          ]);
        }else{
          return response()->json([
            'errorMessage' => $form->validatorMessages
          ]);
        }
      }
      $dataOption = SettingCompany::where('id','!=',$id);
      $dataOption = SettingCompany::getOption($dataOption);
      $dataForm['optionCompany'] = $dataOption;
      return response()->json($dataForm);
    }

    public function delete(){

      $id = Input::get('id');
      SettingCompany::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($source){

      $form = FormBuilder::source($source);
      $form->title(__('app.setting.company.title'));
      $form->add('picture', __('app.setting.company.picture'), 'image')->attributes([
        'moveTo' => 'img/company'
      ]);
      $form->add('code', 'Kode', 'text')->rule('required|max:25');
      $form->add('name', __('app.setting.company.name'), 'text')->rule('required|max:50');
      $form->add('parent', __('app.setting.company.parent'), 'select')->attributes([
        'ng-options' => 'item.id as item.text for item in response.optionCompany',
        'default-option' => "--  Parent Company  --"
      ]);
      $form->add('address', __('app.setting.company.address'), 'textarea');
      $form->add('city', __('app.setting.company.city'), 'text')->rule('max:50');
      $form->add('postal_code', __('app.setting.company.postal_code') ,'text')->rule('max:5');
      $form->add('state', __('app.setting.company.state'), 'text')->rule('max:50');
      $form->add('country', __('app.setting.company.country'), 'text')->rule('max:50');
      $form->add('website', __('app.setting.company.website'), 'text')->rule('max:255');
      $form->add('phone', __('app.setting.company.phone'), 'text')->rule('max:50');
      $form->add('fax', __('app.setting.company.fax'), 'text')->rule('max:50');
      $form->add('email', __('app.setting.company.email'), 'text')->rule('max:255');
      $form->add('npwp', 'NPWP', 'text')->rule('max:50');
      $form->add('active' , __('app.setting.company.active'), 'switch');
      return $form;

    }
}
