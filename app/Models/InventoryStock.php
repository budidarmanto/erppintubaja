<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class InventoryStock extends DevplusModel
{
    protected $table = 'inventory_stock';
    protected $fillable = ['id', 'product', 'uom', 'warehouse_location', 'date', 'begin_qty', 'plus_qty', 'min_qty', 'end_qty'];
}
