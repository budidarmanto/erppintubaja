##step

- copy `env.example` file to `.env` and edit it to meet your requirement
- run `php artisan key:generate` if neccessary
- run `php artisan migrate`
- run `php artisan serve`
