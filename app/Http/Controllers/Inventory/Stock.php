<?php

namespace App\Http\Controllers\Inventory;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryWarehouse;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Excel;
use Auth;

class Stock extends Controller
{
  public function index($productId = null){

//    ProductProduct::getDataStock('59819cdfcd6642d7c9', '5984b1f8b2209c1d41');
    $listCategory = Auth::user()->authCategory();

    $keyword = Input::get('keyword');

    $query = ProductProduct::getQuery();
    if(!empty($productId)){
      $query->whereIn('product_product.id', explode('.', $productId));
    }
    $query->whereIn('product_master.category', $listCategory);
    $query->orderBy('product_master.name', 'ASC');
    $dg = Datagrid::source($query);
    $dg->title('Persediaan');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value){
           $q->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');
           return $q;
        });
      }

      return $query;
    });
    $dg->filter('category', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.category', $value);

      return $query;
    });
    $dg->filter('type', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.type', $value);

      return $query;
    });
    $dg->add('code', 'Kode', true);
    $dg->add('name', 'Nama', true);


    $datagrid = $dg->build();

    $optionWarehouse = InventoryWarehouse::getOption();
    $warehouse = Input::get('warehouse');

    if(empty($warehouse)){
      $warehouse = $optionWarehouse[0]['id'];
    }
    $warehouse = InventoryWarehouse::find($warehouse);

    $data = ProductProduct::renderProductAttribute($datagrid['data']);
    $data = ProductProduct::getRelatedData($data, $warehouse);
    // dd($data);
    $datagrid['data'] = $data;
    $category = ProductCategory::getOption();
    $datagrid['optionCategory'] = $category;
    $datagrid['optionType'] = Params::getOption('OPTION_PRODUCT_TYPE');
    $datagrid['optionWarehouse'] = $optionWarehouse;

    $export = Input::get('export');
    if(!empty($export)){
      Excel::create('Stock', function($excel) use ($data, $warehouse){
        $excel->sheet('Sheet 1', function($sheet) use ($data, $warehouse) {
          $sheet->loadView('export.stock', compact('data', 'warehouse'));
        });
      })->download('xls');
    }

    return response()->json($datagrid);

  }

  public function checkAvailableStock(){

  }

}
