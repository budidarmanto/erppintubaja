<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductAttribute extends DevplusModel
{
    protected $table = 'product_attribute';
    protected $fillable = ['id', 'name'];

    public static function getOption($model = null){
      if($model == null){
          $data = ProductAttribute::all();
      }else{
        $data = $model->get();
      }

      return Helper::toOption('id','name',$data);
    }
}
