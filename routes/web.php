<?php
use Devplus\FormBuilder\FormBuilder;
use App\Models\PajakPegawai;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::setLocale('id');
// Auth
Route::get('login', ['as' => 'AuthLogin', 'uses' => 'Home@login']);
Route::post('login', ['uses' => 'Home@dologin']);
Route::get('forgot', ['as' => 'AuthForgot', 'uses' => 'Home@forgotPassword']);
Route::post('forgot', ['uses' => 'Home@doForgotPassword']);
Route::get('forgot-success', ['as' => 'AuthForgotSuccess', 'uses' => 'Home@forgotPasswordSuccess']);
Route::any('reset', ['as' => 'AuthReset', 'uses' => 'Home@resetPassword']);
Route::post('reset', ['uses' => 'Home@doResetPassword']);


Route::group(['middleware' => 'auth'], function () {
  // Dashboard, Logout

  Route::get('/', ['as' => 'home', 'uses' => 'Home@index', 'middleware' => 'optimize.html']);
  Route::get('logout', ['as' => 'AuthLogout', 'uses' => 'Home@logout']);
  Route::any('my-profile', ['as' => 'MyProfile', 'uses' => 'Setting\MyProfile@index']);
  Route::any('change-password', ['as' => 'ChangePassword', 'uses' => 'Setting\MyProfile@changePassword']);
});
/**
 * Route Group For Authenticated User
 */
Route::group(['middleware' => ['auth', 'auth.role']], function () {

  // Dashboard
  Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function(){
    // General
    Route::group(['prefix' => 'general'], function() {
      Route::get('/', ['as' => 'DashboardGeneral', 'uses' => 'General@index']);
      Route::any('getdata', ['as' => 'DashboardGeneralGet', 'uses' => 'General@getData']);
      Route::any('download', ['as' => 'DashboardGeneralDownload', 'uses' => 'General@download']);
    });

  });

  // System Configuration
  Route::group(['namespace' => 'App', 'prefix' => 'app'], function(){

    Route::group(['prefix' => 'role'], function() {
      Route::get('/', ['as' => 'AppRole', 'uses' => 'Role@index']);
      Route::any('create', ['as' => 'AppRoleCreate', 'uses' => 'Role@create']);
      Route::any('modify/{id}', ['as' => 'AppRoleModify', 'uses' => 'Role@modify']);
      Route::any('delete', ['as' => 'AppRoleDelete', 'uses' => 'Role@delete']);
    });

    Route::group(['prefix' => 'group'], function() {
      Route::get('/', ['as' => 'AppGroup', 'uses' => 'Group@index']);
      Route::any('create', ['as' => 'AppGroupCreate', 'uses' => 'Group@create']);
      Route::any('modify/{id}', ['as' => 'AppGroupModify', 'uses' => 'Group@modify']);
      Route::any('delete', ['as' => 'AppGroupDelete', 'uses' => 'Group@delete']);
    });

    Route::group(['prefix' => 'application'], function() {
      Route::get('/', ['as' => 'AppApplication', 'uses' => 'Application@index']);
      Route::any('create', ['as' => 'AppApplicationCreate', 'uses' => 'Application@create']);
      Route::any('modify/{id}', ['as' => 'AppApplicationModify', 'uses' => 'Application@modify']);
      Route::any('delete', ['as' => 'AppApplicationDelete', 'uses' => 'Application@delete']);
    });

    Route::group(['prefix' => 'config'], function() {
      Route::get('/', ['as' => 'AppConfig', 'uses' => 'Config@index']);
      Route::any('create', ['as' => 'AppConfigCreate', 'uses' => 'Config@create']);
      Route::any('modify/{id}', ['as' => 'AppConfigModify', 'uses' => 'Config@modify']);
      Route::any('delete', ['as' => 'AppConfigDelete', 'uses' => 'Config@delete']);
    });
  });
  //--- End System Configuration

  // Setting
  Route::group(['namespace' => 'Setting', 'prefix' => 'setting'], function(){

    Route::group(['prefix' => 'user'], function() {
      Route::get('/', ['as' => 'SettingUser', 'uses' => 'User@index']);
      Route::any('create', ['as' => 'SettingUserCreate', 'uses' => 'User@create']);
      Route::any('modify/{id}', ['as' => 'SettingUserModify', 'uses' => 'User@modify']);
      Route::any('delete', ['as' => 'SettingUserDelete', 'uses' => 'User@delete']);
    });

    Route::group(['prefix' => 'company'], function() {
      Route::get('/', ['as' => 'SettingCompany', 'uses' => 'Company@index']);
      Route::any('create', ['as' => 'SettingCompanyCreate', 'uses' => 'Company@create']);
      Route::any('modify/{id}', ['as' => 'SettingCompanyModify', 'uses' => 'Company@modify']);
      Route::any('delete', ['as' => 'SettingCompanyDelete', 'uses' => 'Company@delete']);
    });

    Route::group(['prefix' => 'workflow'], function() {
      Route::get('/', ['as' => 'SettingWorkflow', 'uses' => 'Workflow@index']);
      Route::any('create', ['as' => 'SettingWorkflowCreate', 'uses' => 'Workflow@create']);
      Route::any('modify/{id}', ['as' => 'SettingWorkflowModify', 'uses' => 'Workflow@modify']);
      Route::any('delete', ['as' => 'SettingWorkflowDelete', 'uses' => 'Workflow@delete']);
    });

    Route::group(['prefix' => 'currency'], function() {
      Route::get('/', ['as' => 'SettingCurrency', 'uses' => 'Currency@index']);
      Route::any('create', ['as' => 'SettingCurrencyCreate', 'uses' => 'Currency@create']);
      Route::any('modify/{id}', ['as' => 'SettingCurrencyModify', 'uses' => 'Currency@modify']);
      Route::any('delete', ['as' => 'SettingCurrencyDelete', 'uses' => 'Currency@delete']);
    });

    Route::group(['prefix' => 'tax'], function() {
      Route::get('/', ['as' => 'SettingTax', 'uses' => 'Tax@index']);
      Route::any('create', ['as' => 'SettingTaxCreate', 'uses' => 'Tax@create']);
      Route::any('modify/{id}', ['as' => 'SettingTaxModify', 'uses' => 'Tax@modify']);
      Route::any('delete', ['as' => 'SettingTaxDelete', 'uses' => 'Tax@delete']);
    });

    Route::group(['prefix' => 'email-template'], function() {
      Route::get('/', ['as' => 'SettingEmailtemplate', 'uses' => 'EmailTemplate@index']);
      Route::any('create', ['as' => 'SettingEmailtemplateCreate', 'uses' => 'EmailTemplate@create']);
      Route::any('modify/{id}', ['as' => 'SettingEmailtemplateModify', 'uses' => 'EmailTemplate@modify']);
      Route::any('delete', ['as' => 'SettingEmailtemplateDelete', 'uses' => 'EmailTemplate@delete']);
    });

  });
  //--- Setting


  // Product
  Route::group(['namespace' => 'Product', 'prefix' => 'product'], function(){
    // UOM
    Route::group(['prefix' => 'uom'], function() {
      Route::get('/', ['as' => 'ProductUom', 'uses' => 'Uom@index']);
      Route::any('create', ['as' => 'ProductUomCreate', 'uses' => 'Uom@create']);
      Route::any('modify/{id}', ['as' => 'ProductUomModify', 'uses' => 'Uom@modify']);
      Route::any('delete', ['as' => 'ProductUomDelete', 'uses' => 'Uom@delete']);
    });
    // Category
    Route::group(['prefix' => 'category'], function() {
      Route::get('/', ['as' => 'ProductCategory', 'uses' => 'Category@index']);
      Route::any('create', ['as' => 'ProductCategoryCreate', 'uses' => 'Category@create']);
      Route::any('modify/{id}', ['as' => 'ProductCategoryModify', 'uses' => 'Category@modify']);
      Route::any('delete', ['as' => 'ProductCategoryDelete', 'uses' => 'Category@delete']);
    });
    // Attribute
    Route::group(['prefix' => 'attribute'], function() {
      Route::get('/', ['as' => 'ProductAttribute', 'uses' => 'Attribute@index']);
      Route::any('create', ['as' => 'ProductAttributeCreate', 'uses' => 'Attribute@create']);
      Route::any('modify/{id}', ['as' => 'ProductAttributeModify', 'uses' => 'Attribute@modify']);
      Route::any('delete', ['as' => 'ProductAttributeDelete', 'uses' => 'Attribute@delete']);
    });

    // Master Product
    Route::group(['prefix' => 'master'], function() {
      Route::get('/', ['as' => 'ProductMaster', 'uses' => 'Master@index']);
      Route::any('create', ['as' => 'ProductMasterCreate', 'uses' => 'Master@create']);
      Route::any('modify/{id}', ['as' => 'ProductMasterModify', 'uses' => 'Master@modify']);
      Route::any('delete', ['as' => 'ProductMasterDelete', 'uses' => 'Master@delete']);
    });

  });
  //--- End Product

  // Inventory
  Route::group(['namespace' => 'Inventory', 'prefix' => 'inventory'], function(){
    // UOM
    Route::group(['prefix' => 'warehouse'], function() {
      Route::get('/', ['as' => 'InventoryWarehouse', 'uses' => 'Warehouse@index']);
      Route::any('create', ['as' => 'InventoryWarehouseCreate', 'uses' => 'Warehouse@create']);
      Route::any('modify/{id}', ['as' => 'InventoryWarehouseModify', 'uses' => 'Warehouse@modify']);
      Route::any('delete', ['as' => 'InventoryWarehouseDelete', 'uses' => 'Warehouse@delete']);
    });

    // Stock
    Route::group(['prefix' => 'stock'], function() {
      Route::get('/', ['as' => 'InventoryStock', 'uses' => 'Stock@index']);
    });
  });

  // Sales
  Route::group(['namespace' => 'Sales', 'prefix' => 'sales'], function(){
    // Customer
    Route::group(['prefix' => 'customer'], function() {
      Route::get('/', ['as' => 'SalesCustomer', 'uses' => 'Customer@index']);
      Route::any('create', ['as' => 'SalesCustomerCreate', 'uses' => 'Customer@create']);
      Route::any('modify/{id}', ['as' => 'SalesCustomerModify', 'uses' => 'Customer@modify']);
      Route::any('delete', ['as' => 'SalesCustomerDelete', 'uses' => 'Customer@delete']);
    });
    //-- End Customer

    // Sales Order
    Route::group(['prefix' => 'sales-order'], function() {
      Route::get('/', ['as' => 'SalesSalesorder', 'uses' => 'SalesOrder@index']);
      Route::any('create', ['as' => 'SalesSalesorderCreate', 'uses' => 'SalesOrder@create']);
      Route::any('modify/{id}', ['as' => 'SalesSalesorderModify', 'uses' => 'SalesOrder@modify']);
      Route::any('delete', ['as' => 'SalesSalesorderDelete', 'uses' => 'SalesOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'SalesSalesorderChange', 'uses' => 'SalesOrder@changeStatus']);
    });
  });
  //--- End Sales
});

// Genera
Route::group(['prefix' => 'general'], function(){
  Route::get('count-alert', ['as' => 'GeneralCountalert', 'uses' => 'Globals@countAlert']);
});

// Options
Route::group(['prefix' => 'options'], function(){
  Route::get('supplier', ['as' => 'OptionsSuppier', 'uses' => 'Globals@getOptionSupplier']);
  Route::get('customer', ['as' => 'OptionsCustomer', 'uses' => 'Globals@getOptionCustomer']);
  Route::get('product', ['as' => 'OptionsProduct', 'uses' => 'Globals@getOptionProduct']);
  Route::get('product-uom/{id}', ['as' => 'OptionsProductuom', 'uses' => 'Globals@getOptionProductUom']);
});

// Product Finder
Route::get('product-finder/{product?}', ['as' => 'ProductFinder', 'uses' => 'Product\ProductFinder@index']);

// Upload
Route::any('upload', function(){
  if (Input::hasFile('file')) {
    $moveTo = Input::get('moveTo');
    if($moveTo == ''){
      $moveTo = 'uploads';
    }

    $file = Input::file('file');
    $originalName = $file->getClientOriginalName();
    $extension = $file->getClientOriginalExtension();
    $destinationPath = base_path().'/public/'.$moveTo; // upload path
    $fileName = uniqid().substr(md5(mt_rand()), 0, 5).'.'.$extension; // renameing image

    $file->move($destinationPath, $fileName);
    $realPath = $file->getRealPath();

    return response()->json([
      'originalName' => $originalName,
      'fileName' => $fileName,
      'path' => $moveTo.'/'.$fileName
    ]);
  }

});

Route::get('user-finder/{employee?}', ['as' => 'UserFinder', 'uses' => 'UserFinder@index']);

Route::any('remove-upload', function(){
  $file = Input::get('file');
  if(!empty($file) && file_exists($file)){
    unlink($file);
    return response()->json([
      'status' => true
    ]);
  }else{
    return response()->json([
      'status' => false,
      'message' => 'File Not Exists!'
    ]);
  }
});

Route::get('change-company/{id}', function($id) {
    $listCompany = Auth::user()->listCompany();
    if(count($listCompany) > 0 && in_array($id, $listCompany)){
      $company = App\Models\SettingCompany::find($id);
      if ($company != null) {
          session(['company' => $id]);
      }
    }

    return response()->json([
      'id' => $company->id,
      'name' => $company->name
    ]);
});


// Template
Route::group(['prefix' => 'template', 'middleware' => 'optimize.html'], function () {
  Route::get('header', function (){

    $listCompany = Auth::user()->listCompany();

    $queryCompany = App\Models\SettingCompany::whereIn('id', $listCompany);
    $company = App\Models\SettingCompany::getOption($queryCompany);
    $currentCompany = Auth::user()->authCompany();

    return view('header', compact('company', 'currentCompany'));
  });
  Route::get('footer', function (){
    return view('footer');
  });
  Route::get('sidebar', function (){
    Auth::user()->buildAccess();
    return view('sidebar');
  });
  Route::get('quick-sidebar', function (){
    return view('quick-sidebar');
  });
  Route::get('blank', function (){
    return view('pages.blank');
  });
  Route::get('no-access', function(){
    return view('pages.no-access');
  });
  Route::get('not-found', function(){
    return view('pages.not-found');
  });
  Route::get('error', function(){
    return view('pages.error');
  });
  Route::get('datagrid', function(){
    return view('pages.datagrid');
  });
  
  Route::get('datagrid-user-finder', function(){
    return view('pages.datagrid-user-finder');
  });  
  Route::get('form-profile', function (){
    return view('pages.form-profile');
  });

  Route::get('datagrid-stock', function(){
    return view('pages.datagrid-stock');
  });

  Route::get('dashboard-general', function(){
    return view('pages.dashboard-general');
  });

  Route::get('datagrid-stock-daily', function(){
    return view('pages.datagrid-stock-daily');
  });
  
  Route::get('datagrid-user', function(){
    return view('pages.datagrid-user');
  });

  Route::get('datagrid-sales-order', function(){
    return view('pages.datagrid-sales-order');
  });

  Route::get('form-sales-order', function(){
    return view('pages.form-sales-order');
  });
  
  Route::get('form-warehouse', function(){
    return view('pages.form-warehouse');
  });

  Route::get('form', function(){
    return view('pages.form');
  });

  Route::get('form-workflow', function(){
    return view('pages.form-workflow');
  });
  
  Route::get('form-contact', function(){
    return view('pages.form-contact');
  });

  Route::get('datagrid-product-finder', function(){
    return view('pages.datagrid-product-finder');
  });

  // Pages
  Route::get('form-user', function(){
    return view('pages.form-user');
  });
  Route::get('form-application', function(){
    return view('pages.form-application');
  });
  Route::get('form-group', function(){
    return view('pages.form-group');
  });
  Route::get('form-company', function(){
    return view('pages.form-company');
  });

  // Product
  Route::get('datagrid-product', function(){
    return view('pages.datagrid-product');
  });
  Route::get('form-uom', function(){
    return view('pages.form-uom');
  });
  Route::get('form-attribute', function(){
    return view('pages.form-attribute');
  });
  Route::get('form-product', function(){
    return view('pages.form-product');
  });

});
