<?php

return [

  'simhu' => [
    'project' => [
      'project' => 'Project',
      'name' => 'Project Name',
      'company' => 'Company',
      'singkatan' => 'Project Name (Abbreviation)',
      'spk_intern_no' => 'SPK Intern No.',
      'spk_intern_tanggal' => 'SPK Intern Date',
      'spk_extern_no' => 'SPK Extern No.',
      'spk_extern_tanggal' => 'SPK Extern Date',
      'kontrak_no' => 'Contract Number',
      'kontrak_tanggal' => 'Contract Date',
      'kontrak_harga' => 'Contract Value',
      'nilai_usd' => 'USD Value',
      'jenis_pekerjaan' => 'Type of Work',
      'pemberi_kerja' => 'Employer',
      'sumber_dana' => 'Fund Source',
      'kontrak_mulai' => 'Contract Start Date',
      'kontrak_selesai' => 'Contract End Date',
      'kontrak_perpanjangan' => 'Contract Renewal',
      'kontrak_pemeliharaan' => 'Contract Maintenance',
      'catatan' => 'Note',
      'status' => 'Status',
      'jenis_proyek' => 'Project Type',
      'manager_proyek' => 'Project Manager',
      'jenis_jo' => 'JO Type',
      'nama_partner' => 'Partner',
      'porsi_jo' => 'JO Portion',
      'assign' => 'Assign To',
    ],
    'jenis_pekerjaan' => [
      'title' => 'Job Type',
      'nama' => 'Job Name'
    ],
    'report' => [
      'kategori' => [
        'title' => 'Report Category',
        'name' => 'Name',
        'code' => 'Code'
      ],
      'header' => [
        'title' => 'Header',
        'nama' => 'Name',
        'code' => 'Code',
        'parent' => 'Parent',
        'kategori' => 'Category'
      ],
      'uraian' => [
        'title' => 'Analysis',
        'nama' => 'Name',
        'code' => 'Code',
        'parent' => 'Parent',
        'kategori' => 'Category'
      ],
    ],
  ],
  'app' => [
    'application' => [
      'title' => 'Application',
      'code' => 'Code',
      'name' => 'Application Code',
    ],
    'module' => [
      'title' => 'Module',
      'code' => 'Code',
      'name' => 'Module Name',
      'role' => 'Role',
    ],
    'config' => [
      'title' => 'Parameter Settings',
      'code' => 'Code',
      'value' => 'Value',
      'description' => 'Description'
    ],
    'group' => [
      'title' => 'Group',
      'name' => 'Group Name',
    ],
    'role' => [
      'title' => 'Role',
      'code' => 'Code',
      'name' => 'Role Name',
    ],
  ],
  'crm' => [
    'contact' => [
      'title' => 'Contacts',
      'picture' => 'Picture',
      'name' => 'Contact Name',
      'company' => 'Company',
      'type' => 'Type',
      'address' => 'Address',
      'city' => 'City',
      'postal_code' => 'Postal Code',
      'state' => 'State',
      'country' => 'Country',
      'website' => 'Website',
      'phone' => 'Phone',
      'mobile' => 'Mobile',
      'fax' => 'Fax',
      'email' => 'Email',
      'tax' => 'Tax ID',
      'active' => 'Active',
    ]
  ],
  'setting' => [
    'company' => [
      'title' => 'Company',
      'name' => 'Company Name',
      'picture' => 'Picture',
      'parent' => 'Parent',
      'address' => 'Address',
      'city' => 'City',
      'postal_code' => 'Postal Code',
      'state' => 'State',
      'country' => 'Country',
      'website' => 'Website',
      'phone' => 'Phone',
      'fax' => 'Fax',
      'email' => 'Email',
      'active' => 'Active'
    ],
    'period' => [
      'title' => 'Period',
      'year' => 'Period',
      'month' => 'Month',
      'status' => 'Status'
    ],
    'user' => [
      'title' => 'Users',
      'username' => 'Username',
      'fullname' => 'Full Name',
      'nickname' => 'Nickname',
      'last_activity' => 'Last Activity',
      'password' => 'Password',
      'retypePassword' => 'Retype Password',
      'group' => 'Group',
      'company' => 'Company',
      'email' => 'Email',
      'picture' => 'Picture',
      'active' => 'Active'
     ],
     'workflow' => [
       'title' => 'Workflow',
       'code' => 'Category Code',
       'name' => 'Category Name',
       'workflow' => 'Workflow',
       'next' => 'Next Workflow',
       'description' => 'Description',
       'color' => 'Color',
       'default' => 'Default',
     ]
  ],
  'accounting' => [
    'coa' => [
      'title' => 'Chart Of Account',
      'name' => 'Account Name',
      'code' => 'Account Number',
      'parent' => 'Parent Account',
      'category' => 'Category',
      'normal_balance' => 'Normal Balance',
      'company' => 'Companies'
    ],
  
  ]

];
