<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                  <a href="@{{ response.link+'?'+( (queryString == '') ? 'export=1' : queryString+'&export=1' ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
                </div>
            </div>

            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6" style="padding-left: 2;">
                    Gudang : <select ng-model="warehouse" class="input-sm" ng-options="item.id as item.text for item in response.optionWarehouse" ng-change="warehouseChanged()" ng-init="warehouse = response.optionWarehouse[0].id" style="width: 200px;">
                      <!-- <option value="">-- Semua Tipe --</option> -->
                    </select>
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-12">
                  <div class="table-filter row">
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm right">
                        <i class="fa fa-search"></i>
                        <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                      </div>
                    </div>
                    <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
                        <select ng-model="category" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionCategory" ng-change="categoryChanged()">
                          <option value="">-- Semua Kategori --</option>
                        </select>
                    </div>
                    <div class="col-md-2" style="padding-left: 2;">
                        <select ng-model="type" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionType" ng-change="typeChanged()">
                          <option value="">-- Semua Tipe --</option>
                        </select>
                    </div>

                  </div>
                </div>
              </div>
              <!-- Begin Table -->
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th ng-repeat="column in response.columns">
                        <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                        @{{ column.label }}
                        </div>
                      </th>
                      <th>Satuan Ukuran</th>
                      <th class="text-right">Kedatangan</th>
                      <th class="text-right">Stok</th>
                      <th class="text-right">Pengiriman</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="dt in response.data">
                      <!-- <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td> -->
                      <td>@{{ dt.code }}</td>
                      <td><a href="javascript:;" ng-click="openStockCard(dt.id)">@{{ dt.name }}</a></td>
                      <td>@{{ dt.basic_unit }}</td>
                      <td class="text-right text-stock" ng-click="showPopupForecast(dt.dataForecast)" style="cursor: pointer;">@{{ dt.forecast | number:2 }}</td>
                      <td class="text-right text-stock" ng-click="showPopupStock(dt.dataStock)" style="cursor: pointer;">@{{ dt.stock | number:2 }}</td>
                      <td class="text-right text-stock" ng-click="showPopupReserved(dt.dataReserved)" style="cursor: pointer;">@{{ dt.reserved | number:2 }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    @include('pages.partials.datagrid.datagrid-mass-button')
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->

<!-- POPUP FORECAST TEMPLATE -->
<script type="text/ng-template" id="modalForecast.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Barang Masuk</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div class="table-responsive">
      <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th>Estimasi Kedatangan</th>
            <th>No. Pembelian</th>
            <th>No. Penerimaan</th>
            <th>Pemasok</th>
            <th class="text-right">Jumlah</th>
            <th>Satuan Ukuran</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="dt in data">
            <td>@{{ dt.expected_date }}</td>
            <td>@{{ dt.stock_movement }}</td>
            <td>@{{ dt.receipt_code }}</td>
            <td>@{{ dt.contact }}</td>
            <td class="text-right">@{{ dt.qty | number:2 }}</td>
            <td>@{{ dt.uom }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</script>
<!-- END POPUP FORECAST TEMPLATE -->

<!-- POPUP STOCK TEMPLATE -->
<script type="text/ng-template" id="modalStock.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Persediaan</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>Gudang</th>
          <th class="text-right">Jumlah</th>
          <th>Satuan Ukuran</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="dt in data">
          <td>@{{ dt.warehouse_location_name }}</td>
          <td class="text-right">@{{ dt.qty | number:2 }}</td>
          <td>@{{ dt.uom_name }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</script>
<!-- END POPUP STOCK TEMPLATE -->

<!-- POPUP RESERVED TEMPLATE -->
<script type="text/ng-template" id="modalReserved.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Barang Keluar</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div class="table-responsive">
      <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th>Estimasi Pengiriman</th>
            <th>No. Penjualan</th>
            <th>No. Pengiriman</th>
            <th>Pelanggan</th>
            <th class="text-right">Jumlah</th>
            <th>Satuan Ukuran</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="dt in data">
            <td>@{{ dt.expected_date }}</td>
            <td>@{{ dt.stock_movement }}</td>
            <td>@{{ dt.delivery_code }}</td>
            <td>@{{ dt.contact }}</td>
            <td class="text-right">@{{ dt.qty | number:2 }}</td>
            <td>@{{ dt.uom }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</script>
<!-- END POPUP RESERVED TEMPLATE -->
<style>
td.text-stock:hover{
  color: #408da7 !important;
  font-weight: bold;

}
</style>
