<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\SettingCompany;
use App\Models\SettingWorkflow;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\ProductProduct;
use App\Models\ProductCategory;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Carbon\Carbon;
use Auth;
use Excel;
use PDF;
use Devplus\Params\Params;
use Devplus\Helper\Helper;
use Illuminate\Support\Facades\DB;

class General extends Controller
{
    protected function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

  public function index() {

    $arrayCompany = SettingCompany::where('active', true)->get()->toArray();

    $listCompany = Helper::toOption('id', 'name', $arrayCompany, 'id', 'name');
	
    $currentCompany = Auth::user()->authCompany()->id;
    
    $workflow = SettingWorkflow::where([
        'code' => 'SO'
    ])->first()->toArray();
    $statuses = [];
    if(!empty($workflow)) {
        $statuses = json_decode($workflow['workflow'], true);
    }
    $this->array_sort_by_column($statuses, 'code');

    return response()->json([
      'data' => [
        'listCompany' => $listCompany,
        'currentCompany' => $currentCompany,
        'statuses' => $statuses,
      ]
    ]);
  }

  public function getData(){
              
        $arrayCompany = SettingCompany::where('active', true)->get()->toArray();
        $listCompany = Helper::toFlatOption('id', 'name', $arrayCompany);

        $company = Input::get('company');
        $status = Input::get('status');
        if(empty($status)) {
            $status = [];
        }
        $category = Input::get('category');
        $dateFrom =  Input::get('from');
        $dateTo   = Input::get('to');
                
        if(empty($dateTo)){
            $dateTo = $dateFrom;
        }
        $dateTo = Carbon::parse($dateTo)->addDay(1)->toDateTimeString();
		
        // die($chartDate);

        $totalSales = 0;
        $totalTransaction = 0;
        $averageSales = 0;
        $salesPerDay = [];
        $data = [];
        $totalDay = 0;
        $targetAverageSales = 0;
        $targetSales = 0;
        $targetTransaction = 0;
        $totalSale = [];
        // $totalSaleSangkuriang = 0;
        // $totalSaleNonSangkuriang = 0;

        $targetSalesAmount = 0;
        $targetTransactionAmount = 0;
        $dataChart = [];
        $dataSalesByStore = [];

        for($x = 1; $x <= 24; $x++){
            $hour = str_pad($x, 2, '0', STR_PAD_LEFT);


            $dataChart[$hour] = [
                'hour' => $hour,
                'average_price' => 0,
                'average_qty' => 0
                // 'sangkuriangAverageSalesPrice' => 0,
                // 'sangkuriangAverageSalesQty' => 0,
                // 'nonSangkuriangAverageSalesPrice' => 0,
                // 'nonSangkuriangAverageSalesQty' => 0,
                // 'averageSalesPrice' => 0,
                // 'averageSalesQty' => 0,
            ];
        }

        $dataProductTopSales = [];
		$dataChasierTopSales = [];
		//dd($company);
        if(!empty($company)){
            $data = InventoryStockMovement::select([
                'inventory_stock_movement_product.product',
                'inventory_stock_movement_product.price',
                'inventory_stock_movement_product.qty',
                'inventory_stock_movement_product.total',
                'inventory_stock_movement.id AS stock_movement',
                'inventory_stock_movement.date',
                'inventory_stock_movement.created_at',
                'inventory_stock_movement.cashier_reference',
                'inventory_stock_movement.created_by',
                'product_master.category',
                'product_master.name as product_name',
                'product_master.picture',
                'inventory_stock_movement.company',
                'setting_user.fullname',
            ])
            ->join('inventory_stock_movement_product', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement');
            foreach($status as $codeStatus) {
                $data = $data->leftJoin('inventory_stock_movement_product as ' . $codeStatus, function($join) use ($codeStatus){
                    $join->on('inventory_stock_movement.id', '=', $codeStatus . '.stock_movement'); 
                });
            }
            $data = $data->join('product_product', 'product_product.id', 'inventory_stock_movement_product.product')
                ->join('product_master', 'product_master.id', 'product_product.product')
                            ->join('setting_user', 'setting_user.username', 'inventory_stock_movement.created_by')
                ->where([
                    'inventory_stock_movement.type' => 'SO',
                ])
                ->whereIn('inventory_stock_movement.company', $company)
                ->whereIn('inventory_stock_movement.status', empty($status) ? ['3SO'] : $status)
                ->whereBetween('inventory_stock_movement.date', [$dateFrom, $dateTo])
                ->get();
        }
        //dd(collect($data)->toArray());


        if(count($data) > 0){
            $data = $data->toArray();

            // $listProductId = collect($data)->pluck('product')->unique()->toArray();
            // if(!empty($listProductId)){
            //     $dataProduct = ProductProduct::getData(function($query) use ($listProductId) {
            //         $query->whereIn('product_product.id', $listProductId);
            //         return $query;
            //     }, false);
            //     $dataProduct = ProductProduct::getRelatedData($dataProduct);
            // }

            $dataListTransactionCode = [];

            foreach($data as $key => $val){
                // $product = collect($dataProduct)->filter(function($dt) use($val){
                //     return ($dt['id'] == $val['product']);
                // })->first();
                // if(!empty($product)){
                //     $data[$key]['product_name'] = $product['name'];
                //     $data[$key]['picture'] = $product['picture'];
                // }
                $dataListTransactionCode[] = $val['stock_movement'];

                if(isset($totalSale[$val['category']])){
                  $totalSale[$val['category']] += 1;
                }

            }

            $totalTransaction = count($dataListTransactionCode);
            foreach($data as $val){
                // Calculate Sales Total, Transaction Total and Sales Average


                $dateNow = Carbon::parse($val['date'])->toDateString();
                if(!isset($salesPerDay[$dateNow])){
                  $salesPerDay[$dateNow] = 0;
                }
                $salesPerDay[$dateNow] += $val['total'];

                // Calculate Serial Chart
                $hour = Carbon::parse($val['created_at'])->format('H');
                
                if(isset($dataChart[$hour])){
                    $dataChart[$hour]['average_price'] += $val['total'];
                    $dataChart[$hour]['average_qty'] += $val['qty'];
                }
                $totalSales += $val['total'];
                if(!isset($dataProductTopSales[$val['product']])){
                    $dataProductTopSales[$val['product']] = [
                        'name' => $val['product_name'],
                        'picture' => $val['picture'],
                        'qty' => 0,
                        'total' => 0
                    ];
                }
                
                $dataProductTopSales[$val['product']]['qty'] += $val['qty'];
                $dataProductTopSales[$val['product']]['total'] += $val['total'];

                if(!isset($dataChasierTopSales[$val['created_by']])){
                    $dataChasierTopSales[$val['created_by']] = [
                        'name' => $val['fullname'],
                        'qty' => 0,
                        'total' => 0
                    ];
                }
                $dataChasierTopSales[$val['created_by']]['qty'] += $val['qty'];
                $dataChasierTopSales[$val['created_by']]['total'] += $val['total'];
            }
			
            $dataProductTopSales = collect($dataProductTopSales)->sortByDesc('qty')->slice(0, 10);
            $dataProductTopSales = $dataProductTopSales->all();
			
			$dataChasierTopSales = collect($dataChasierTopSales)->sortByDesc('qty')->slice(0, 10);
            $dataChasierTopSales = $dataChasierTopSales->all();
			
			//dd($dataChasierTopSales);

            $totalDay = count($salesPerDay);
            if(count($salesPerDay) > 0){
                $averageSales = collect($salesPerDay)->sum() / count($salesPerDay);
            }

            $targetSales = $this->calculateTarget($totalSales, $targetSalesAmount);
            $targetAverageSales = $this->calculateTarget($averageSales, $targetSalesAmount);
            $targetTransaction = $this->calculateTarget($totalTransaction, $targetTransactionAmount);
        }

        $dataChart = array_values($dataChart);
        $dataProductTopSales = array_values($dataProductTopSales);
		$dataChasierTopSales = array_values($dataChasierTopSales);
		//dd($dataChasierTopSales);
        $dataSalePercentage = [];
        foreach($totalSale as $ksale => $vsale){
          try{
            $catName = $optionCategory[$ksale];
            $dataSalePercentage[] = [
              'category' => $catName,
              'transactions' => $vsale
            ];
          }catch(\Exception $e){}
        }

        $dataResponse = [
            'totalSales' => $totalSales,
            'totalTransaction' => $totalTransaction,
            'averageSales' => ($totalSales > 0 && $totalTransaction > 0) ? ($totalSales / $totalTransaction) : 0, // $averageSales
            'targetSales' => $targetSales,
            'targetTransaction' => $targetTransaction,
            'targetAverageSales' => ($targetSales > 0 && $targetTransaction > 0) ? ($targetSales / $targetTransaction) : 0, // $targetAverageSales
            'dataChart' => $dataChart,
            'dataProductTopSales' => $dataProductTopSales,
            'dataSalesPercentage' => $dataSalePercentage,
            'dataChasierTopSales' => $dataChasierTopSales,
            'dataSalesByStore' => array_values($dataSalesByStore),
			
            // 'optionCategory' => $optionCategory
        ];


        return response()->json($dataResponse);

    }

  public function calculateTarget($total, $target){
    if($target == 0){
      return 0;
    }
    $percentage = ($total / $target) * 100;
    return $percentage;
  }

  protected function randomColorPart() {
      return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
  }

  public function randomColor() {
      return '#' . $this->randomColorPart() . $this->randomColorPart() . $this->randomColorPart();
  }

  public function download(){
    $company = Input::get('company');
    $status = Input::get('status');
    if(empty($status)) {
        $status = '';
    }
    $dateFrom = Input::get('from');
    $dateTo = Input::get('to');
    $format = Input::get('format');
			
    if(empty($dateTo)){
        $dateTo = $dateFrom;
    }
    $dateTo = Carbon::parse($dateTo)->addDay(1)->toDateTimeString();

    $listCompany = explode('|', $company);
    $listStatus = array_filter(explode('|', $status), 'strlen');

    $data = InventoryStockMovement::select([
        'inventory_stock_movement.code',
        'inventory_stock_movement.date',
        'crm_contact.name as customer_name',
        'setting_user.fullname',
        'product_master.name as product_name',
        'inventory_stock_movement_product.qty',
        'inventory_stock_movement.status',
    ])
    ->join('inventory_stock_movement_product', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement');
    foreach($listStatus as $codeStatus) {
        $data = $data->leftJoin('inventory_stock_movement_product as ' . $codeStatus, function($join) use ($codeStatus){
            $join->on('inventory_stock_movement.id', '=', $codeStatus . '.stock_movement'); 
        });
    }
    $data = $data->join('product_product', 'product_product.id', 'inventory_stock_movement_product.product')
    ->join('product_master', 'product_master.id', 'product_product.product')
    ->join('crm_contact', 'crm_contact.id', 'inventory_stock_movement.contact')
    ->join('setting_user', 'setting_user.username', 'inventory_stock_movement.created_by')
    ->where([
        'inventory_stock_movement.type' => 'SO',
    ])
    ->whereIn('inventory_stock_movement.company', $listCompany)
    ->whereIn('inventory_stock_movement.status', empty($listStatus) ? ['3SO'] : $listStatus)
    ->whereBetween('inventory_stock_movement.date', [$dateFrom, $dateTo])
    ->get()->toArray();

    $setting_workflow = SettingWorkflow::where('code', 'SO')->first();
    $raw_workflow = json_decode($setting_workflow->workflow, true);
    $workflow = [];

    foreach($raw_workflow as $wf) {
        $workflow[$wf['code']] = $wf['label'];
    }

    foreach($data as $key => $val){
      $data[$key]['date'] = Carbon::parse($val['date'])->format('d/m/Y');
      if(isset($workflow[$val['status']])) {
        $data[$key]['status'] = $workflow[$val['status']];
      }
    }

    $comp = SettingCompany::select(['id', 'name'])->whereIn('id', $listCompany)->get();
    $comp = collect($comp)->pluck('name')->toArray();
    $listCompany = implode(', ', $comp);

    if($format == 'excel') {

        Excel::create('Laporan Penjualan '.date('Y-m-d', strtotime($dateFrom)).' - '.date('Y-m-d', strtotime($dateTo)), function($excel) use ($data, $listCompany, $dateFrom, $dateTo) {

          $excel->sheet('Report', function($sheet) use ($data, $listCompany, $dateFrom, $dateTo) {
              $excelOffice = 'Kantor: ';
              if($listCompany) {
                $excelOffice .= $listCompany;
              }

              $rows = array();
              $rows[] = ['Laporan Penjualan', $excelOffice];
              $rows[] = [null, 'Tanggal: '.date('d/m/Y', strtotime($dateFrom)).' - '.date('d/m/Y', strtotime($dateTo))];
              $rows[] = [];
            $rows[] = ['No. Sales Order','Tgl. Sales Order','Customer','Salesman','Product','Jumlah','Status Sales Order'];

              foreach($data as $val) {
                $rows[] = [
                    $val['code'],
                    $val['date'],
                    $val['customer_name'],
                    $val['fullname'],
                    $val['product_name'],
                    $val['qty'],
                    $val['status']
                ];
              }

              $sheet->fromArray($rows, null, 'A1', true, false);

              $sheet->getStyle('A1')->applyFromArray(array(
                'font' => array(
                    'bold' => true,
                    'size' => 14
                )
              ));

              $sheet->mergeCells('B1:G1');
              $sheet->mergeCells('B2:G2');
          });
        })->export('xls');

    } else {
        
        $pdf = PDF::loadView('pages.dashboard-export', compact('data', 'listCompany', 'dateFrom', 'dateTo'));
        return $pdf->download('Laporan Penjualan '.date('Y-m-d', strtotime($dateFrom)).' - '.date('Y-m-d', strtotime($dateTo)).'.pdf');
        
        //return view('pages.dashboard-export', compact('data', 'listCompany', 'dateFrom', 'dateTo'));     
    }
  }
}
