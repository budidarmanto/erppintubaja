@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <span>{!! $error !!}</span>
        @endforeach
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        <span>{!! session()->get('success') !!}</span>
    </div>
@endif
