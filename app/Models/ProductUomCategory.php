<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductUomCategory extends DevplusModel
{
    protected $table = 'product_uom_category';
    protected $fillable = ['id', 'name'];

    public static function getOption($model = null){
      if($model == null){
          $data = ProductUomCategory::orderBy('name')->get();
      }else{
        $data = $model->orderBy('name')->get();
      }

      return Helper::toOption('id', 'name', $data, 'id', 'text');
    }

}
