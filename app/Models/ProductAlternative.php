<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductAlternative extends DevplusModel
{
  protected $table = 'product_alternative';
  protected $fillable = ['id', 'product_master', 'product'];
}
