<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class InventoryWarehouseLocation extends DevplusModel
{
  protected $table = 'inventory_warehouse_location';
  protected $fillable = ['id', 'warehouse', 'name', 'active', 'pos'];

  public static function getOption($warehouse = null, $model = null){
    $company = Helper::currentCompany();
    $dataWarehouse = [];
    if($model == null){
        $query = InventoryWarehouseLocation::select([
          'inventory_warehouse_location.id',
          'inventory_warehouse_location.name',
          'inventory_warehouse.id AS warehouse',
          'inventory_warehouse.name AS warehouse_name',
          'inventory_warehouse.company',
        ])
        ->join('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_warehouse_location.warehouse');
        if(!empty($warehouse)){
          $query->where('inventory_warehouse.id', $warehouse);
        }
        $data = $query->orderBy('name')->get();
    }else{
      $data = $model->orderBy('name')->get();
    }

    foreach($data as $val){
      $listCompany = json_decode($val->company, true);
      if(!empty($listCompany) && in_array($company, $listCompany)){
        $val['name'] = $val['warehouse_name'].'/'.$val['name'];
        $dataWarehouse[] = $val;
      }
    }

    $optionWarehouseLoc = [];
    if(count($dataWarehouse) > 0){
      foreach($dataWarehouse as $val){
        $optionWarehouseLoc[] = [
          'id' => $val['id'],
          'text' => $val['name'],
          'warehouse' => $val['warehouse']
        ];
      }
    }

    return $optionWarehouseLoc;
  }

}
