<?php

namespace App\Http\Controllers\Product;

use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Uom extends Controller
{

  public function index() {

    $dg = Datagrid::source(ProductUomCategory::query());
    $dg->title('Satuan Ukuran');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('name', 'Nama Satuan Ukuran');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new ProductUomCategory());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        if(!empty($form->dataPost['dataUom'])){
          foreach($form->dataPost['dataUom'] as $val){
            $uom = new ProductUom();
            $val['uom_category'] = $form->model->id;
            $uom->fill($val);
            $uom->save();
          }
        }

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionUomType'] = Params::getOption('OPTION_UOM_TYPE');
    $dataForm['dataUom'] = [];
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(ProductUomCategory::find($id));
    $dataForm = $form->build();


    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        $listId = [];
        if(!empty($form->dataPost['dataUom'])){
          foreach($form->dataPost['dataUom'] as $val){
            $uom = new ProductUom();
            $val['uom_category'] = $form->model->id;
            if(!empty($val['id'])){
              $uom = ProductUom::find($val['id']);
            }
            $uom->fill($val);
            $uom->save();
            $listId[] = $uom->id;
          }
        }
        ProductUom::where('uom_category', $id)->whereNotIn('id', $listId)->delete();

        $dataUom = ProductUom::select(['id', 'code', 'name', 'type', 'ratio', 'active', 'uom_category'])->where('uom_category', $id)->get()->toArray();

        return response()->json([
          'status' => true,
          'dataUom' => $dataUom
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionUomType'] = Params::getOption('OPTION_UOM_TYPE');
    $dataUom = ProductUom::select(['id', 'code', 'name', 'type', 'ratio', 'active', 'uom_category'])->where('uom_category', $id)->get()->toArray();
    $dataForm['dataUom'] = $dataUom;
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    ProductUomCategory::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Satuan Ukuran');
    $form->add('name', 'Nama Satuan Ukuran', 'text')->rule('required|max:100');
    return $form;
  }

}
