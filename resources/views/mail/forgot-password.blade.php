@include('mail.header')
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <meta itemprop="name" content="Confirm Email"/>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-block">
                                        <h3>Hello, {!! $user->user_full_name !!}</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        There was recently a request to change the password for your account.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        If you requested this password change, please reset your password here:
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block" itemprop="handler">
                                        <a href="{!! $link !!}" class="btn-primary">Reset Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        If you did not make this request, you can ignore this message and your password will remain the same.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block">support@devplus.id</td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
        <td></td>
    </tr>
</table>
@include('mail.footer')
