/* Setup blank page controller */
angular.module('DevplusApp').controller('FormGroupController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$filter', function($rootScope, $scope, settings, response, $http, $location, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataDetail = response.dataDetail;
    var dataGroup = $scope.data.access || [];
    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    if(dataGroup.length <= 0){
        angular.forEach($scope.dataDetail, function(application){
          angular.forEach(application.module, function(module){
            dataGroup.push({
              application: application.code,
              module: module.code,
              role: []
            });
          });
        });
    }

    $scope.isChecked = function(application, module, role){

       var target = $filter('filter')(dataGroup, {
        application: application,
        module: module
      }, true);

       if(target.length > 0){
         if(target[0].role.indexOf(role) != -1){
           return true;
         }
       }
       return false;
    }

    $scope.changeSwitch = function(application, module, role){
       var target = $filter('filter')(dataGroup, {
          application: application,
          module: module
       }, true);

       if(target.length > 0){
           if(target[0].role.indexOf(role) == -1){
             target[0].role.push(role);
           }else{
             target[0].role.splice(target[0].role.indexOf(role), 1);
           }
       }else{
         dataGroup.push({
           application: application,
           module: module,
           role: [role]
         });
       }
    }


    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.access = dataGroup;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);
