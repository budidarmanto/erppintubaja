<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Auth;
use Helper;

class SettingPeriod extends DevplusModel
{
  protected $table = 'setting_period';
  protected $fillable  = ['year','month','status'];

  public static function boot(){

      parent::boot();

      static::creating(function($table){
          $table->status = 0;
          $table->created_by = (Auth::check()) ? Auth::user()->username : 'SYSTEM';
      });

      static::updating(function($table){
          $table->updated_by = (Auth::check()) ? Auth::user()->username : 'SYSTEM';
      });

  }

  public static function getOptionYear(){
    $data = SettingPeriod::get();

    return Helper::toOption('year', 'year', $data);
  }


  public static function getOptionMonth(){
      $list_month = [];
      for ($i = 1; $i <= 12; $i++) {
          $month = date('F', mktime(0, 0, 0, $i, 1, date('Y')));
          $list_month[str_pad($i, 2, '0', STR_PAD_LEFT)] = $month;
      }
      $option = [];
      foreach($list_month as $key => $month){
        $option[] = [
          'id' => $key,
          'text' => $month
        ];
      }
      return $option;

  }
}
