<?php

namespace App\Http\Controllers\Product;

use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Attribute extends Controller
{

  public function index() {

    $dg = Datagrid::source(ProductAttribute::query());
    $dg->title('Varian');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('name', 'Nama Varian');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new ProductAttribute());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        if(!empty($form->dataPost['dataAttribute'])){
          foreach($form->dataPost['dataAttribute'] as $val){
            $uom = new ProductAttributeValue();
            $val['attribute'] = $form->model->id;
            $uom->fill($val);
            $uom->save();
          }
        }

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['dataAttribute'] = [];
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(ProductAttribute::find($id));
    $dataForm = $form->build();


    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        $listId = [];
        if(!empty($form->dataPost['dataAttribute'])){
          foreach($form->dataPost['dataAttribute'] as $val){
            $uom = new ProductAttributeValue();
            $val['attribute'] = $form->model->id;
            if(!empty($val['id'])){
              $uom = ProductAttributeValue::find($val['id']);
            }
            $uom->fill($val);
            $uom->save();
            $listId[] = $uom->id;
          }
        }
        ProductAttributeValue::where('attribute', $id)->whereNotIn('id', $listId)->delete();

        $dataAttribute = ProductAttributeValue::select(['id', 'name', 'attribute'])->where('attribute', $id)->get()->toArray();

        return response()->json([
          'status' => true,
          'dataAttribute' => $dataAttribute
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataAttribute = ProductAttributeValue::select(['id', 'name', 'attribute'])->where('attribute', $id)->get()->toArray();
    $dataForm['dataAttribute'] = $dataAttribute;
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    ProductAttribute::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Varian');
    $form->add('name', 'Nama Varian', 'text')->rule('required|max:100');
    return $form;
  }

}
