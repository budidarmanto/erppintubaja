/* Setup blank page controller */
angular.module('DevplusApp').controller('FormProfileController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', function($rootScope, $scope, settings, response, $http, $location, $uibModal) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    // Handle Password
    $scope.showPopupPassword = function(dataSupplier){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalPassword.html',
        controller: 'ModalPasswordCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: {},
            };
          }
        }
      });

      modalInstance.result.then(function (data) {

      });
    }

    $scope.deleteSupplier = function(supplier){
      $scope.dataSupplier.splice($scope.dataSupplier.indexOf(supplier), 1);
    }
    // End Handle Password

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
              setTimeout(function(){
                window.location.reload();
              }, 500);
            }else{

              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);

// Supplier Popup Controller
angular.module('DevplusApp').controller('ModalPasswordCtrl', ['$scope', '$uibModalInstance', '$http','params', function($scope,  $uibModalInstance, $http, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'old_password', label: 'Password Lama', type: 'password', attributes: {required: ''}},
    {name: 'new_password', label: 'Password Baru', type: 'password', attributes: {required: '', minlength: 6, maxlength: 60}},
    {name: 'enter_newpassword', label: 'Ketik Ulang Password Baru', type: 'password', attributes: {required: '', equalTo: "input[name='new_password']"}},
  ];

  $scope.save = function(data, $event){


    if($('#formChangePassword').valid()){
      var btn = Ladda.create($event.currentTarget);
      btn.start();
      $http.post('change-password', $scope.data).then(function(resp){
        if(resp.data.status){
          toastr['success'](resp.data.messages, "Saved");
          $uibModalInstance.close($scope.data);
        }else{
          toastr['error'](resp.data.messages, "Failed");
        }
        btn.stop();
      });
    }
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End Supplier Popup Controller
