<?php

 namespace App\Http\Controllers\Sales;

 use App\Models\CrmContact;
 use App\Models\CrmContactDetail;
 use App\Models\SettingCompany;
 use App\Http\Controllers\Controller;
 use FormBuilder;
 use Datagrid;
 use Request;
 use Input;
 use Params;
 use Helper;

class Customer extends Controller{

  public function index() {
    $dg = Datagrid::source(CrmContact::where('type', 'customer'));
    $dg->title('Pelanggan');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('name', 'Nama Pelanggan');
    $dg->add('address', 'Alamat');
    $dg->add('phone', 'Telepon');
    $dg->add('active', 'Status')->options([true => 'Aktif', false => 'Tidak Aktif']);
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new CrmContact());
    $form->pre( function($dataPost){
       $dataPost['type'] = 'customer';
       $dataPost['revision'] = 1;
       return $dataPost;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        //save uom detail
        if(!empty($form->dataPost['dataContact'])){
          foreach($form->dataPost['dataContact'] as $val){
            $uom = new CrmContactDetail();
            $val['contact'] = $form->model->id;
            $uom->fill($val);
            $uom->save();
          }
        }

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'error' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataContact'] = [];
    return response()->json($dataForm);
  }

  public function modify($id = null){
    $customer = CrmContact::find($id);
    $form = $this->anyForm($customer);
    $dataForm = $form->build();
    $form->pre( function($dataPost) use ($customer){
       $dataPost['type'] = 'customer';
       $dataPost['revision'] = $customer->revision + 1;
       return $dataPost;
    });
    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        $listId = [];
        if(!empty($form->dataPost['dataContact'])){
          foreach($form->dataPost['dataContact'] as $val){
            $uom = new CrmContactDetail();
            $val['contact'] = $form->model->id;
            if(!empty($val['id'])){
              $uom = CrmContactDetail::find($val['id']);
            }
            $uom->fill($val);
            $uom->save();
            $listId[] = $uom->id;
          }
        }
        CrmContactDetail::where('contact', $id)->whereNotIn('id', $listId)->delete();

        $dataContact = CrmContactDetail::select(['id','name','type', 'position', 'address', 'city', 'postal_code', 'state', 'country', 'website', 'phone', 'mobile', 'fax', 'email'])->where('contact', $id)->get()->toArray();

        return response()->json([
          'status' => true,
          'dataContact' => $dataContact
        ]);
      }else{
        return response()->json([
          'error' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataContact = CrmContactDetail::select(['id','name','type', 'position', 'address', 'city', 'postal_code', 'state', 'country', 'website', 'phone', 'mobile', 'fax', 'email'])->where('contact', $id)->get()->toArray();
    $dataForm['dataContact'] = $dataContact;
    return response()->json($dataForm);
  }

  public function getDataResponse($dataForm){
    // $optionCompany = SettingCompany::getOption();
    // $dataForm['optionCompany'] = $optionCompany;OPTION_CONTACT_TYPE
    $dataForm['optionCountry'] = Params::getOption('OPTION_COUNTRY');
    $dataForm['optionContactType'] = Params::getOption('OPTION_CONTACT_TYPE');
    $dataForm['optionType'] = Params::getOption('OPTION_CRM_TYPE');
    return $dataForm;
  }

  public function delete(){
    $id = Input::get('id');
    CrmContact::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
  $form = FormBuilder::source($source);
    $form->title('Pelanggan');
    $form->add('picture', 'Gambar Pelanggan', 'image')->attributes([
      'moveTo' => 'img/contact'
    ]);
    $form->add('name', 'Nama Pelanggan', 'text')->rule('required|max:50');
    // $form->add('company', __('app.crm.contact.company'), 'select')->attributes([
    //   'ng-options' => 'item.id as item.text for item in response.optionCompany',
    // ]);
    // $form->add('type', __('app.crm.contact.type'), 'select')->attributes([
    //   'ng-options' => 'item.id as item.text for item in response.optionType',
    // ]);
    $form->add('address', __('app.crm.contact.address'), 'textarea');
    $form->add('city', __('app.crm.contact.city'), 'text')->rule('max:50');
    $form->add('postal_code', __('app.crm.contact.postal_code') ,'text')->rule('max:5');
    $form->add('state', __('app.crm.contact.state'), 'text')->rule('max:50');
    $form->add('country', __('app.crm.contact.country'), 'text')->rule('max:50');
    $form->add('website', __('app.crm.contact.website'), 'text')->rule('max:255');
    $form->add('phone', __('app.crm.contact.phone'), 'text')->rule('max:50');
    $form->add('mobile', __('app.crm.contact.mobile'), 'text')->rule('max:50');
    $form->add('fax', __('app.crm.contact.fax'), 'text')->rule('max:50');
    $form->add('email', __('app.crm.contact.email'), 'text')->rule('max:255');
    $form->add('tax_no', __('app.crm.contact.tax'), 'text')->rule('max:50');
    $form->add('active' , __('app.crm.contact.active'), 'switch');
    return $form;
  }
}
