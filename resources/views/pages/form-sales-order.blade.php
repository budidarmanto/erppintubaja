<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <form role="form" id="@{{ response.id }}" id="#formSalesOrder">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <!-- <i class="icon-settings font-green-sharp"></i> -->
                        <span class="caption-subject bold uppercase">@{{ response.title }}</span>
                        &nbsp;&nbsp;
                        <span class="btn-status" style="background: @{{ getStatus(data.status).color }}; color: #fff; padding: 3px 6px;" ng-hide="response.action == 'create'">@{{ getStatus(data.status).label }}</span>
                    </div>

                    @include('pages.partials.form.form-action')
                    <div class="actions" ng-hide="getStatus(data.status).text.length <= 0 || response.action == 'create' || response.role.indexOf('change') == -1" style="margin-left: 40px;float: left;">
                    <a href="#" onclick="return false;" class="btn blue btn-outline mt-ladda-btn ladda-button" ng-repeat="next in getStatus(data.status).next" data-style="zoom-out"  ng-click="changeStatus(next.id, $event)" style="margin-right: 5px;" >@{{ next.text }}</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row margin-bottom-5 margin-top-20">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <form-builder field="code"></form-builder>
                                    <form-builder field="date"></form-builder>
                                    <!-- <form-builder field="diajukan"></form-builder> -->
									<form-builder field="description"></form-builder>
                                </div>
                                <div class="col-md-6">
                                    <form-builder field="contact"></form-builder>
                                    <form-builder field="expected_date"></form-builder>
									
                                    <form-builder field="source"></form-builder>
                                    <form-builder field="shipment_address"></form-builder>
                                    <form-builder field="created_by"></form-builder>
									
                                    <form-builder field="currency"></form-builder>
                                    <div ng-hide="data.currency == response.defaultCurrency">
                                        <form-builder field="currency_rate"></form-builder>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
      
        <div class="portlet">
              <div class="portlet-title">
                <div class="caption">Bahan/Jasa</div>
                <div class="actions">
                  <a href="#" class="btn btn-sm btn-success" ng-click="showPopupProduct()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                        <th>Barang/Jasa</th>
                        <th>Jumlah</th>
                        <th>Satuan Ukuran</th>
                        <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="product in dataProduct" ng-dblclick="showPopupProduct(product)">
                        <td>@{{ product.name }}</td>
                        <td>
                          @{{ product.qty | number:2 }}
                        </td>
                        <td>
                          @{{ product.uom_name }}
                        </td>
                        <td class="text-center" ng-if="response.action!='view'">
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupProduct(product)">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteProduct(product)">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

    </div>
</div>
<!-- END MAIN CONTENT -->
<!-- EDIT PRODUCT POPUP TEMPLATE -->
<script type="text/ng-template" id="modalProduct.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Produk</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <form-builder field="product"></form-builder>
    <div class="row">
      <div class="col-md-6">
        <form-builder field="qty"></form-builder>
      </div>
      <div class="col-md-6">
        <form-builder field="uom"></form-builder>
      </div>
	  <div class="col-md-6">
        <form-builder field="door_height"></form-builder>
      </div>
	  <div class="col-md-6">
		<form-builder field="door_height_pic" ></form-builder>
	  </div>
	   <div class="col-md-6">
        <form-builder field="door_width"></form-builder>
      </div>
	  <div class="col-md-6">
		<form-builder field="door_width_pic" ></form-builder>
	  </div>
	   <div class="col-md-6">
        <form-builder field="frame_height"></form-builder>
      </div>
	  <div class="col-md-6">
		<form-builder field="frame_height_pic" ></form-builder>
	  </div>
	  <div class="col-md-6">
        <form-builder field="frame_width"></form-builder>
      </div>
	  <div class="col-md-6">
		<form-builder field="frame_width_pic" ></form-builder>
	  </div>
	  <div class="col-md-6">
		<form-builder field="door_direction" ></form-builder>
	  </div>
	  <div class="col-md-6">
		<form-builder field="door_installation" ></form-builder>
	  </div>
	  
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT PRODUCT POPUP TEMPLATE -->



