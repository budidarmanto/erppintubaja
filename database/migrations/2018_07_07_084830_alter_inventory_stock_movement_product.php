<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInventoryStockMovementProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_stock_movement_product', function (Blueprint $table) {
			$table->decimal('door_height',18,2)->default(0);
			$table->string('door_height_pic', 225)->nullable();			
			$table->decimal('door_width',18,2)->default(0);
			$table->string('door_width_pic', 225)->nullable();		
			$table->decimal('frame_height',18,2)->default(0);
			$table->string('frame_height_pic', 225)->nullable();	
			$table->decimal('frame_width',18,2)->default(0);
			$table->string('frame_width_pic', 225)->nullable();
			$table->string('door_direction', 3)->nullable();
			$table->string('door_installation', 3)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_stock_movement_product', function (Blueprint $table) {
            //
        });
    }
}
