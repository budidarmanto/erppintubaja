<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          <div class="actions">
            <button class="btn btn-sm green-haze mt-ladda-btn ladda-button" type="submit" ng-click="save(data, $event)" data-style="zoom-out" ng-hide="response.action == 'view'"><span class="ladda-label">Simpan &nbsp;&nbsp;<i class="fa fa-floppy-o"></i></span></button>
          </div>

        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-4 text-center">
              <form-builder field="picture"></form-builder>
              <a href="javsacript:;" ng-click="showPopupPassword()">Ubah Password</a>
            </div>
            <div class="col-md-8">
              <form-builder field="fullname"></form-builder>
              <form-builder field="nickname"></form-builder>
              <form-builder field="username"></form-builder>
              <form-builder field="email"></form-builder>
              <form-builder field="group"></form-builder>
              <form-builder field="company"></form-builder>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
<!-- EDIT ALTERNATIVE POPUP TEMPLATE -->
<script type="text/ng-template" id="modalPassword.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Ubah Password</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <form id="formChangePassword">
      <div ng-repeat="field in response.fields">
        <form-builder field="@{{ field.name }}"></form-builder>
      </div>
    </form>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze mt-ladda-btn ladda-button" type="submit" ng-click="save(data, $event)" data-style="zoom-out"><span class="ladda-label">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></span></button>
  </div>
</script>
<!-- END EDIT ALTERNATIVE POPUP TEMPLATE -->
