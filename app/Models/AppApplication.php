<?php

namespace App\Models;

use Devplus\Model\DevplusModel;

class AppApplication extends DevplusModel
{
    protected $table = 'app_application';
    protected $fillable = ['id', 'code', 'name'];

    public function module()
    {
      return $this->hasMany('App\Models\AppModule', 'application');
    }
}
