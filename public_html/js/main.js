/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var DevplusApp = angular.module("DevplusApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "angularFileUpload",
    "angularMoment",
    "xeditable"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
DevplusApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
DevplusApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

// Http Error Handling
DevplusApp.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($q) {
    return {
      response: function(response){
        if(typeof response.data.status != 'undefined'){
          if(response.data.status == 'noAccess'){
            window.location.hash = 'no-access';
          }
        }

        if(response.status == 404){
          window.location.hash = 'not-found';
        }
        if(response.status == 500){
          var url = 'http://'+window.location.host+'/'+response.config.url;
          $('#modalError').modal('show');
          $('#modalError').find('#linkError').text(url);
          return;
          // window.location.hash = 'error';
        }
        return response;
      },
      // This is the responseError interceptor
      responseError: function(rejection) {
        // if (rejection.status > 399) { // assuming that any code over 399 is an error
        //   $q.reject(rejection)
        // }
        console.log(rejection);
        if(rejection.status == 401){
          window.location.reload();
        }
        if(rejection.status == 500){
          var url = 'http://'+window.location.host+rejection.config.url;
          $('#modalError').modal('show');
          $('#modalError').find('#linkError').text(url);
        }

        return rejection;
      }
    };
  });
});

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
DevplusApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        // assetsPath: '',
        // globalPath: '../assets/global',
        // layoutPath: '../assets/layouts/layout',
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
DevplusApp.controller('AppController', ['$scope', '$rootScope', '$http', '$filter', '$location', function($scope, $rootScope, $http, $filter, $location) {

    $scope.notEmptyProduct = function(dataProduct){
      if(dataProduct.length > 0){

          var status = true;
          angular.forEach(dataProduct, function(val){
            if(val.qty == 0){
              status = false;
              toastr['warning']("Jumlah "+val.name+" tidak boleh kosong!", "Warning");
            }else if(val.qty <= 0){
              status = false;
              toastr['warning']("Jumlah "+val.name+" harus lebih dari 0 (NOL)!", "Warning");
            }
          });
          if(!status){
            return false;
          }
      }else{
        toastr['warning']("Barang/Jasa harus diisi!", "Warning");
        return false;
      }

      return true;
    }

    $scope.$on('$viewContentLoaded', function() {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive

    });

    $scope.lockScreen = function(){
      $('#modalLockScreen').modal({
        backdrop: 'static',
        keyboard: false
      });

      $('#modalLockScreen').modal('show');
    }


    $scope.refreshSidebarAlert = function(){
      $http.get('general/count-alert').then(function(resp){
        $scope.sidebarAlert = resp.data;
      });
    }
    $scope.refreshSidebarAlert();

    setInterval(function(){
      $scope.refreshSidebarAlert();
    }, 300000);

    $scope.validateNumber = function(data) {
        if(isNaN(data)){
          return false;
        }
    };

    $scope.changeCompany = function(company){
      // change-company/{{ $val['id'] }}
      App.blockUI({
        animate: true
      });
      $http.get('change-company/'+company).then(function(resp){
        var locsplit = $location.path().split('/');
        window.location.href = 'http://'+window.location.host+'/#/'+locsplit[1]+'/'+locsplit[2];
        window.location.reload();
      });
    }

    $scope.handleDatePickers = function(event){
      setTimeout(function(){
        var el = $(event.currentTarget).parent().find('input');
        el.datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
            }
        });
        el.on("dp.change", function (e) {
          console.log(e.date.toDate());
          var date = moment(e.date.toDate()).format('DD/MM/YYYY');
          el.val(date);
          el.trigger('change');
        });
      }, 1);
    }

    $scope.confirmChangeStatus = function(status, optionWorkflow, success){
      var nextStatus = $filter('filter')(optionWorkflow, {
        id: status
      });

      swal({
        title: 'Ubah Status',
        text: "Ubah status menjadi "+nextStatus[0].label+'?',
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Ubah',
        cancelButtonText: 'Batalkan',
      },
      function(){

        success(function(){
          swal({
            title: "Berhasil!",
            text: "Status berhasil diubah menjadi "+nextStatus[0].label+".",
            timer: 1000,
            showConfirmButton: false,
            type: 'success'
          });
        });
      });
    }

    $scope.printPage = function(url){
      $('#printFrame').attr('src', url);
    }

}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
DevplusApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
DevplusApp.controller('SidebarController', ['$state', '$scope', function($state, $scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar($state); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
DevplusApp.controller('QuickSidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
       setTimeout(function(){
            QuickSidebar.init(); // init quick sidebar
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
DevplusApp.controller('ThemePanelController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
DevplusApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
DevplusApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/my-profile");

    // Default Resolve Param
    // Async to Requested URL
    var $delegate = $stateProvider.state;
    $stateProvider.state = function(name, definition) {
        definition.resolve = angular.extend({}, definition.resolve, {
          response : function($q, $location, $http){
            var url = $location.url();
            return $http.get(url).then(function(response){

              return response.data;
            });
          }
        });

        return $delegate.apply(this, arguments);
    };


    var loadController = function(files){

      return ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
              name: 'DevplusApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: files
          });
      }];
    }

    var defaultGridOpt = function(url, templateUrl, controller){
      if(!templateUrl){
        templateUrl = 'template/datagrid';
      }
      if(!controller){
        controller = "DatagridController";
      }
      return {
        url: url,
        templateUrl: templateUrl,
        controller: controller,
        resolve: {
            deps: loadController([
                'js/controllers/'+controller+'.js?v=2'
            ])
        }
      }
    }

    var defaultFormOpt = function(url, templateUrl, controller){
      if(!templateUrl){
        templateUrl = 'template/form';
      }
      if(!controller){
        controller = "FormController";
      }

      return {
        url: url,
        templateUrl: templateUrl,
        controller: controller,
        resolve: {
            deps: loadController([
                'js/controllers/'+controller+'.js?v=2'
            ])
        }
      }
    }

    var loadSettingWorkflow = function(url){
      return {
          url: url,
          templateUrl: "template/form-workflow",
          controller: "FormWorkflowController",
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    name: 'DevplusApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                    files: [
                        'plugins/bootstrap-colorpicker/css/colorpicker.css',

                        'plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                        'js/controllers/FormWorkflowController.js'

                    ]
                }]);
            }]
          }
      };
    }

    var datagridParam = '?keyword&sort&perpage&page';
    var state = [];

    $stateProvider

        // Blank Page
        .state('blank', {
            url: "/blank",
            templateUrl: "template/blank",
            data: {pageTitle: 'Blank Page Template'},
            controller: "BlankController",
            resolve: {
                deps: loadController([
                    'js/controllers/BlankController.js'
                ])
            }
        })
        .state('no-access', {
            url: "/no-access",
            templateUrl: "template/no-access",
            controller: "BlankController",
        })
        .state('not-found', {
            url: "/not-found",
            templateUrl: "template/not-found",
            controller: "BlankController",
        })
        .state('error', {
            url: "/error",
            templateUrl: "template/error",
            controller: "BlankController",
        })

        // Dashboard
        .state('DashboardGeneral', {
            url: '/dashboard/general', // Data => General.php
            templateUrl: 'template/dashboard-general', // template
            controller: 'DashboardGeneralController', // Nama Controller
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'DevplusApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            serie: true,
                            files: [
                              'css/pages/dashboard-general.css',
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.css',
                              'plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                              'plugins/moment.min.js',
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.js',
                              'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                              'plugins/amcharts/amcharts/amcharts.js',
                              'plugins/amcharts/amcharts/serial.js',
                              'plugins/amcharts/amcharts/pie.js',
                              'plugins/amcharts/amcharts/themes/light.js',
                              'plugins/amcharts-directive/amcharts-directive.js?v=3',
                              'js/controllers/DashboardGeneralController.js?v=6',
                            ]
                        });
                }],
            }
        })

        // My Profile
        .state('MyProfile', defaultFormOpt('/my-profile', 'template/form-profile', 'FormProfileController'))

        // AppRole
        .state('AppRole', defaultGridOpt('/app/role'+datagridParam))
        .state('AppRoleCreate', defaultFormOpt('/app/role/create'))
        .state('AppRoleModify', defaultFormOpt('/app/role/modify/{id}'))

        // AppGroup
        .state('AppGroup', defaultGridOpt('/app/group'+datagridParam))
        .state('AppGroupCreate', defaultFormOpt('/app/group/create', 'template/form-group', 'FormGroupController'))
        .state('AppGroupModify', defaultFormOpt('/app/group/modify/{id}', 'template/form-group', 'FormGroupController'))

        // AppApplication
        .state('AppApplication', defaultGridOpt('/app/application'+datagridParam))
        .state('AppApplicationCreate', defaultFormOpt('/app/application/create', 'template/form-application', 'FormApplicationController'))
        .state('AppApplicationModify', defaultFormOpt('/app/application/modify/{id}', 'template/form-application', 'FormApplicationController'))

        // AppConfig
        .state('AppConfig', defaultGridOpt('/app/config'+datagridParam))
        .state('AppConfigCreate', defaultFormOpt('/app/config/create'))
        .state('AppConfigModify', defaultFormOpt('/app/config/modify/{id}'))

        // SettingUser
        .state('SettingUser', defaultGridOpt('/setting/user'+datagridParam, 'template/datagrid-user', 'DatagridUserController'))
        .state('SettingUserCreate', defaultFormOpt('/setting/user/create', 'template/form-user'))
        .state('SettingUserModify', defaultFormOpt('/setting/user/modify/{id}', 'template/form-user'))

        // SettingCompany
        .state('SettingCompany', defaultGridOpt('/setting/company'+datagridParam))
        .state('SettingCompanyCreate', defaultFormOpt('/setting/company/create', 'template/form-company'))
        .state('SettingCompanyModify', defaultFormOpt('/setting/company/modify/{id}', 'template/form-company'))


        // SettingWorkflow
        .state('SettingWorkflow', defaultGridOpt('/setting/workflow'+datagridParam))
        .state('SettingWorkflowCreate', loadSettingWorkflow('/setting/workflow/create'))
        .state('SettingWorkflowModify', loadSettingWorkflow('/setting/workflow/modify/{id}'))

        // SettingCurrency
        .state('SettingCurrency', defaultGridOpt('/setting/currency'+datagridParam))
        .state('SettingCurrencyCreate', defaultFormOpt('/setting/currency/create'))
        .state('SettingCurrencyModify', defaultFormOpt('/setting/currency/modify/{id}'))

        // SettingTax
        .state('SettingTax', defaultGridOpt('/setting/tax'+datagridParam))
        .state('SettingTaxCreate', defaultFormOpt('/setting/tax/create'))
        .state('SettingTaxModify', defaultFormOpt('/setting/tax/modify/{id}'))

        // SettingTax
        .state('SettingEmailtemplate', defaultGridOpt('/setting/email-template'+datagridParam))
        .state('SettingEmailtemplateCreate', defaultFormOpt('/setting/email-template/create'))
        .state('SettingEmailtemplateModify', defaultFormOpt('/setting/email-template/modify/{id}'))

        // CrmContact
        .state('CrmContact', defaultGridOpt('/crm/contact'+datagridParam))
        .state('CrmContactCreate', defaultFormOpt('/crm/contact/create', 'template/form-contact', 'FormContactController'))
        .state('CrmContactModify', defaultFormOpt('/crm/contact/modify/{id}', 'template/form-contact', 'FormContactController'))

        // ProductUom
        .state('ProductUom', defaultGridOpt('/product/uom'+datagridParam))
        .state('ProductUomCreate', defaultFormOpt('/product/uom/create', 'template/form-uom', 'FormUomController'))
        .state('ProductUomModify', defaultFormOpt('/product/uom/modify/{id}', 'template/form-uom', 'FormUomController'))

        // ProductCategory
        .state('ProductCategory', defaultGridOpt('/product/category'+datagridParam))
        .state('ProductCategoryCreate', defaultFormOpt('/product/category/create'))
        .state('ProductCategoryModify', defaultFormOpt('/product/category/modify/{id}'))

        // ProductMaster
        .state('ProductMaster', defaultGridOpt('/product/master'+datagridParam, 'template/datagrid-product', 'DatagridProductController'))
        .state('ProductMasterCreate', defaultFormOpt('/product/master/create', 'template/form-product', 'FormProductController'))
        .state('ProductMasterModify', defaultFormOpt('/product/master/modify/{id}', 'template/form-product', 'FormProductController'))

        // ProductAttribute
        .state('ProductAttribute', defaultGridOpt('/product/attribute'+datagridParam))
        .state('ProductAttributeCreate', defaultFormOpt('/product/attribute/create', 'template/form-attribute', 'FormAttributeController'))
        .state('ProductAttributeModify', defaultFormOpt('/product/attribute/modify/{id}', 'template/form-attribute', 'FormAttributeController'))


        // PurchaseSupplier
        .state('PurchaseSupplier', defaultGridOpt('/purchase/supplier'+datagridParam))
        .state('PurchaseSupplierCreate', defaultFormOpt('/purchase/supplier/create', 'template/form-contact', 'FormContactController'))
        .state('PurchaseSupplierModify', defaultFormOpt('/purchase/supplier/modify/{id}', 'template/form-contact', 'FormContactController'))

        // PurchaseRequestorder
        .state('PurchaseRequestorder', defaultGridOpt('/purchase/request-order'+datagridParam, 'template/datagrid-request-order', 'DatagridRequestOrderController'))
        .state('PurchaseRequestorderCreate', defaultFormOpt('/purchase/request-order/create', 'template/form-request-order', 'FormRequestOrderController'))
        .state('PurchaseRequestorderModify', defaultFormOpt('/purchase/request-order/modify/{id}', 'template/form-request-order', 'FormRequestOrderController'))

        // PurchaseApprovalOrder
        .state('PurchaseApprovalorder', defaultGridOpt('/purchase/approval-order'+datagridParam, 'template/datagrid-approval-order', 'DatagridApprovalOrderController'))
        .state('PurchaseApprovalorderModify', defaultFormOpt('/purchase/approval-order/modify/{id}', 'template/form-approval-order', 'FormApprovalOrderController'))

        // Purchaseorder
        .state('PurchasePurchaseorder', defaultGridOpt('/purchase/purchase-order'+datagridParam, 'template/datagrid-purchase-order', 'DatagridPurchaseOrderController'))
        .state('PurchasePurchaseorderCreate', defaultFormOpt('/purchase/purchase-order/create', 'template/form-purchase-order', 'FormPurchaseOrderController'))
        .state('PurchasePurchaseorderModify', defaultFormOpt('/purchase/purchase-order/modify/{id}', 'template/form-purchase-order', 'FormPurchaseOrderController'))

        // PurchaseorderLbs
        .state('PurchasePurchaselbs', defaultGridOpt('/purchase/purchase-lbs'+datagridParam, 'template/datagrid-purchase-lbs', 'DatagridPurchaseLbsController'))
        .state('PurchasePurchaselbsCreate', defaultFormOpt('/purchase/purchase-lbs/create', 'template/form-purchase-lbs', 'FormPurchaseLbsController'))
        .state('PurchasePurchaselbsModify', defaultFormOpt('/purchase/purchase-lbs/modify/{id}', 'template/form-purchase-lbs', 'FormPurchaseLbsController'))

        // PurchaseRequestnote
        .state('PurchaseRequestnote', defaultGridOpt('/purchase/request-note'+datagridParam, 'template/datagrid-request-note', 'DatagridRequestNoteController'))
        .state('PurchaseRequestnoteCreate', defaultFormOpt('/purchase/request-note/create', 'template/form-request-note', 'FormRequestNoteController'))
        .state('PurchaseRequestnoteModify', defaultFormOpt('/purchase/request-note/modify/{id}', 'template/form-request-note', 'FormRequestNoteController'))


        // InventoryIncoming
        .state('InventoryIncoming', defaultGridOpt('/inventory/incoming'+datagridParam, 'template/datagrid-incoming', 'DatagridIncomingController'))

        // InventoryReceiptorder
        .state('InventoryReceiptorder', defaultGridOpt('/inventory/receipt-order'+datagridParam, 'template/datagrid-receipt-order', 'DatagridReceiptOrderController'))
        .state('InventoryReceiptorderCreate', defaultFormOpt('/inventory/receipt-order/create/{withOrWithoutPO}', 'template/form-receipt-order', 'FormReceiptOrderController'))
        .state('InventoryReceiptorderModify', defaultFormOpt('/inventory/receipt-order/modify/{id}', 'template/form-receipt-order', 'FormReceiptOrderController'))

        // InventoryStock
        .state('InventoryStock', defaultGridOpt('/inventory/stock'+datagridParam, 'template/datagrid-stock', 'DatagridStockController'))

        // InventoryOutgoing
        .state('InventoryOutgoing', defaultGridOpt('/inventory/outgoing'+datagridParam, 'template/datagrid-outgoing', 'DatagridOutgoingController'))

        // InventoryDeliveryorder
        .state('InventoryDeliveryorder', defaultGridOpt('/inventory/delivery-order'+datagridParam, 'template/datagrid-delivery-order', 'DatagridDeliveryOrderController'))
        .state('InventoryDeliveryorderCreate', defaultFormOpt('/inventory/delivery-order/create/{withOrWithoutSO}', 'template/form-delivery-order', 'FormDeliveryOrderController'))
        .state('InventoryDeliveryorderModify', defaultFormOpt('/inventory/delivery-order/modify/{id}', 'template/form-delivery-order', 'FormDeliveryOrderController'))

        // InventoryWarehouse
        .state('InventoryWarehouse', defaultGridOpt('/inventory/warehouse'+datagridParam))
        .state('InventoryWarehouseCreate', defaultFormOpt('/inventory/warehouse/create', 'template/form-warehouse', 'FormWarehouseController'))
        .state('InventoryWarehouseModify', defaultFormOpt('/inventory/warehouse/modify/{id}', 'template/form-warehouse', 'FormWarehouseController'))

        // InventoryAdjustment
        .state('InventoryAdjustment', defaultGridOpt('/inventory/adjustment'+datagridParam, 'template/datagrid-adjustment', 'DatagridAdjustmentController'))
        .state('InventoryAdjustmentCreate', defaultFormOpt('/inventory/adjustment/create', 'template/form-adjustment', 'FormAdjustmentController'))
        .state('InventoryAdjustmentModify', defaultFormOpt('/inventory/adjustment/modify/{id}', 'template/form-adjustment', 'FormAdjustmentController'))

        // InventoryIncoming
        .state('InventoryIncomingRequest', defaultGridOpt('/inventory/incoming-request'+datagridParam, 'template/datagrid-incoming-request', 'DatagridIncomingRequestController'))

        // InventoryExternaltransfer
        .state('InventoryExternaltransfer', defaultGridOpt('/inventory/external-transfer'+datagridParam, 'template/datagrid-external-transfer', 'DatagridExternalTransferController'))
        .state('InventoryExternaltransferCreate', defaultFormOpt('/inventory/external-transfer/create/{withOrWithoutRN}', 'template/form-external-transfer', 'FormExternalTransferController'))
        .state('InventoryExternaltransferModify', defaultFormOpt('/inventory/external-transfer/modify/{id}', 'template/form-external-transfer', 'FormExternalTransferController'))

        // InventoryInternaltransfer
        .state('InventoryInternaltransfer', defaultGridOpt('/inventory/internal-transfer'+datagridParam, 'template/datagrid-internal-transfer', 'DatagridInternalTransferController'))
        .state('InventoryInternaltransferCreate', defaultFormOpt('/inventory/internal-transfer/create', 'template/form-internal-transfer', 'FormInternalTransferController'))
        .state('InventoryInternaltransferModify', defaultFormOpt('/inventory/internal-transfer/modify/{id}', 'template/form-internal-transfer', 'FormInternalTransferController'))

        // InventoryReturn
        .state('InventoryReturnorder', defaultGridOpt('/inventory/return-order'+datagridParam, 'template/datagrid-return-order', 'DatagridReturnOrderController'))
        .state('InventoryReturnorderCreate', defaultFormOpt('/inventory/return-order/create', 'template/form-return-order', 'FormReturnOrderController'))
        .state('InventoryReturnorderModify', defaultFormOpt('/inventory/return-order/modify/{id}', 'template/form-return-order', 'FormReturnOrderController'))

        // Cashier User
        // SettingUser
        .state('CashierUser', defaultGridOpt('/cashier/user'+datagridParam))
        .state('CashierUserCreate', defaultFormOpt('/cashier/user/create', 'template/form-cashier-user', 'FormCashierUserController'))
        .state('CashierUserModify', defaultFormOpt('/cashier/user/modify/{id}', 'template/form-cashier-user', 'FormCashierUserController'))

        // Cashier Order
        .state('CashierCashierorder', defaultGridOpt('/cashier/cashier-order'+datagridParam, 'template/datagrid-cashier-order', 'DatagridCashierOrderController'))
        .state('CashierCashierorderCreate', defaultFormOpt('/cashier/cashier-order/create', 'template/form-cashier-order', 'FormCashierOrderController'))
        .state('CashierCashierorderModify', defaultFormOpt('/cashier/cashier-order/modify/{id}', 'template/form-cashier-order', 'FormCashierOrderController'))

        // Cashier Bank
        .state('CashierBank', defaultGridOpt('/cashier/bank'+datagridParam))
        .state('CashierBankCreate', defaultFormOpt('/cashier/bank/create'))
        .state('CashierBankModify', defaultFormOpt('/cashier/bank/modify/{id}'))

        // Cashier Edc
        .state('CashierEdc', defaultGridOpt('/cashier/edc'+datagridParam))
        .state('CashierEdcCreate', defaultFormOpt('/cashier/edc/create'))
        .state('CashierEdcModify', defaultFormOpt('/cashier/edc/modify/{id}'))

        // Cashier Surcharge
        .state('CashierSurcharge', defaultGridOpt('/cashier/surcharge'+datagridParam))
        .state('CashierSurchargeCreate', defaultFormOpt('/cashier/surcharge/create'))
        .state('CashierSurchargeModify', defaultFormOpt('/cashier/surcharge/modify/{id}'))

        // CashierSession
        .state('CashierSession', defaultGridOpt('/cashier/session'+datagridParam, 'template/datagrid-cashier-session', 'DatagridCashierSessionController'))
        .state('CashierSessionCreate', defaultFormOpt('/cashier/session/create'))
        .state('CashierSessionModify', defaultFormOpt('/cashier/session/modify/{id}'))

        // CashierShift
        .state('CashierShift', defaultGridOpt('/cashier/shift'+datagridParam))
        .state('CashierShiftModify', defaultFormOpt('/cashier/shift/modify/{id}'))

        // Cashier Target
        .state('CashierCashiertarget', defaultGridOpt('/cashier/cashier-target'+datagridParam, 'template/datagrid-cashier-target', 'DatagridCashierTargetController'))
        .state('CashierCashiertargetCreate', defaultFormOpt('/cashier/cashier-target/create', 'template/form-cashier-target', 'FormCashierTargetController'))
        .state('CashierCashiertargetModify', defaultFormOpt('/cashier/cashier-target/modify/{id}', 'template/form-cashier-target', 'FormCashierTargetController'))

        // SalesCustomer
        .state('SalesCustomer', defaultGridOpt('/sales/customer'+datagridParam))
        .state('SalesCustomerCreate', defaultFormOpt('/sales/customer/create', 'template/form-contact', 'FormContactController'))
        .state('SalesCustomerModify', defaultFormOpt('/sales/customer/modify/{id}', 'template/form-contact', 'FormContactController'))

        // Salesorder
        .state('SalesSalesorder', {
            url: "/sales/sales-order", // Data => General.php
            templateUrl: "template/datagrid-sales-order", // template
            controller: "DatagridSalesOrderController", // Nama Controller
            resolve: {
              deps: [
                "$ocLazyLoad",
                function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                    name: "DevplusApp",
                    insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    serie: true,
                    files: [
                      "plugins/bootstrap-daterangepicker/daterangepicker.min.css",
                      "plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css",
                      "plugins/moment.min.js",
                      "plugins/bootstrap-daterangepicker/daterangepicker.min.js",
                      "plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                      "js/controllers/DatagridSalesOrderController.js?v=11"
                    ]
                  });
                }
              ]
            }
          })
        .state('SalesSalesorderCreate', defaultFormOpt('/sales/sales-order/create', 'template/form-sales-order', 'FormSalesOrderController'))
        .state('SalesSalesorderModify', defaultFormOpt('/sales/sales-order/modify/{id}', 'template/form-sales-order', 'FormSalesOrderController'))

        // ReportStockdaily
        .state('ReportStockdaily', defaultGridOpt('/report/stock-daily'+datagridParam, 'template/datagrid-stock-daily', 'DatagridStockDailyController'))
        .state('ReportProductDaily',  defaultFormOpt('/report/productdaily', 'template/form-product-daily', 'FormProductDailyController'))
        .state('ReportProductHourly',  defaultFormOpt('/report/producthourly', 'template/form-product-hourly', 'FormProductHourlyController'))
        .state('ReportProductSummary',  defaultFormOpt('/report/productsummary', 'template/form-product-summary', 'FormProductSummaryController'))

        // AccPeriod
        .state('AccPeriod', defaultGridOpt('/acc/period'+datagridParam))
        .state('AccPeriodCreate', defaultFormOpt('/acc/period/create', 'template/form-period', 'FormPeriodController'))
        .state('AccPeriodModify', defaultFormOpt('/acc/period/modify/{id}', 'template/form-period', 'FormPeriodController'))

        //AccCoa
        .state('AccCoa', defaultGridOpt('/acc/coa'+datagridParam))
        .state('AccCoaCreate', defaultFormOpt('/acc/coa/create', 'template/form-chart-of-account', 'FormCoaController'))
        .state('AccCoaModify', defaultFormOpt('/acc/coa/modify/{id}', 'template/form-chart-of-account', 'FormCoaController'))

        //AccCoa
        .state('AccTrialbalance', defaultGridOpt('/acc/trialbalance'+datagridParam, 'template/datagrid-acc-trialbalance', 'DatagridTrialBalanceController'))

        //AccCustomReport
        .state('AccCustomreport', defaultFormOpt('/acc/customreport', 'template/form-report-customreport', 'FormReportCustomController'))
        .state('AccCustomreportDetail', {
            url: '/acc/customreport/detail', // Data => General.php
            templateUrl: 'template/datagrid-custom-report', // template
            controller: 'DatagridReportCustomController', // Nama Controller
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'DevplusApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            serie: true,
                            files: [
                              'js/controllers/DatagridReportCustomController.js?v=6',
                            ]
                        });
                }],
            }
        })

        //AccCustomReport
        .state('AccMgmreport', defaultFormOpt('/acc/mgmreport', 'template/form-report-customreport', 'FormReportCustomController'))
        .state('AccMgmreportDetail', {
            url: '/acc/mgmreport/detail', // Data => General.php
            templateUrl: 'template/datagrid-custom-report', // template
            controller: 'DatagridReportCustomController', // Nama Controller
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'DevplusApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            serie: true,
                            files: [
                              'js/controllers/DatagridReportCustomController.js?v=6',
                            ]
                        });
                }],
            }
        })

        //AccDailyReport
        .state('AccReportdaily', {
            url: '/acc/reportdaily', // Data => General.php
            templateUrl: 'template/datagrid-acc-dailyreport', // template
            controller: 'DatagridReportDailyController', // Nama Controller
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'DevplusApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            serie: true,
                            files: [
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.css',
                              'plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                              'plugins/moment.min.js',
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.js',
                              'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                              'js/controllers/DatagridReportDailyController.js?v=6',
                            ]
                        });
                }],
            }
        })
        //AccJournal
        .state('AccJournal', {
            url: '/acc/journal', // Data => General.php
            templateUrl: 'template/datagrid-journal', // template
            controller: 'DatagridJournalController', // Nama Controller
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'DevplusApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            serie: true,
                            files: [
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.css',
                              'plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                              'plugins/moment.min.js',
                              'plugins/bootstrap-daterangepicker/daterangepicker.min.js',
                              'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                              'js/controllers/DatagridJournalController.js?v=2',
                            ]
                        });
                }],
            }
        })
        .state('AccJournalCreate', defaultFormOpt('/acc/journal/create', 'template/form-journal', 'FormJournalController'))
        .state('AccJournalModify', defaultFormOpt('/acc/journal/modify/{id}', 'template/form-journal', 'FormJournalController'))

        //AccReport
        .state('AccReport', defaultGridOpt('/acc/report'+datagridParam))
        .state('AccReportCreate', defaultFormOpt('/acc/report/create', 'template/form-acc-report', 'FormAccReportController'))
        .state('AccReportModify', defaultFormOpt('/acc/report/modify/{id}', 'template/form-acc-report', 'FormAccReportController'))



}]);

/* Init global settings and run the app */
DevplusApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);
