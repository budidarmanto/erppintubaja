<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\SettingCompany;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementLog;
use App\Models\SettingWorkflow;
use Illuminate\Http\Request;
use Params;
use File;

class SalesOrder extends Controller {
    
    public function index(){
		// $user = $request->input('user');
		$input = Input::all();
        $query = InventoryStockMovement::select([
                'inventory_stock_movement.*',
                'crm_contact.name'
              ])
              ->leftjoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
			  ->where('inventory_stock_movement.created_by',$input['user'])
			  ->orWhere('inventory_stock_movement.updated_by',$input['user'])
              ->getQuery();
        $query = $query->get()->toArray();
		$dataResponse =[];
		foreach($query as $Valquery){
			$dd = $this->getStatusNext('SO', $Valquery->status);
			$next_data =[];
			foreach($dd as $val){
				$next_data [] = [
					'key' => $val . '-'. $this->getStatus($val),
					'label' => $this->getStatus($val) ,
				];
			} 
			$dataResponse []= [
				'id' => $Valquery->id,
				'code' => $Valquery->code,
				'company' => $Valquery->company,
				'contact' => $Valquery->contact,
				'pos' => $Valquery->pos,
				'description' => $Valquery->description,
				'source' => $Valquery->source,
				'destination' => $Valquery->destination,
				'company_destination' => $Valquery->company_destination,
				'expected_date' => $Valquery->expected_date,
				'reference' => $Valquery->reference,
				'type' => $Valquery->type,
				'status' => $this->getStatus($Valquery->status),
				'currency' => $Valquery->currency,
				'currency_rate' => $Valquery->currency_rate,
				'printed' => $Valquery->printed,
				'category' => $Valquery->category,
				'date' => $Valquery->date,
				'total_discount' => $Valquery->total_discount,
				'total_price' => $Valquery->total_price,
				'total_tax' => $Valquery->total_tax,
				'total' => $Valquery->total,
				'cashier_transaction_code' => $Valquery->cashier_transaction_code,
				'cashier_session' => $Valquery->cashier_session,
				'cashier_reference' => $Valquery->cashier_reference,
				'cashier_reference_no' => $Valquery->cashier_reference_no,
				'cashier_payment_type' => $Valquery->cashier_payment_type,
				'cashier_bank' => $Valquery->cashier_bank,
				'cashier_edc' => $Valquery->cashier_edc,
				'cashier_card_number' => $Valquery->cashier_card_number,
				'cashier_surcharge' => $Valquery->cashier_surcharge,
				'cashier_customer_email' => $Valquery->cashier_customer_email,
				'created_by' => $Valquery->created_by,
				'updated_by' => $Valquery->updated_by,
				'created_at' => $Valquery->created_at,
				'updated_at' => $Valquery->updated_at,
				'shipment_address' => $Valquery->shipment_address,
				'name' => $Valquery->name,
				'next_tahapan' => $next_data,//$this->getStatusNext('SO',$Valquery->status)
			];
		}
		$data['data'] = $dataResponse;
		
		
		//dd($next_data);
        return response()->json($data);
    }
    
    public function detail($id = null) {
        if($id === null || empty($id)) {
            return response()->json(['data' => []]);
        }
        $data = InventoryStockMovement::select([
                    'inventory_stock_movement.*',
                    'crm_contact.name'
                ])
                ->leftjoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
                ->where('inventory_stock_movement.id', $id)->first();
        $SalesDetail = InventoryStockMovementProduct::select(DB::raw(''
                            .'inventory_stock_movement_product.id AS id_detail,'
                            .'product_master.name AS name,'
                            .'product_master.code AS code,'
                            .'product_product.id,'
                            .'product_master.id AS product_id,'
                            .'product_product.attribute,'
                            .'inventory_stock_movement_product.qty,'
                            .'inventory_stock_movement.shipment_address,'
                            .'inventory_stock_movement.status,'
                            .'inventory_stock_movement.total'
                        ))
                        ->leftjoin('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
                        ->leftjoin('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
                        ->leftjoin('product_master','product_master.id', '=', 'product_product.product')
                        ->leftjoin('product_uom','product_uom.id', '=', 'inventory_stock_movement_product.uom')
                        ->where('inventory_stock_movement.id',$id)
                        ->get();
        $dataDetail = [];
        if(count($SalesDetail) > 0){
            $attributeValue = \App\Models\ProductAttributeValue::all()->toArray();
            $attributeValue = collect($attributeValue)->pluck('name', 'id')->toArray();

            foreach($SalesDetail as $valSalesDetail){
                $attrJson = json_decode($valSalesDetail['attribute'], true);
                $attr = collect($attrJson)->pluck('attribute_value')->map(function($value, $key) use ($attributeValue){
                  return (isset($attributeValue[$value])) ? $attributeValue[$value] : '';
                })->implode(', ');
                $attr = (!empty($attr)) ? ' ('.$attr.')' : '';
                $productName = $valSalesDetail['name'].$attr;
                
                $dataDetail[] = [
                    'id_detail' => $valSalesDetail->id_detail,
                    'namaproduct' => $productName,
                    'total' => $valSalesDetail->total,
                    'kuantiti' => $valSalesDetail->qty,
                    'alamatpengiriman' => $valSalesDetail->shipment_address,
                    'status' => $this->getStatus($valSalesDetail->status)
                ];
            }
        }		  
					  
					  
        $dataResponse = [
            'id_header' => $data->id,
            'code' => $data->code,
            'tanggal' => $data->date,
            'idpelanggan' => $data->contact,
            'pelanggan' => $data->name,
            'detail' => $dataDetail,
        ];
        return response()->json($dataResponse);
    }
    
    public function history($customer = null){
        $query = InventoryStockMovement::select([
                        'inventory_stock_movement.*',
                        'crm_contact.name',
                        'crm_contact.picture',
                        'crm_contact.address',
                        'crm_contact.city',
                        'crm_contact.postal_code',
                        'crm_contact.state',
                        'crm_contact.country',
                        'crm_contact.website',
                        'crm_contact.phone',
                        'crm_contact.mobile',
                        'crm_contact.fax',
                        'crm_contact.email',
                        'crm_contact.tax_no',
                        'crm_contact.type',
                        'crm_contact.pkp',
                        'crm_contact.term_of_payment',
                        'crm_contact.revision',
                        'setting_workflow.workflow',
                    ])
                    ->leftjoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
                    ->leftjoin('setting_workflow', 'setting_workflow.code', '=', 'inventory_stock_movement.type')
                    ->where('inventory_stock_movement.contact', $customer);
        $query = $query->get()->toArray();
        
        for($i = 0; $i < count($query); $i++) {
            $workflow = json_decode($query[$i]['workflow'], true);
            foreach($workflow as $rowWf) {
                if($rowWf['code'] === $query[$i]['status']) {
                    $query[$i]['status_description'] = $rowWf['description'];
                }
            }
            
        }
        $data['data'] = $query; 
        return response()->json($data);
    }
	
    public function create() {
        $data = Input::get();
        $id = uniqid().substr(md5(mt_rand()), 0, 5);
		
        $addsales = new InventoryStockMovement;
        $addsales->id = $id;
        $addsales->code = InventoryStockMovement::generateId('SO', $data['company']);
        $addsales->company = $data['company'];
        $addsales->contact = $data['contact'];
        $addsales->expected_date= $data['expected_date'];
        $addsales->type = 'SO';
        $addsales->status = $this->getStatusDefaul('SO');
        $addsales->date = date('Y-m-d');
        $addsales->shipment_address= $data['shipment_address'];
        $addsales->description	= $data['description'];
        $addsales->created_by = $data['salesman'];
        try {
            $success = $addsales->save();	
            return response()->json([
                'status'   => $success,
                'id_order' => $id
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => false,
            ]);
        }
    }
	
    public function updateSales($id = null) {
        $data = Input::get();
        $updateDetail = InventoryStockMovement::where('id', $id)
            ->update([
                'contact' => $data['contact'], 
                'expected_date' => $data['expected_date'],
                'shipment_address' => $data['shipment_address'],
                'description' => $data['description']
            ]);

        if($updateDetail) {
            return response()->json([
                'status' => true,
            ]);
        }
        else {
            return response()->json([
                'status' => false,
            ]);
        }
    }
    
    public function createdetail(Request $request) {		
        $id = $request->input('id');
        $product = $request->input('product');
        $uom = \Devplus\Params\Params::get('UOM_PIECES');//$request->input('uom');
        $qty = $request->input('qty');		
        $door_height = $request->input('door_height');
        $door_width = $request->input('door_width');
        $frame_height = $request->input('frame_height');
        $frame_width = $request->input('frame_width');
        $door_direction = $request->input('door_direction');
        $door_installation = $request->input('door_installation');

        if(empty($id) || empty($product) || empty($uom)) {
            return response()->json([
                'status' => false,
                'message' => 'Parameter \'id\', \'product\', dan \'uom\' tidak boleh kosong.',
            ]);
        }
        
        $addsalesDetail = new InventoryStockMovementProduct;
        $addsalesDetail->stock_movement	= $id;
        $addsalesDetail->product = $product;
        $addsalesDetail->uom = $uom;
        $addsalesDetail->qty = $qty;
        $addsalesDetail->door_height = $door_height;
        $addsalesDetail->door_width = $door_width;
        $addsalesDetail->frame_height = $frame_height;
        $addsalesDetail->frame_width = $frame_width;
        $addsalesDetail->door_direction = $door_direction;
        $addsalesDetail->door_installation = $door_installation;
		
        try {
            $success = $addsalesDetail->save();	
            return response()->json([
                'status' => $success,
                'id_detail' => $addsalesDetail->id,
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                    'status' => false,
            ]);
        }
    }
	
    public function updatedetail($id=null, $product=null) {
        $data = Input::get();
        $product = $data['product'];
        $uom = '5afc19ae1264aa6a65';//$data['uom'];
        $qty  = $data['qty'];		
        $door_height = $data['door_height'];
        $door_width = $data['door_width'];
        $frame_height = $data['frame_height'];
        $frame_width = $data['frame_width'];
        $door_direction = $data['door_direction'];
        $door_installation = $data['door_installation'];

        $updateDetail = InventoryStockMovementProduct::where('id', $id)
            ->update([
                'product'=>$product,
                'uom' => $uom,
                'qty'=> $qty,
                'door_height'=> $door_height,
                'door_width' => $door_width,
                'frame_height' => $frame_height,
                'frame_width' => $frame_width,
                'door_direction' => $door_direction,
                'door_installation' => $door_installation
            ]);

        if($updateDetail) {
            return response()->json([
                'status' => true,
            ]);
        }
        else {
            return response()->json([
                'status' => false,
                //'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }
    }
	
    public function createdetailDoorHeightPic(Request $request) {		
        $id 			 = $request->input('id');
        $product 		 = $request->input('product');
        $door_height_pic = $request->file('door_height_pic');
        $door_height_pic_img = getimagesize($door_height_pic);
        if(floatval($door_height_pic->getClientSize()) > 2 * 1024 * 1024) {
            return response()->json([
                'status' => false,
                'message' => 'Ukuran maksimal gambar adalah 2MB.',
            ]);
        }
        if($door_height_pic_img[0] > 800 || $door_height_pic_img[1] > 800) {
            return response()->json([
                'status' => false,
                'message' => 'Panjang dan lebar maksimal gambar 800px.',
            ]);
        }
        
        $updateDetail = InventoryStockMovementProduct::where('id', $id)
            ->update([
                'door_height_pic' => $door_height_pic->getClientOriginalName()
            ]);

        $filename1 = $door_height_pic->getClientOriginalName();
        $uploaded1 = false;
        if($updateDetail) {
            $uploaded1 = $door_height_pic->storeAs('img/product-order', $filename1, 'public_uploads');
            if(!$uploaded1) {
                return response()->json([
                    'status' => false,
                    'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                ]);
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);

    }
	
    public function createdetailDoorWidthPic(Request $request) {
        $id = $request->input('id');
        $product = $request->input('product');
        $door_width_pic = $request->file('door_width_pic');
        $door_width_pic_img = getimagesize($door_width_pic);
        if(floatval($door_width_pic->getClientSize()) > 2 * 1024 * 1024) {
            return response()->json([
                'status' => false,
                'message' => 'Ukuran maksimal gambar adalah 2MB.',
            ]);
        }
        if($door_width_pic_img[0] > 800 || $door_width_pic_img[1] > 800) {
            return response()->json([
                'status' => false,
                'message' => 'Panjang dan lebar maksimal gambar 800px.',
            ]);
        }
        $filename1 = $door_width_pic->getClientOriginalName();
	    $updateDetail = InventoryStockMovementProduct::where('id', $id)
            ->update([
                'door_width_pic' => $filename1
            ]);
        
        $uploaded1 = false;
        if($updateDetail) {
            $uploaded1 = $door_width_pic->storeAs('img/product-order', $filename1, 'public_uploads');
                if(!$uploaded1) {
                return response()->json([
                    'status' => false,
                    'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                ]);
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);

    }
	
    public function createdetailFrameHeightPic(Request $request) {
        $id = $request->input('id');
        $product = $request->input('product');
        $frame_height_pic = $request->file('frame_height_pic');
        $frame_height_pic_img = getimagesize($frame_height_pic);
        if(floatval($frame_height_pic->getClientSize()) > 2 * 1024 * 1024) {
            return response()->json([
                'status' => false,
                'message' => 'Ukuran maksimal gambar adalah 2MB.',
            ]);
        }
        if($frame_height_pic_img[0] > 800 || $frame_height_pic_img[1] > 800) {
            return response()->json([
                'status' => false,
                'message' => 'Panjang dan lebar maksimal gambar 800px.',
            ]);
        }
        $filename1 = $frame_height_pic->getClientOriginalName();
        $updateDetail = InventoryStockMovementProduct::where('id', $id)
            ->update([
                'frame_height_pic' => $filename1
            ]);

        $filename1 = $frame_height_pic->getClientOriginalName();
        $uploaded1 = false;
        if($updateDetail) {
            $uploaded1 = $frame_height_pic->storeAs('img/product-order', $filename1, 'public_uploads');
            if(!$uploaded1) {
                return response()->json([
                    'status' => false,
                    'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                ]);
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);

    }
	
    public function createdetailFrameWidthPic(Request $request) {		
        $id = $request->input('id');
        $product = $request->input('product');
        $frame_width_pic = $request->file('frame_width_pic');
        $frame_width_pic_img = getimagesize($frame_width_pic);
        if(floatval($frame_width_pic->getClientSize()) > 2 * 1024 * 1024) {
            return response()->json([
                'status' => false,
                'message' => 'Ukuran maksimal gambar adalah 2MB.',
            ]);
        }
        if($frame_width_pic_img[0] > 800 || $frame_width_pic_img[1] > 800) {
            return response()->json([
                'status' => false,
                'message' => 'Panjang dan lebar maksimal gambar 800px.',
            ]);
        }
        $filename1 = $frame_width_pic->getClientOriginalName();
        $updateDetail = InventoryStockMovementProduct::where('id', $id)
            ->update([
                'frame_width_pic' => $filename1
            ]);

        $filename1 = $frame_width_pic->getClientOriginalName();
        $uploaded1 = false;
        if($updateDetail) {
            $uploaded1 = $frame_width_pic->storeAs('img/product-order', $filename1, 'public_uploads');
            if(!$uploaded1) {
                return response()->json([
                    'status' => false,
                    'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                ]);
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);

    }
	
	
	/* Delete */
	
	public function deleteDoorHeightPic(Request $request) {		
        $id				= $request->input('id');
		$filename 		= InventoryStockMovementProduct::where('id', $id)->first();
		$delete			= File::Delete('img/product-order/'.$filename->door_height_pic);
		if($delete){
			$updateDetail	= InventoryStockMovementProduct::where('id', $id)
							->update([
								'door_height_pic' => null
							]);
		}else{
			return response()->json([
                    'status'  => false,
                    'message' => 'Terjadi kesalahan pada server saat menghapus gambar.',
                ]);
		}
        return response()->json([
            'status' => true,
        ]);
    }
	
	public function deleteDoorWidthPic(Request $request) {		
        $id				= $request->input('id');
		$filename 		= InventoryStockMovementProduct::where('id', $id)->first();
		$delete			= File::Delete('img/product-order/'.$filename->door_width_pic);
		if($delete){
			$updateDetail	= InventoryStockMovementProduct::where('id', $id)
							->update([
								'door_width_pic' => null
							]);
		}else{
			return response()->json([
                    'status'  => false,
                    'message' => 'Terjadi kesalahan pada server saat menghapus gambar.',
                ]);
		}
        return response()->json([
            'status' => true,
        ]);
    }
	
	public function deleteFrameHeightPic(Request $request) {		
        $id				= $request->input('id');
		$filename 		= InventoryStockMovementProduct::where('id', $id)->first();
		$delete			= File::Delete('img/product-order/'.$filename->frame_height_pic);
		if($delete){
			$updateDetail	= InventoryStockMovementProduct::where('id', $id)
							->update([
								'frame_height_pic' => null
							]);
		}else{
			return response()->json([
                    'status'  => false,
                    'message' => 'Terjadi kesalahan pada server saat menghapus gambar.',
                ]);
		}
        return response()->json([
            'status' => true,
        ]);
    }
	
	public function deleteFrameWidthPic(Request $request) {		
        $id				= $request->input('id');
		$filename 		= InventoryStockMovementProduct::where('id', $id)->first();
		$delete			= File::Delete('img/product-order/'.$filename->frame_width_pic);
		if($delete){
			$updateDetail	= InventoryStockMovementProduct::where('id', $id)
							->update([
								'frame_width_pic' => null
							]);
		}else{
			return response()->json([
                    'status'  => false,
                    'message' => 'Terjadi kesalahan pada server saat menghapus gambar.',
                ]);
		}
        return response()->json([
            'status' => true,
        ]);
    }
	/* end delete */
	
    public function getStatus($status){
        $workflow = SettingWorkflow::where('code', 'SO')->first();
        $data = [];
        if(!empty($workflow)){
            $workflow = json_decode($workflow->workflow, 1);
            foreach($workflow as $val){
                if($val['code'] == $status){
                    $data = $val['description'];
                }
            }
        }
        return $data;
    }
	
	public function getStatusDefaul($type){
        $workflow = SettingWorkflow::where('code', $type)->first();
        $data = [];
        if(!empty($workflow)){
            $workflow = json_decode($workflow->workflow, 1);
            foreach($workflow as $val){
                if($val['default'] == true){
                    $data = $val['code'];
                }
            }
        }
        return $data;
    }
	
	public function getStatusNext($type, $status){
        $workflow 	= SettingWorkflow::where('code', $type)->first();
        $next_data 	= [];
        if(!empty($workflow)){
            $workflow = json_decode($workflow->workflow, 1);
            foreach($workflow as $val){
                if($val['code'] == $status){
                    $next_data = $val['next'];
                }
            }
        }
        return $next_data;
    }
	
    public function getorder($id = null) {
        if($id === null || empty($id)) {
            return response()->json(['data' => []]);
        }
	
        $data = InventoryStockMovement::select([
                'inventory_stock_movement.id',
                'inventory_stock_movement.contact',
                'inventory_stock_movement.description',
                'inventory_stock_movement.expected_date',
                'inventory_stock_movement.shipment_address',
				'inventory_stock_movement.status',
                'crm_contact.name'
            ])
            ->join('crm_contact','crm_contact.id','=', 'inventory_stock_movement.contact')
            ->where('inventory_stock_movement.id', $id)->first();
		
		$dd = $this->getStatusNext('SO', $data->status);
		$next_data =[];
		foreach($dd as $val){
			$next_data [] = [
				'key' => $val . '-'. $this->getStatus($val),
				'label' => $this->getStatus($val) ,
			];
		} 
		$dataResponse =[];
		$dataResponse = [
            'id' => $data->id,
            'contact' => $data->contact,
            'description' => $data->description,
            'expected_date' => $data->expected_date,
            'shipment_address' => $data->shipment_address,
            'status' => $this->getStatus($data->status),
			'name' => $data->name,
			'next_tahapan' => $next_data,
        ];
		
        return response()->json($dataResponse);
    }
	
    public function getdetail($id) {
        if($id === null || empty($id)) {
            return response()->json(['data' => []]);
        }
		
        $data = InventoryStockMovementProduct::select([
                'inventory_stock_movement_product.id as id_detail',
                'inventory_stock_movement_product.stock_movement as id_header',
                'inventory_stock_movement_product.uom',
                'inventory_stock_movement_product.qty',
                'inventory_stock_movement_product.door_height',
                'inventory_stock_movement_product.door_width',
                'inventory_stock_movement_product.frame_height',
                'inventory_stock_movement_product.frame_width',
                'inventory_stock_movement_product.door_direction',
                'inventory_stock_movement_product.door_installation',
                'product_product.id as id_product',
                'product_product.attribute as attribute',
                'product_master.name',
                'product_uom.name as uom_name',
                DB::raw('CONCAT(\'' . url('/') . '/img/product-order/' . '\', inventory_stock_movement_product.door_height_pic) as door_height_pic_uri'),
                DB::raw('CONCAT(\'' . url('/') . '/img/product-order/' . '\', inventory_stock_movement_product.door_width_pic) as door_width_pic_uri'),
                DB::raw('CONCAT(\'' . url('/') . '/img/product-order/' . '\', inventory_stock_movement_product.frame_height_pic) as frame_height_pic_uri'),
                DB::raw('CONCAT(\'' . url('/') . '/img/product-order/' . '\', inventory_stock_movement_product.frame_width_pic) as frame_width_pic_uri'),
            ])
            ->join('product_product','product_product.id','=', 'inventory_stock_movement_product.product')
            ->join('product_master','product_master.id','=', 'product_product.product')
            ->join('product_uom','product_uom.id','=', 'inventory_stock_movement_product.uom')
            ->where('inventory_stock_movement_product.id', $id)->first();
	if(empty($data)) {
            return response()->json(['data' => []]);
        }
        $PR = Params::getOption('OPTION_PRODUCT_DIRECTION'); 
        $direction ='';
        $dr = $data->door_direction;
        foreach($PR as $key => $val){
            if(!empty($dr)){
                if($val['id'] ===  $data->door_direction) {
                    $direction = $val['text'];
                }
            }
        }
        $PI = Params::getOption('OPTION_PRODUCT_INSTALLATION'); 
        $installation ='';
        $ints = $data->door_installation;
        foreach($PI as $key => $val){
            if(!empty($ints)){
                if($val['id'] ===  $data->door_installation) {
                    $installation = $val['text'];
                }
            }
        }
        
        $attributeValue = \App\Models\ProductAttributeValue::all()->toArray();
        $attributeValue = collect($attributeValue)->pluck('name', 'id')->toArray();
        $attrJson = json_decode($data->attribute, true);
        $attr = collect($attrJson)->pluck('attribute_value')->map(function($value, $key) use ($attributeValue){
          return (isset($attributeValue[$value])) ? $attributeValue[$value] : '';
        })->implode(', ');
        $attr = (!empty($attr)) ? ' ('.$attr.')' : '';
        $productName = $data->name.$attr;
                
        
        $dataResponse = [
            'id_detail' => $data->id_detail,
            'id_header' => $data->id_header,
            'uom' => $data->uom,
            'qty' => $data->qty,
            'door_height' => $data->door_height,
            'door_width' => $data->door_width,
            'frame_height' => $data->frame_height,
            'frame_width' => $data->frame_width,
            'id_door_direction' => $data->door_direction,
            'door_direction' => $direction,
            'id_door_installation' => $data->door_installation,
            'door_installation' => $installation,
            'id_product' => $data->id_product,
            'name' => $productName,
            'uom_name' => $data->uom_name,
            'door_height_pic' => $data->door_height_pic_uri,
            'door_width_pic' => $data->door_width_pic_uri,
            'frame_width_pic' => $data->frame_width_pic_uri,
            'frame_height_pic' => $data->frame_height_pic_uri,
        ];
        return response()->json($dataResponse);
    }
	
	public function changeStatus(){
		$data 	= Input::all();
        $id 	= $data['id'];
        $status = $data['status'];

		
		$stockMovement = InventoryStockMovement::find($id);
		if(!empty($stockMovement)){
			if($stockMovement->status == $status){
				return false;
			}
			$stockMovement->status = $status;
			$saved = $stockMovement->save();
			

			if($saved) {
				InventoryStockMovementLog::addLog($id, $status);
			}
			else {
				return response()->json([
					'status' => false,
					'message' => 'Terjadi kesalahan pada server',
				]);
			}
			
		}
        return response()->json([
            'status' => true,
        ]);
	}
  
}
