<div class="modal-header">
    <button type="button" class="close" aria-hidden="true" ng-click="cancel()"></button>
    <h3 class="modal-title" id="modal-title">Employee</h3>
</div>
<div class="modal-body" id="modal-body">
  <div class="portlet">
      <div class="portlet-body">
        <div class="row margin-bottom-10">
          <div class="col-md-6">
            <div class="table-filter">
                <div class="input-icon input-icon-sm right">
                  <i class="fa fa-search"></i>
                  <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                </div>
            </div>
          </div>
          <div class="col-md-6 text-right">
            @include('pages.partials.datagrid.datagrid-status')
          </div>
        </div>

<!-- Begin Table -->
<div class="table-responsive">
  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th ng-repeat="column in response.columns">
          <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
          @{{ column.label }}
          </div>
        </th>
        <th style="width: 120px;" class="text-center">@lang('system.datagrid.table.action')</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="dt in response.data">
        <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td>
        <td class="text-center">
          <button class="btn btn-xs btn-success tooltips" ng-click="addEmployee(dt)" data-original-title="Tambah" data-placement="left"><i class="fa fa-plus"></i></button>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>

<!-- End Table -->
        <div class="row margin-top-10">
          <div class="col-md-6">
            <div>

              <!-- @include('pages.partials.datagrid.datagrid-mass-button') -->
            </div>
          </div>
          <div class="col-md-6">
            <div ng-hide="selectionOnly == true">
              @include('pages.partials.datagrid.datagrid-pagination')
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
<!-- <div class="modal-footer">
    <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
    <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
</div> -->
