/* Setup blank page controller */
angular.module('DevplusApp').controller('FormContactController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', function($rootScope, $scope, settings, response, $http, $location) {

    $scope.contactFields = [
      {name: 'type', label: 'Tipe', type: 'select', attributes: {maxlength: 100, 'ng-options': 'item.id as item.text for item in optionContactType'}},
      {name: 'name', label: 'Nama Kontak', type: 'text', attributes: {required: '', maxlength: 100}},
      {name: 'position', label: 'Jabatan', type: 'text', attributes: {maxlength: 100}},
      {name: 'address', label: 'Alamat', type: 'textarea', attributes: {maxlength: 100}},
      {name: 'city', label: 'Kota', type: 'text', attributes: {maxlength: 100}},
      {name: 'postal_code', label: 'Kode Pos', type: 'text', attributes: {maxlength: 100}},
      {name: 'state', label: 'Provinsi', type: 'text', attributes: {maxlength: 100}},
      {name: 'country', label: 'Negara', type: 'select', attributes: {maxlength: 100, 'ng-options': 'item.id as item.text for item in optionCountry', 'data-live-search' : true}},
      {name: 'website', label: 'Website', type: 'text', attributes: {maxlength: 100}},
      {name: 'fax', label: 'Fax', type: 'text', attributes: {maxlength: 100}},
      {name: 'mobile', label: 'No. HP', type: 'text', attributes: {maxlength: 100}},
      {name: 'phone', label: 'Telepon', type: 'text', attributes: {maxlength: 100}},
      {name: 'email', label: 'Email', type: 'text', attributes: {maxlength: 100}},
    ];

    $scope.response = response;
    $scope.dataContact = response.dataContact;
    $scope.optionCountry = response.optionCountry;
    $scope.optionContactType = response.optionContactType;

    $scope.addContact = function(){
      $scope.dataContact.push({
        id: '',
        name: '#Belum ada Nama',
        type: 'CT',
        position: '',
        address: '',
        city: '',
        postal_code: '',
        state: '',
        country: '',
        website: '',
        phone: '',
        mobile: '',
        fax: '',
        email: ''
      });
      setTimeout(function(){
        App.initAjax();
      }, 1);
    }

    $scope.deleteContact = function(contact){
      $scope.dataContact.splice($scope.dataContact.indexOf(contact), 1);
    }

    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.dataContact = $scope.dataContact;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataContact = resp.data.dataContact;
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);
