<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAlternativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_alternative', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('product_master', 25);
          $table->string('product', 25);

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('product_master')
                ->references('id')->on('product_master')
                ->onDelete('cascade')
                ->onUpdate('cascade');

          $table->foreign('product')
                ->references('id')->on('product_product')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_alternative');
    }
}
