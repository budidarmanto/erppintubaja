<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductAttributeValue extends DevplusModel
{
    protected $table = 'product_attribute_value';
    protected $fillable = ['id', 'attribute', 'name'];

    public static function getOption($model = null){
      if($model == null){
          $data = ProductAttributeValue::orderBy('name')->get();
      }else{
        $data = $model->orderBy('name')->get();
      }

      $opt = [];
      if(count($data) > 0){
        foreach($data as $val){
          $opt[] = [
            'id' => $val['id'],
            'text' => $val['name'],
            'attribute' => $val['attribute']
          ];
        }
      }

      return $opt;
    }
}
