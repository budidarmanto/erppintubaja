<?php

namespace Devplus\Model;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Baum;

abstract class DevplusModelTree extends Baum\Node {
  public $incrementing = false;

  // Order Column
  protected $orderColumn = 'left';

  // 'parent_id' column name
  protected $parentColumn = 'parent';

  // 'lft' column name
  protected $leftColumn = 'left';

  // 'rgt' column name
  protected $rightColumn = 'right';

  // 'depth' column name
  protected $depthColumn = 'depth';

  protected static function boot() {
    parent::boot();

    static::creating(function($table) {
        $table->id = uniqid().substr(md5(mt_rand()), 0, 5);
        $table->created_by = 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
    });

    static::updating(function($table) {
        $table->updated_by = 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
    });
  }
}
