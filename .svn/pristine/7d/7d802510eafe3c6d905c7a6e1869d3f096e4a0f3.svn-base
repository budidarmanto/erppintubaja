<?php

namespace App\Http\Controllers\App;

use App\Models\AppApplication;
use App\Models\AppModule;
use App\Models\AppRole;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Helper;

class Application extends Controller
{

  public function index() {
    $dg = Datagrid::source(AppApplication::query());
    $dg->title(__('app.app.application.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', __('app.app.application.code'));
    $dg->add('name', __('app.app.application.name'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new AppApplication());
    $form->title(__('app.app.application.title'));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save Module
        if(!empty($form->dataPost['dataModule'])){
          foreach($form->dataPost['dataModule'] as $val){
            $module = new AppModule();
            $module->fill([
              'code' => $val['code'],
              'name' => $val['name'],
              'role' => json_encode($val['role']),
              'application' => $form->model->id
            ]);
            $module->save();
          }
        }

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['optionRole'] = AppRole::getOption();
    $dataForm['dataModule'] = [];
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(AppApplication::find($id));
    $form->title(__('app.app.application.title'));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        AppModule::where('application', $id)->delete();
        //save Module
        if(!empty($form->dataPost['dataModule'])){
          foreach($form->dataPost['dataModule'] as $val){
            $module = new AppModule();
            $module->fill([
              'code' => $val['code'],
              'name' => $val['name'],
              'role' => json_encode($val['role']),
              'application' => $form->model->id
            ]);
            $module->save();
          }
        }
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['optionRole'] = AppRole::getOption();
    // get data module
    $dataModule = AppModule::select(['id', 'code', 'name', 'role', 'application'])->where('application', $id)->get()->toArray();
    $dataModule = collect($dataModule)->map(function($value){
        $value['role'] = json_decode($value['role']);
        return $value;
    });
    $dataForm['dataModule'] = $dataModule;

    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    AppApplication::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('app.app.application.title');
    $form->add('code', __('app.app.application.code'), 'text')->rule('required|max:100');
    $form->add('name', __('app.app.application.name'), 'text')->rule('required|max:100');
    return $form;
  }

}
