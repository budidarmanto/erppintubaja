<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stock', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('product', 25);
            $table->string('uom', 25);
            $table->string('warehouse_location', 25);
            $table->timestamp('date');
            $table->decimal('begin_qty', 18, 2)->default(0);
            $table->decimal('plus_qty', 18, 2)->default(0);
            $table->decimal('min_qty', 18, 2)->default(0);
            $table->decimal('end_qty', 18, 2)->default(0);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('product')
                  ->references('id')->on('product_product')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('uom')
                  ->references('id')->on('product_uom')
                  ->onDelete('restrict')
                  ->onUpdate('cascade');

            $table->foreign('warehouse_location')
                  ->references('id')->on('inventory_warehouse_location')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stock');
    }
}
