<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Carbon\Carbon;
use App\Models\SettingWorkflow;

class InventoryStockMovementLog extends DevplusModel
{
    protected $table = 'inventory_stock_movement_log';
    protected $fillable = ['id', 'stock_movement', 'status', 'created_at', 'updated_at'];

    public static function addLog($stockMovement = null, $status = null){
      if(empty($stockMovement || empty($status))){
        return false;
      }

      InventoryStockMovementLog::create([
        'stock_movement' => $stockMovement,
        'status' => $status
      ]);
    }

    public static function getData($id, $type, $label){

      $stockMovementLog = InventoryStockMovementLog::select([
        'inventory_stock_movement_log.id AS id',
        'inventory_stock_movement.id AS stock_movement',
        'inventory_stock_movement.code AS code',
        'inventory_stock_movement_log.status AS status',
        'inventory_stock_movement_log.created_by AS created_by',
        'inventory_stock_movement_log.created_at AS created_at',
        'setting_user.fullname AS fullname',
        'setting_user.nickname AS nickname',
        'setting_user.picture AS picture',
      ])
      ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_log.stock_movement')
      ->join('setting_user', 'setting_user.username', '=', 'inventory_stock_movement_log.created_by')
      ->where('stock_movement', $id)->orderBy('inventory_stock_movement_log.created_at', 'ASC')->get()->toArray();

      $dataWorkflow = SettingWorkflow::getOption($type);

      $dataLog = [];

      if(!empty($stockMovementLog)){
        foreach($stockMovementLog as $val){
          $data = [
            'id' => $val['id'],
            'status' => $val['status'],
            'picture' => $val['picture'],
            'name' => ( (empty($val['nickname'])) ? $val['fullname'] : $val['nickname'] ),
            'date' => strtotime($val['created_at']),
            'created_at' => Carbon::parse($val['created_at'])->format('d/m/Y H:i'),
          ];

          $workflow = collect($dataWorkflow)->where('id', $val['status'])->first();
          if(!empty($workflow)){
            $data['status_label'] = $workflow['label'];
            $data['color'] = $workflow['color'];
            $data['comment'] = $label.' '.$data['status_label'];
          }

          $dataLog[] = $data;
        }
      }


      return $dataLog;
    }
}
