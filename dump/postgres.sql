--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: app_application; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY app_application (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44b033808c34e2	app	System Configuration	SYSTEM	\N	2017-03-30 17:47:28	2017-03-30 17:47:28
58ddfe5a16f71146e4	setting	Settings	SYSTEM	\N	2017-03-31 06:59:38	2017-03-31 06:59:38
\.


--
-- Data for Name: app_config; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY app_config (id, code, value, description, created_by, updated_by, created_at, updated_at) FROM stdin;
59269554ea8fd90f80	OPTION_COUNTRY	{"BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria", "BA": "Bosnia and Herzegovina", "BB": "Barbados", "WF": "Wallis and Futuna", "BL": "Saint Barthelemy", "BM": "Bermuda", "BN": "Brunei", "BO": "Bolivia", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin", "BT": "Bhutan", "JM": "Jamaica", "BV": "Bouvet Island", "BW": "Botswana", "WS": "Samoa", "BQ": "Bonaire, Saint Eustatius and Saba ", "BR": "Brazil", "BS": "Bahamas", "JE": "Jersey", "BY": "Belarus", "BZ": "Belize", "RU": "Russia", "RW": "Rwanda", "RS": "Serbia", "TL": "East Timor", "RE": "Reunion", "TM": "Turkmenistan", "TJ": "Tajikistan", "RO": "Romania", "TK": "Tokelau", "GW": "Guinea-Bissau", "GU": "Guam", "GT": "Guatemala", "GS": "South Georgia and the South Sandwich Islands", "GR": "Greece", "GQ": "Equatorial Guinea", "GP": "Guadeloupe", "JP": "Japan", "GY": "Guyana", "GG": "Guernsey", "GF": "French Guiana", "GE": "Georgia", "GD": "Grenada", "GB": "United Kingdom", "GA": "Gabon", "SV": "El Salvador", "GN": "Guinea", "GM": "Gambia", "GL": "Greenland", "GI": "Gibraltar", "GH": "Ghana", "OM": "Oman", "TN": "Tunisia", "JO": "Jordan", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "HK": "Hong Kong", "HN": "Honduras", "HM": "Heard Island and McDonald Islands", "VE": "Venezuela", "PR": "Puerto Rico", "PS": "Palestinian Territory", "PW": "Palau", "PT": "Portugal", "SJ": "Svalbard and Jan Mayen", "PY": "Paraguay", "IQ": "Iraq", "PA": "Panama", "PF": "French Polynesia", "PG": "Papua New Guinea", "PE": "Peru", "PK": "Pakistan", "PH": "Philippines", "PN": "Pitcairn", "PL": "Poland", "PM": "Saint Pierre and Miquelon", "ZM": "Zambia", "EH": "Western Sahara", "EE": "Estonia", "EG": "Egypt", "ZA": "South Africa", "EC": "Ecuador", "IT": "Italy", "VN": "Vietnam", "SB": "Solomon Islands", "ET": "Ethiopia", "SO": "Somalia", "ZW": "Zimbabwe", "SA": "Saudi Arabia", "ES": "Spain", "ER": "Eritrea", "ME": "Montenegro", "MD": "Moldova", "MG": "Madagascar", "MF": "Saint Martin", "MA": "Morocco", "MC": "Monaco", "UZ": "Uzbekistan", "MM": "Myanmar", "ML": "Mali", "MO": "Macao", "MN": "Mongolia", "MH": "Marshall Islands", "MK": "Macedonia", "MU": "Mauritius", "MT": "Malta", "MW": "Malawi", "MV": "Maldives", "MQ": "Martinique", "MP": "Northern Mariana Islands", "MS": "Montserrat", "MR": "Mauritania", "IM": "Isle of Man", "UG": "Uganda", "TZ": "Tanzania", "MY": "Malaysia", "MX": "Mexico", "IL": "Israel", "FR": "France", "IO": "British Indian Ocean Territory", "SH": "Saint Helena", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands", "FM": "Micronesia", "FO": "Faroe Islands", "NI": "Nicaragua", "NL": "Netherlands", "NO": "Norway", "NA": "Namibia", "VU": "Vanuatu", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria", "NZ": "New Zealand", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "CK": "Cook Islands", "XK": "Kosovo", "CI": "Ivory Coast", "CH": "Switzerland", "CO": "Colombia", "CN": "China", "CM": "Cameroon", "CL": "Chile", "CC": "Cocos Islands", "CA": "Canada", "CG": "Republic of the Congo", "CF": "Central African Republic", "CD": "Democratic Republic of the Congo", "CZ": "Czech Republic", "CY": "Cyprus", "CX": "Christmas Island", "CR": "Costa Rica", "CW": "Curacao", "CV": "Cape Verde", "CU": "Cuba", "SZ": "Swaziland", "SY": "Syria", "SX": "Sint Maarten", "KG": "Kyrgyzstan", "KE": "Kenya", "SS": "South Sudan", "SR": "Suriname", "KI": "Kiribati", "KH": "Cambodia", "KN": "Saint Kitts and Nevis", "KM": "Comoros", "ST": "Sao Tome and Principe", "SK": "Slovakia", "KR": "South Korea", "SI": "Slovenia", "KP": "North Korea", "KW": "Kuwait", "SN": "Senegal", "SM": "San Marino", "SL": "Sierra Leone", "SC": "Seychelles", "KZ": "Kazakhstan", "KY": "Cayman Islands", "SG": "Singapore", "SE": "Sweden", "SD": "Sudan", "DO": "Dominican Republic", "DM": "Dominica", "DJ": "Djibouti", "DK": "Denmark", "VG": "British Virgin Islands", "DE": "Germany", "YE": "Yemen", "DZ": "Algeria", "US": "United States", "UY": "Uruguay", "YT": "Mayotte", "UM": "United States Minor Outlying Islands", "LB": "Lebanon", "LC": "Saint Lucia", "LA": "Laos", "TV": "Tuvalu", "TW": "Taiwan", "TT": "Trinidad and Tobago", "TR": "Turkey", "LK": "Sri Lanka", "LI": "Liechtenstein", "LV": "Latvia", "TO": "Tonga", "LT": "Lithuania", "LU": "Luxembourg", "LR": "Liberia", "LS": "Lesotho", "TH": "Thailand", "TF": "French Southern Territories", "TG": "Togo", "TD": "Chad", "TC": "Turks and Caicos Islands", "LY": "Libya", "VA": "Vatican", "VC": "Saint Vincent and the Grenadines", "AE": "United Arab Emirates", "AD": "Andorra", "AG": "Antigua and Barbuda", "AF": "Afghanistan", "AI": "Anguilla", "VI": "U.S. Virgin Islands", "IS": "Iceland", "IR": "Iran", "AM": "Armenia", "AL": "Albania", "AO": "Angola", "AQ": "Antarctica", "AS": "American Samoa", "AR": "Argentina", "AU": "Australia", "AT": "Austria", "AW": "Aruba", "IN": "India", "AX": "Aland Islands", "AZ": "Azerbaijan", "IE": "Ireland", "ID": "Indonesia", "UA": "Ukraine", "QA": "Qatar", "MZ": "Mozambique"}	\N	SYSTEM	\N	2017-05-25 08:27:00	2017-05-25 08:27:00
59dc455aaf20b46eee	EMAIL_SUPPORT	support@r3mix.id	\N	barkah.hadi	\N	2017-10-10 10:58:18	2017-10-10 10:58:18
\.


--
-- Data for Name: app_group; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY app_group (id, name, access, category, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44980ef099e703	Administrator	[{"application":"58dd44b033808c34e2","module":"58ddfe43981a69ff67","role":["delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58ddfe4398b1cfa361","role":["create","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58ddfe43994be33717","role":["create","delete","modify","print"]},{"application":"58dd44b033808c34e2","module":"58ddfe43999e51d641","role":["create","modify","print"]},{"application":"58ddfe5a16f71146e4","module":"58e25786ab09edaf25","role":["create","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdc39d025cd","role":["print","view","modify","create","delete"]},{"application":"58dd44b033808c34e2","module":"58e2619fdce0b22e91","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdbc0e86754","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdc8ea69f0f","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58e261986cd2ddf3e3","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58e261986c60602eeb","role":["create","delete","print","view","modify"]},{"application":"58e30caa0d3fd14521","module":"58e30d8b70bf774067","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e31012f1243b761c","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e39da74b7d7578e7","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e39da74a0d08512b","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2096dd60145a","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2095e155ebe1","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2098a69e835c","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f20977d583eb8","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2097fc72e888","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38b8bd689f8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38ac179b3f3","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38c17e01482","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38d5def3a10","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38cc45af55e","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58f0b85edc82d41b02","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58f0b85ed9774d0e4c","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58ec63cb13ab70400d","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58ec63cb12f03a480b","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942db21ad8e32","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942da8127ccdb","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942d9bb19f09e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc887482d450bf","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc88714c5bfce8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc8873509fe34d","role":[]},{"application":"58e31012f065c9e35b","module":"5902fc88720a4a94c8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbfec513baa","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbc63854bd7","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbd6903c17e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e6955285336a618","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69553000303a57","role":[]},{"application":"58e31012f065c9e35b","module":"591e69552c767dd709","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552792feeec6","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552bb61b1e44","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552a9fe74375","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552d184ea908","role":["view"]},{"application":"58e31012f065c9e35b","module":"591e69552a04f04680","role":[]},{"application":"58e31012f065c9e35b","module":"591e69552dd3e7fe70","role":["view"]},{"application":"58e31012f065c9e35b","module":"591e69552b3a5c9148","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1ab2351736","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b22415fae3d","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1fea4f4613","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b23f6f121ad","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b191ae0dfbe","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1f518c7fe4","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b23669a6048","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1dbec8ebde","role":["print","view","assign","modify"]},{"application":"58e31012f065c9e35b","module":"5921bc7b207daffeb5","role":["view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b2113fce3c7","role":["view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1e9a8c37c7","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b22d4a6adc4","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e184ec61968","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1c1d98f4dc","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1ada634720","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1d0be301d0","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e17b03b19c8","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1a8e933f20","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1cbff4f4ce","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e19e5422cff","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1d60bcf090","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1b38cae843","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1b8b22e0c1","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1a3fd0e682","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1c680500b3","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4b41847e2e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4c9cf5aecc","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4eefb517d3","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4a83fbdebc","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4c47191c8a","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4e73f43cad","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4b9e9d14c4","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4fb4710dca","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4cf3003662","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4d7251bbec","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4bf24c8303","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4dfe4b3105","role":["modify","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e26bd54353","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e34e7e6755","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e1dcf4f2fe","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e2e66c1978","role":["create","delete","modify","print","view"]},{"application":"592695ca3a789ad93f","module":"592695ca3b2150693e","role":["create","delete","modify","print","view"]},{"application":"app","module":"application","role":["create","delete","modify","print","view"]},{"application":"app","module":"config","role":["create","delete","modify","print","view"]},{"application":"app","module":"group","role":["create","delete","modify","print","view"]},{"application":"app","module":"role","role":["create","delete","modify","print","view"]},{"application":"crm","module":"contact","role":[]},{"application":"pajak","module":"pegawai","role":["create","delete","modify","print","view"]},{"application":"setting","module":"company","role":["create","delete","modify","print","view"]},{"application":"setting","module":"period","role":["create","delete","modify","print","view"]},{"application":"setting","module":"user","role":["create","delete","modify","print","view"]},{"application":"setting","module":"workflow","role":["create","delete","modify","print","view"]},{"application":"pajak","module":"objek","role":["create","delete","modify","view"]},{"application":"pajak","module":"pajak","role":["create","delete","modify","view"]},{"application":"product","module":"uom","role":["create","delete","modify","view"]},{"application":"product","module":"category","role":["create","delete","modify","view"]},{"application":"product","module":"attribute","role":["create","delete","modify","view"]},{"application":"purchase","module":"supplier","role":["create","delete","modify","view"]},{"application":"sales","module":"customer","role":["create","delete","modify","view"]},{"application":"product","module":"master","role":["create","delete","modify","view"]},{"application":"inventory","module":"warehouse","role":["create","delete","modify","view"]},{"application":"setting","module":"tax","role":["create","delete","modify","view"]},{"application":"setting","module":"currency","role":["create","delete","modify","view"]},{"application":"purchase","module":"requestorder","role":["create","delete","modify","view","change","duplicate","print"]},{"application":"purchase","module":"approvalorder","role":["create","delete","modify","view","change"]},{"application":"purchase","module":"purchaseorder","role":["create","delete","modify","view","change","print"]},{"application":"sales","module":"salesorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"deliveryorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"receiptorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"incoming","role":["view","receipt"]},{"application":"inventory","module":"outgoing","role":["view","deliver"]},{"application":"inventory","module":"stock","role":["view"]},{"application":"inventory","module":"adjustment","role":["create","delete","modify","change","view"]},{"application":"inventory","module":"externaltransfer","role":["create","delete","modify","change","view"]},{"application":"inventory","module":"internaltransfer","role":["create","delete","modify","change","view"]},{"application":"chasier","module":"cashierorder","role":["modify","view"]},{"application":"chasier","module":"session","role":["create","delete","modify","view"]},{"application":"cashier","module":"cashierorder","role":["modify","view"]},{"application":"cashier","module":"session","role":[]},{"application":"purchase","module":"purchaselbs","role":[]},{"application":"inventory","module":"returnorder","role":["change","create","delete","modify","print","view"]},{"application":"report","module":"stockdaily","role":["print","view"]},{"application":"cashier","module":"bank","role":["create","delete","modify","view"]},{"application":"cashier","module":"edc","role":["create","delete","modify","view"]},{"application":"cashier","module":"surcharge","role":["create","delete","modify","view"]},{"application":"inventory","module":"incomingrequest","role":["transfer","view"]},{"application":"purchase","module":"requestnote","role":["change","create","delete","modify","print","view"]},{"application":"setting","module":"emailtemplate","role":["create","delete","modify","view"]},{"application":"dashboard","module":"general","role":["view"]},{"application":"cashier","module":"cashiertarget","role":["create","delete","modify","view"]},{"application":"cashier","module":"user","role":["create","modify","delete","view"]},{"application":"cashier","module":"shift","role":["modify","view"]},{"application":"report","module":"productdaily","role":["view"]},{"application":"report","module":"producthourly","role":["view"]},{"application":"report","module":"productsummaryhourly","role":["view"]},{"application":"report","module":"productsummary","role":["view"]}]	["59a66f453b84383b92","59a66f45440dd81ee4","59a66f4545725368e0","59a66f454680d805c5","59a66f454771b543a7","59a66f45485db4b3a1","59a66f45495bf353c4","59a66f454a50c8c4ee","59a66f454b404dccd5","59a66f454c337716cb","59a66f454d2115d310"]	SYSTEM	muhamad.fahmi	2017-03-30 17:47:04	2018-01-15 19:03:51
\.


--
-- Data for Name: app_module; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY app_module (id, application, code, name, role, created_by, updated_by, created_at, updated_at) FROM stdin;
592683e1e1dcf4f2fe	58dd44b033808c34e2	group	Group	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e26bd54353	58dd44b033808c34e2	application	Application	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e2e66c1978	58dd44b033808c34e2	role	Role	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e34e7e6755	58dd44b033808c34e2	config	Parameter Settings	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
5a9ac59ef0cedb87f9	58ddfe5a16f71146e4	user	User	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
5a9ac59ef14a1752ca	58ddfe5a16f71146e4	company	Company	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
5a9ac59ef1b2d02358	58ddfe5a16f71146e4	workflow	Workflow	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
\.


--
-- Data for Name: app_role; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY app_role (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44f4eb306ef2a9	create	Create	SYSTEM	\N	2017-03-30 17:48:36	2017-03-30 17:48:36
58dd44fbf179d136da	modify	Modify	SYSTEM	\N	2017-03-30 17:48:43	2017-03-30 17:48:43
58dd4504ac7c82f91e	delete	Delete	SYSTEM	\N	2017-03-30 17:48:52	2017-03-30 17:48:52
58dd450da1a2797c45	view	View	SYSTEM	\N	2017-03-30 17:49:01	2017-03-30 17:49:01
58dd451335c69006ff	print	Print	SYSTEM	\N	2017-03-30 17:49:07	2017-03-30 17:49:07
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY migrations (id, migration, batch) FROM stdin;
1	2014_10_12_100000_create_password_resets_table	1
2	2017_03_21_071326_create_setting_user_table	1
3	2017_03_23_064603_create_app_role_table	1
4	2017_03_30_021328_create_app_group_table	1
5	2017_03_30_024606_create_app_application_table	1
6	2017_03_30_024616_create_app_module_table	1
7	2017_03_30_035634_create_app_config_table	1
8	2017_03_31_085046_create_setting_company_table	1
9	2017_05_26_110842_create_setting_workflow_table	1
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('migrations_id_seq', 9, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY password_resets (email, token, created_at) FROM stdin;
barkah.hadi@gmail.com	$2y$10$qzEF.O7N3aCJRWEAnDdMW.hqwspPcT5nmS0YAUrJkMDsz5sHQPYz.	2017-03-30 15:13:44
\.


--
-- Data for Name: setting_company; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY setting_company (id, code, name, parent, "left", "right", depth, picture, address, city, postal_code, state, country, website, phone, fax, email, npwp, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a0e6e7f864bb09c64	DEV	Dev+ Test	\N	73	74	0	\N	Ruko Grand Galaxy City Block RSA 5 no 82	\N	\N	\N	\N	\N	085741129274	\N	\N	\N	t	SYSTEM	SYSTEM	2017-11-17 12:07:11	2017-12-13 10:18:56
\.


--
-- Data for Name: setting_user; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY setting_user (id, username, password, remember_token, fullname, nickname, email, picture, active, is_login, last_activity, count_wrong_password, "group", company, pin, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9ac484a01c81bf70	admin	$2y$10$h8JImLV61h9l4iS4qNZ45uA8TVk2EcfmkoD8e/ANN6xxt4lR.CYR.	gNymFy3GHCWY1EfrPBGGSqLszhLao5Vs0I0WZmMC9OkxkUTlVMiJg4wbCR0L	Administrator	Admin	admin@gmail.com	\N	t	f	2018-03-03 22:56:47	0	["58dd44980ef099e703"]	["5a0e6e7f864bb09c64"]	0	SYSTEM	SYSTEM	2018-03-03 22:51:32	2018-03-03 22:56:47
\.


--
-- Data for Name: setting_workflow; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY setting_workflow (id, code, name, workflow, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

