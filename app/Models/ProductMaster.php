<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use App\Models\ProductMaster;

class ProductMaster extends DevplusModel
{
  protected $table = 'product_master';
  protected $fillable = ['id', 'code', 'category', 'uom', 'name', 'type', 'barcode', 'picture', 'sale_price', 'cost_price', 'pos', 'active', 'revision', 'tax', 'discount'];

  public static function productCodeExists($productCode = null, $id = null){
    $query = ProductMaster::where('code', $productCode);
    if(!empty($id)){
      $query->where('id', '!=', $id);
    }
    $product = $query->first();
    if(!empty($product)){
      return true;
    }else{
      return false;
    }

  }

  public static function getQuery($modifierFn = null, $onlyActiveProduct = true){
    $product = self::select([
      'product_master.name AS name',
      'product_master.code AS code',
      'product_master.category AS category',
      'product_master.uom AS uom',
      'product_master.revision AS revision',
      'product_uom.uom_category AS uom_category',
      'product_master.type AS type',
      'product_master.barcode AS barcode',
      'product_master.picture AS picture',
      'product_master.sale_price AS sale_price',
      'product_master.cost_price AS cost_price',
      'product_master.pos AS pos',
      'product_master.tax AS tax',
      'product_master.discount AS discount',
      'product_master.active AS active',
      'product_master.id AS id',
      'product_master.updated_at',
    ])
    ->join('product_uom', 'product_uom.id', '=', 'product_master.uom');

    if (is_a($modifierFn, '\Closure')) {
      $product = call_user_func_array($modifierFn, [$product]);
    }
    if($onlyActiveProduct){
      $product->where([
        'product_master.active' => true,
      ]);
    }

    return $product;
  }
  
  
  public static function renderProductAttribute($dataProduct){
    $attribute = ProductAttribute::all()->toArray();
    $attribute = collect($attribute)->pluck('name', 'id')->toArray();
    $attributeValue = ProductAttributeValue::all()->toArray();
    $attributeValue = collect($attributeValue)->pluck('name', 'id')->toArray();

    if(!empty($dataProduct)){
      foreach($dataProduct as $key => $val){
        foreach($attribute as $keyAttribute => $valAttribute) {
            $dataProduct[$key][strtolower($valAttribute)] = '';
        }
        
        $productProduct = ProductProduct::where('product', $dataProduct[$key]['id'])->get()->toArray();
        if(!empty($productProduct)) {
            foreach($productProduct as $valProduct) {
                $attrJson = json_decode($valProduct['attribute'], true);
                if(!$attrJson) {
                    $attrJson = [];
                }
                $attrJson = collect($attrJson)->pluck('attribute_value', 'attribute')->toArray();
                foreach($attrJson as $attrKey => $attrVal) {
                    if(isset($attribute[$attrKey])) {
                        $dataProduct[$key][strtolower($attribute[$attrKey])] .= $attributeValue[$attrVal] . ', ';
                    }
                }
            }
        }
        
        foreach($attribute as $keyAttribute => $valAttribute) {
            $dataProduct[$key][strtolower($valAttribute)] = rtrim(trim($dataProduct[$key][strtolower($valAttribute)]), ',');
        }
      }
    }
    return $dataProduct;
  }
}
