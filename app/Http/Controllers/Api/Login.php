<?php

namespace App\Http\Controllers\Api;

use App\Models\SettingUser;
use App\Models\SettingCompany;
//use App\Models\CashierSurcharge;
//use App\Models\CashierSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\CashierBank;
use App\Models\CashierEdc;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Devplus\Params\Params;
//use Params;

class Login extends Controller
{
  // Response
  // - User Picture

  // Return
  // array:4 [
  //   "status" => true
  //   "message" => "Berhasil!"
  //   "company" => array:8 [
  //     "id" => "5984af8bb6e6e1ce0a"
  //     "code" => "AR"
  //     "name" => "KI Sentul"
  //     "address" => null
  //     "city" => null
  //     "phone" => null
  //     "postal_code" => null
  //     "email" => null
  //   ]
  //   "session" => array:3 [
  //     "id" => "59a7b78adbfdcd80fb"
  //     "code" => "AR.SS1708310001"
  //     "balance" => "9000000.00"
  //   ]
  // ]

  public function index() {

    $input = Input::all();

    $rules = [
        'username' => 'required',
        'password' => 'required|min:6',
    ];

    $validator = Validator::make($input, $rules);
    // If Validation Fails
    if ($validator->fails()) {
      return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
      exit;
    }else{
      $user = SettingUser::where('username', $input['username'])->orWhere('email', $input['username'])->first();

      if ($user) {

        if($user->active != 1){
          return response()->json(['status' => false, 'message' => 'User tidak aktif!']);
          exit;
        }else{
            $grup = json_decode($user->group);
          if (Hash::check($input['password'], $user->password)) {

              $session = [
                  'username' => $user->username,
                  'password' => $input['password']
              ];

              $company = json_decode($user->company);
              $company = collect($company)->first();
              $dataCompany = [];
              $dataSession = [];
              if(!empty($company)){
                $company = SettingCompany::find($company);
                $company = collect($company)->toArray();
                $dataCompany = [
                  'id' => $company['id'],
                  'code' => $company['code'],
                  'name' => $company['name'],
                  'address' => $company['address'],
                  'city' => $company['city'],
                  'phone' => $company['phone'],
                  'postal_code' => $company['postal_code'],
                  'email' => $company['email'],
                ];

                $chasierSession = CashierSession::where([
                  'company' => $dataCompany['id'],
                  'status' => 0
                ])->first();
                if(empty($chasierSession)){
                  return response()->json(['status' => false, 'message' => 'Sesi tidak ditemukan!']);
                  exit;
                }
                $dataSession = [
                  'id' => $chasierSession['id'],
                  'code' => $chasierSession['code'],
                  'balance' => $chasierSession['balance'],
                ];

              }

              $allUser = SettingUser::orderBy('created_at', 'ASC')->get()->toArray();
              $indexUser = collect($allUser)->pluck('username')->search($user->username);
              $indexUser = str_pad($indexUser, 3, '0', STR_PAD_LEFT);

              $dataResponse = [
                'status' => true,
                'message' => 'Berhasil!',
                'company' => $dataCompany,
                'session' => $dataSession,
                'name' => (empty($user->nickname)) ? $user->fullname : $user->nickname,
                'user_id' => $indexUser
              ];

              return response()->json($dataResponse);
              exit;

          } else {
            return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
            exit;
          }
        }
      }else{
        return response()->json(['status' => false, 'message' => 'User tidak ditemukan!']);
        exit;
      }
    }
    exit;
  }

  public function json() {
    $input = Input::all();
    $groupCashier = Params::get('GROUP_SALESMAN');
	
    $rules = [
        'username' => 'required',
        'password' => 'required|min:6',
    ];
    $validator = Validator::make($input, $rules);
    // If Validation Fails
    if ($validator->fails()) {
      return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
      exit;
    }else{
      $user = SettingUser::where('username', $input['username'])->orWhere('email', $input['username'])->first();
	  
      if ($user) {

        if($user->active != 1){
          return response()->json(['status' => false, 'message' => 'User tidak aktif!']);
          exit;
        }else{
          if (Hash::check($input['password'], $user->password)) {

              $session = [
                  'username' => $user->username,
                  'password' => $input['password']
              ];

              $company = json_decode($user->company);
              $dataCompany = [];

              if(!empty($company)){
					$valCompany = $company[0];

                  //$users = SettingUser::where('group', 'ilike', '%'.$groupCashier.'%')->where('company', 'ilike', '%'.$valCompany.'%')->get();
                  $users = SettingUser::where('company', 'ilike', '%'.$valCompany.'%')->get();
                  $salesmen = json_decode($user->group,true);
				  if (!in_array($groupCashier, $salesmen))
				  {
						return response()->json(['status' => false, 'message' => 'Bukan Login sebagai SALESMAN']);
						exit;
				  }

                  $dataUser = [];
                  if(count($users) > 0){
                    foreach($users as $valUser){
                      // die(print_r($valUser->fullname));
                      $dataUser[] = [
							'id' => $valUser->id,
							'username' => $valUser->username,
							'name' => (empty($valUser->nickname)) ? $valUser->fullname : $valUser->nickname,
							'pin' => $valUser->pin
                      ];
                    }
                  }

                  $currCompany = SettingCompany::find($valCompany);
                  $dataCompany = [
                    'id' => $currCompany->id,
                    'code' => $currCompany->code,
                    'name' => $currCompany->name,
                    'address' => $currCompany->address,
                    'city' => $currCompany->city,
                    'phone' => $currCompany->phone,
                    'postal_code' => $currCompany->postal_code,
                    'dataUser' => $dataUser,
                  ];
                }
              $dataResponse = [
                'status' 		=> true,
                'message' 		=> 'Berhasil!',
                'id' 			=> $user->id,
				'username' 		=> $user->username,
                'company_id' 	=> (empty($company)) ? null : $company[0],
                'name' 			=> (empty($user->nickname)) ? $user->fullname : $user->nickname,
                'pict' 			=> (empty($user->picture)) ? '' : Request::root().'/img/user/'.$user->picture,
                'company' 		=> $dataCompany,
              ];

              return response()->json($dataResponse);
              exit;

          } else {
            return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
            exit;
          }
        }
      }else{
        return response()->json(['status' => false, 'message' => 'User tidak ditemukan!']);
        exit;
      }
    }
    exit;
  }
}

/*
array:4 [
  "status" => true
  "message" => "Berhasil!"
  "name" => "Revia"
  "company" => array:2 [
    0 => array:4 [
      "id" => "59916fb5d4b2bfc424"
      "code" => "PC"
      "name" => "Store Puncak"
      "dataUser" => array:4 [
        0 => array:3 [
          "username" => "adel.septiana"
          "name" => "Adel"
          "pin" => 23580
        ]
        1 => array:3 [
          "username" => "tita.amelia"
          "name" => "Tita Amelia"
          "pin" => 54172
        ]
        2 => array:3 [
          "username" => "saima.siregar"
          "name" => "Aisyah Saima Siregar"
          "pin" => 84200
        ]
        3 => array:3 [
          "username" => "yulia.ayu"
          "name" => "Yulia"
          "pin" => 73317
        ]
      ]
    ]
    1 => array:4 [
      "id" => "5991700a05e21cf152"
      "code" => "ST"
      "name" => "Store Stasiun"
      "dataUser" => array:3 [
        0 => array:3 [
          "username" => "adel.septiana"
          "name" => "Adel"
          "pin" => 23580
        ]
        1 => array:3 [
          "username" => "saima.siregar"
          "name" => "Aisyah Saima Siregar"
          "pin" => 84200
        ]
        2 => array:3 [
          "username" => "yulia.ayu"
          "name" => "Yulia"
          "pin" => 73317
        ]
      ]
    ]
  ]
]
*/
