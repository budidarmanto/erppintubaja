<!-- Penjualan : @{{ data.sales }} -->

<div style="padding: 25px 20px 10px;">
  <div class="row margin-bottom-10">
    <div class="col-md-12">
    <div class="actions pull-right">
        <select ng-model="export_format" style="width:100px;display:inline-block;margin-right:10px;" class="form-control input-sm">
            <option selected="selected" value="excel">Excel</option>
            <option value="pdf">PDF</option>
        </select>
        <a target="_blank" href="@{{ 'dashboard/general/download'+( (queryString == '') ? 'format='+export_format : queryString+'&format='+export_format ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
    </div>
    </div>
  </div>
    <div class="row">
        <div class="col-md-3">
            <div id="portlet-filter" class="portlet light">
                <div id="dashboard-report-range" class="tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Kantor/Cabang</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-checkbox-list" style="padding-bottom: 0; padding-top: 0;">
                        <label class="mt-checkbox">
                            <input type="checkbox"
                            ng-model="selectAll"
                            ng-click="checkAll()" />
                            All Company
                            <span></span>
                        </label>
                        <label ng-repeat="company in data.listCompany" class="mt-checkbox">
                            <input type="checkbox"
                            ng-checked="selectedCompany.indexOf(company.id) > -1"
                            ng-click="companyChanged(company.id)" />
                            @{{ company.name }}
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            
            
            <div id="portlet-filter" class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Status</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-checkbox-list" style="padding-bottom: 0; padding-top: 0;">
                        <label class="mt-checkbox">
                            <input type="checkbox"
                            ng-model="selectAllStatus"
                            ng-click="checkAllStatus()" />
                            All Status
                            <span></span>
                        </label>
                        <label ng-repeat="status in data.statuses" class="mt-checkbox">
                            <input type="checkbox"
                            ng-checked="selectedStatus.indexOf(status.code) > -1"
                            ng-click="statusChanged(status.code)" />
                            @{{ status.label }}
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div style="display:none" class="row widget-row">
                <div class="col-md-4">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Sales (IDR)</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green fa fa-line-chart"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="@{{ totalSales }}">@{{ totalSales | number:2 }}</span>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: @{{ (targetSales > 100) ? 100 : targetSales }}%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">@{{ targetSales | number:2 }}% Target</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Target </div>
                                        <div class="status-number"> @{{ targetSales | number:2 }}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Receipts</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-red icon-wallet"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="@{{ totalTransaction | number:2 }}">
                                  @{{ totalTransaction | number:2 }}
                                </span>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: @{{ (targetTransaction > 100) ? 100 : targetTransaction }}%;" class="progress-bar progress-bar-success red-haze">
                                            <span class="sr-only">@{{ targetTransaction | number:2 }}% Target</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">Target</div>
                                        <div class="status-number"> @{{ targetTransaction | number:2 }}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Average Sale (IDR)</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="@{{ averageSales }}">
                                  @{{ averageSales | number:2 }}
                                </span>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: @{{ (targetAverageSales > 100) ? 100 : targetAverageSales }}%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">@{{ targetAverageSales | number:2 }}% Target</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> Target </div>
                                        <div class="status-number"> @{{ targetAverageSales | number:2 }}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="display:none" class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-bar-chart font-green-haze"></i>
                                <span class="caption-subject bold uppercase font-green-haze">Sales</span>
                                <span class="caption-helper">per hour</span>
                            </div>
                            <!-- <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <a
                                        id="chart-date"
                                        class="btn dashboard-btn blue btn-outline btn-circle btn-sm"
                                        href="javascript:;">
                                        <span>November 22, 2017</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </div>
                            </div> -->
                        </div>
                        <div class="portlet-body">
                            <div style="height: 400px;">
                                <am-chart id="sales_chart" options="salesChartOptions" height="100%" width="100%"></am-chart>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="margin-top: 30px;">
                <div class="row">
                  <div class="col-md-12">
                      <div class="portlet light bordered">
                          <div class="portlet-title">
                              <div class="caption">
                                  <i class="icon-star font-purple-plum"></i>
                                  <span class="caption-subject bold font-purple-plum uppercase">Sales</span>
                                  <span class="caption-helper">by product</span>
                              </div>
                          </div>
                          <div class="portlet-body" style="padding-top: 0;">
                              <div class="table-scrollable table-scrollable-borderless" style="margin-top: 0;">
                                  <table class="table table-hover table-light table-dashboard">
                                      <thead>
                                          <tr class="uppercase">
                                              <th colspan="2">PRODUCT</th>
                                              <th align="right" style="text-align:right;">QUANTITY</th>
                                              <th align="right" style="text-align:right;display:none">TOTAL</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr ng-repeat="product in dataProductTopSales">
                                              <td class="fit">
                                                  <img class="user-pic rounded" src="@{{ (product.picture != null) ? 'img/product/'+product.picture : 'img/product/placeholder.png' }}">
                                              </td>
                                              <td>
                                                  @{{ product.name }}
                                              </td>
                                              <td align="right">
                                                <span class="bold theme-font">@{{ product.qty | number:2 }}</span>
                                              </td>
                                              <td align="right" style="display:none">
                                                   @{{ product.total | number:2 }}
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
				<div class="row">
                  <div class="col-md-12">
                      <div class="portlet light bordered">
                          <div class="portlet-title">
                              <div class="caption">
                                  <i class="icon-star font-purple-plum"></i>
                                  <span class="caption-subject bold font-purple-plum uppercase">Sales</span>
                                  <span class="caption-helper">by salesman</span>
                              </div>
                          </div>
                          <div class="portlet-body" style="padding-top: 0;">
                              <div class="table-scrollable table-scrollable-borderless" style="margin-top: 0;">
                                  <table class="table table-hover table-light table-dashboard">
                                      <thead>
                                          <tr class="uppercase">
                                              <th >Name</th>
                                              <th align="right" style="text-align:right;">QUANTITY</th>
                                              <th align="right" style="text-align:right;display:none">TOTAL</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr ng-repeat="cashier in dataChasierTopSales">
                                              
                                              <td>
                                                  @{{ cashier.name }}
                                              </td>
                                              <td align="right">
                                                <span class="bold theme-font">@{{ cashier.qty | number:2 }}</span>
                                              </td>
                                              <td align="right" style="display:none">
                                                   @{{ cashier.total | number:2 }}
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
