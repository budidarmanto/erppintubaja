<?php

namespace App\Http\Controllers;

use App\Models\SettingUser;
use App\Http\Controllers\Controller;
use Datagrid;
use Input;
use Params;
use Auth;
use Illuminate\Support\Facades\DB;
use Devplus\Helper\Helper;

class UserFinder extends Controller
{
    public function index(){
        $keyword = Input::get('keyword');

        $query = SettingUser::select([
            'setting_user.id',
            'setting_user.fullname',
            'setting_user.email',
            'setting_user.username',
        ])
        ->where('setting_user.company', 'ilike', '%'. Helper::currentCompany() . '%');
    
        
        $dg = Datagrid::source($query);
        $dg->title('User');
        $dg->perPage = 10;
        $dg->optionPerpage = [10, 20, 50, 100];
        $dg->filter('keyword', function($query, $value){
          if($value != '')
              return $query->where('setting_user.fullname', 'ilike', '%'.$value.'%')
                  ->orWhere('setting_user.nikname', 'ilike', '%'.$value.'%');

          return $query;
        });
        $dg->add('fullname', 'Full Name');
        $dg->add('email', 'Email');
        $datagrid = $dg->build();
        
        return response()->json($datagrid);
    }
}
