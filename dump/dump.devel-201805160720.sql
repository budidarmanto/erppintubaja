--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_application (
    id character varying(25) NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_application OWNER TO postgres;

--
-- Name: app_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_config (
    id character varying(25) NOT NULL,
    code character varying(255) NOT NULL,
    value text NOT NULL,
    description text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_config OWNER TO postgres;

--
-- Name: app_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_group (
    id character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    access text,
    category json,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_group OWNER TO postgres;

--
-- Name: app_module; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_module (
    id character varying(25) NOT NULL,
    application character varying(25) NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    role text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_module OWNER TO postgres;

--
-- Name: app_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_role (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_role OWNER TO postgres;

--
-- Name: crm_contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE crm_contact (
    id character varying(25) NOT NULL,
    company character varying(25),
    name character varying(50) NOT NULL,
    picture character varying(255),
    address text,
    city character varying(50),
    postal_code character varying(5),
    state character varying(50),
    country character varying(50),
    website character varying(255),
    phone character varying(50),
    mobile character varying(50),
    fax character varying(50),
    email character varying(255),
    tax_no character varying(50),
    type character varying(25) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    pkp boolean DEFAULT false NOT NULL,
    term_of_payment character varying(50),
    revision integer DEFAULT 1 NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE crm_contact OWNER TO postgres;

--
-- Name: crm_contact_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE crm_contact_detail (
    id character varying(25) NOT NULL,
    contact character varying(25) NOT NULL,
    type character(2) NOT NULL,
    name character varying(50) NOT NULL,
    "position" character varying(50),
    address text,
    city character varying(50),
    postal_code character varying(5),
    state character varying(50),
    country character varying(50),
    website character varying(255),
    phone character varying(50),
    mobile character varying(50),
    fax character varying(50),
    email character varying(255),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE crm_contact_detail OWNER TO postgres;

--
-- Name: inventory_stock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_stock (
    id character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    uom character varying(25) NOT NULL,
    warehouse_location character varying(25) NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    begin_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    plus_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    min_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    end_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_stock OWNER TO postgres;

--
-- Name: inventory_stock_movement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_stock_movement (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    company character varying(25) NOT NULL,
    contact character varying(25),
    pos character varying(25),
    description text,
    source character varying(25),
    destination character varying(25),
    company_destination character varying(25),
    expected_date timestamp(0) without time zone,
    reference character varying(100),
    type character varying(5),
    status character varying(10),
    currency character varying(25),
    currency_rate numeric(18,2),
    printed integer DEFAULT 0 NOT NULL,
    category character varying(25),
    date timestamp(0) without time zone NOT NULL,
    total_discount numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    total_price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    total_tax numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    total numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    cashier_transaction_code character varying(100),
    cashier_session character varying(100),
    cashier_reference character varying(25),
    cashier_reference_no character varying(25),
    cashier_payment_type character varying(25),
    cashier_bank character varying(25),
    cashier_edc character varying(25),
    cashier_card_number character varying(25),
    cashier_surcharge numeric(18,2) DEFAULT '0'::numeric,
    cashier_customer_email character varying(25),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_stock_movement OWNER TO postgres;

--
-- Name: inventory_stock_movement_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_stock_movement_comment (
    id character varying(25) NOT NULL,
    stock_movement character varying(25) NOT NULL,
    comment text NOT NULL,
    attachment character varying(200),
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_stock_movement_comment OWNER TO postgres;

--
-- Name: inventory_stock_movement_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_stock_movement_log (
    id character varying(25) NOT NULL,
    stock_movement character varying(25) NOT NULL,
    status character varying(10) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_stock_movement_log OWNER TO postgres;

--
-- Name: inventory_stock_movement_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_stock_movement_product (
    id character varying(25) NOT NULL,
    stock_movement character varying(25) NOT NULL,
    reference character varying(25),
    product character varying(50) NOT NULL,
    uom character varying(25) NOT NULL,
    qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    rcp_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    do_qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    discount json,
    tax character varying(25),
    tax_amount numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    total numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    contact character varying(25),
    status character varying(25) DEFAULT '0'::character varying,
    expected_date timestamp(0) without time zone,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_stock_movement_product OWNER TO postgres;

--
-- Name: inventory_warehouse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_warehouse (
    id character varying(25) NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    address text,
    city character varying(50),
    postal_code character varying(5),
    state character varying(50),
    country character varying(50),
    phone character varying(50),
    fax character varying(50),
    company json,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_warehouse OWNER TO postgres;

--
-- Name: inventory_warehouse_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inventory_warehouse_location (
    id character varying(25) NOT NULL,
    warehouse character varying(25) NOT NULL,
    name character varying(50) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    pos boolean DEFAULT false NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE inventory_warehouse_location OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: product_alternative; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_alternative (
    id character varying(25) NOT NULL,
    product_master character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_alternative OWNER TO postgres;

--
-- Name: product_attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_attribute (
    id character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_attribute OWNER TO postgres;

--
-- Name: product_attribute_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_attribute_value (
    id character varying(25) NOT NULL,
    attribute character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_attribute_value OWNER TO postgres;

--
-- Name: product_bom; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_bom (
    id character varying(25) NOT NULL,
    product_master character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    uom character varying(25) NOT NULL,
    qty numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_bom OWNER TO postgres;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_category (
    id character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    parent character varying(25),
    "left" integer,
    "right" integer,
    depth integer,
    revision integer DEFAULT 1 NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_category OWNER TO postgres;

--
-- Name: product_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_master (
    id character varying(25) NOT NULL,
    code character varying(50) NOT NULL,
    category character varying(25) NOT NULL,
    uom character varying(25) NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(25) NOT NULL,
    barcode character varying(100),
    picture character varying(255),
    sale_price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    cost_price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    pos boolean DEFAULT false NOT NULL,
    tax character varying(25),
    discount numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    active boolean DEFAULT true NOT NULL,
    revision integer DEFAULT 1 NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_master OWNER TO postgres;

--
-- Name: product_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_product (
    id character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    attribute json,
    price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_product OWNER TO postgres;

--
-- Name: product_supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_supplier (
    id character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    supplier character varying(25) NOT NULL,
    price numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    tax character varying(25),
    discount numeric(18,2) DEFAULT '0'::numeric NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_supplier OWNER TO postgres;

--
-- Name: product_uom; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_uom (
    id character varying(25) NOT NULL,
    uom_category character varying(25) NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    type character varying(3) NOT NULL,
    ratio numeric(18,2) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_uom OWNER TO postgres;

--
-- Name: product_uom_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_uom_category (
    id character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_uom_category OWNER TO postgres;

--
-- Name: product_variant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_variant (
    id character varying(25) NOT NULL,
    product character varying(25) NOT NULL,
    attribute character varying(25) NOT NULL,
    attribute_value character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE product_variant OWNER TO postgres;

--
-- Name: setting_company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_company (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(50) NOT NULL,
    parent character varying(25),
    "left" integer,
    "right" integer,
    depth integer,
    picture character varying(100),
    address text,
    city character varying(50),
    postal_code character varying(5),
    state character varying(50),
    country character varying(50),
    website character varying(255),
    phone character varying(50),
    fax character varying(50),
    email character varying(255),
    npwp character varying(50),
    active boolean DEFAULT false NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_company OWNER TO postgres;

--
-- Name: setting_currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_currency (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(50) NOT NULL,
    "default" boolean DEFAULT false NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_currency OWNER TO postgres;

--
-- Name: setting_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_period (
    id character varying(25) NOT NULL,
    year character varying(5) NOT NULL,
    month text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_period OWNER TO postgres;

--
-- Name: setting_tax; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_tax (
    id character varying(25) NOT NULL,
    name character varying(50) NOT NULL,
    amount numeric(18,2) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_tax OWNER TO postgres;

--
-- Name: setting_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_user (
    id character varying(25) NOT NULL,
    username character varying(25) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(255),
    fullname character varying(50) NOT NULL,
    nickname character varying(50),
    email character varying(255) NOT NULL,
    picture character varying(255),
    active boolean DEFAULT false NOT NULL,
    is_login boolean DEFAULT false NOT NULL,
    last_activity timestamp(0) without time zone,
    count_wrong_password integer DEFAULT 0 NOT NULL,
    "group" text,
    company text,
    pin integer DEFAULT 0,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_user OWNER TO postgres;

--
-- Name: setting_workflow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE setting_workflow (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(60) NOT NULL,
    workflow json,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE setting_workflow OWNER TO postgres;

--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Data for Name: app_application; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_application (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44b033808c34e2	app	System Configuration	SYSTEM	\N	2017-03-30 17:47:28	2017-03-30 17:47:28
58ddfe5a16f71146e4	setting	Settings	SYSTEM	\N	2017-03-31 06:59:38	2017-03-31 06:59:38
5ac4ceae5113746795	product	Product	admin	\N	2018-04-04 20:10:06	2018-04-04 20:10:06
5ac4cfdfc558c15a03	sales	Sales	admin	\N	2018-04-04 20:15:11	2018-04-04 20:15:11
5ac6e7e408787420f4	inventory	Inventori	admin	\N	2018-04-06 10:22:12	2018-04-06 10:22:12
\.


--
-- Data for Name: app_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_config (id, code, value, description, created_by, updated_by, created_at, updated_at) FROM stdin;
59269554ea8fd90f80	OPTION_COUNTRY	{"BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria", "BA": "Bosnia and Herzegovina", "BB": "Barbados", "WF": "Wallis and Futuna", "BL": "Saint Barthelemy", "BM": "Bermuda", "BN": "Brunei", "BO": "Bolivia", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin", "BT": "Bhutan", "JM": "Jamaica", "BV": "Bouvet Island", "BW": "Botswana", "WS": "Samoa", "BQ": "Bonaire, Saint Eustatius and Saba ", "BR": "Brazil", "BS": "Bahamas", "JE": "Jersey", "BY": "Belarus", "BZ": "Belize", "RU": "Russia", "RW": "Rwanda", "RS": "Serbia", "TL": "East Timor", "RE": "Reunion", "TM": "Turkmenistan", "TJ": "Tajikistan", "RO": "Romania", "TK": "Tokelau", "GW": "Guinea-Bissau", "GU": "Guam", "GT": "Guatemala", "GS": "South Georgia and the South Sandwich Islands", "GR": "Greece", "GQ": "Equatorial Guinea", "GP": "Guadeloupe", "JP": "Japan", "GY": "Guyana", "GG": "Guernsey", "GF": "French Guiana", "GE": "Georgia", "GD": "Grenada", "GB": "United Kingdom", "GA": "Gabon", "SV": "El Salvador", "GN": "Guinea", "GM": "Gambia", "GL": "Greenland", "GI": "Gibraltar", "GH": "Ghana", "OM": "Oman", "TN": "Tunisia", "JO": "Jordan", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "HK": "Hong Kong", "HN": "Honduras", "HM": "Heard Island and McDonald Islands", "VE": "Venezuela", "PR": "Puerto Rico", "PS": "Palestinian Territory", "PW": "Palau", "PT": "Portugal", "SJ": "Svalbard and Jan Mayen", "PY": "Paraguay", "IQ": "Iraq", "PA": "Panama", "PF": "French Polynesia", "PG": "Papua New Guinea", "PE": "Peru", "PK": "Pakistan", "PH": "Philippines", "PN": "Pitcairn", "PL": "Poland", "PM": "Saint Pierre and Miquelon", "ZM": "Zambia", "EH": "Western Sahara", "EE": "Estonia", "EG": "Egypt", "ZA": "South Africa", "EC": "Ecuador", "IT": "Italy", "VN": "Vietnam", "SB": "Solomon Islands", "ET": "Ethiopia", "SO": "Somalia", "ZW": "Zimbabwe", "SA": "Saudi Arabia", "ES": "Spain", "ER": "Eritrea", "ME": "Montenegro", "MD": "Moldova", "MG": "Madagascar", "MF": "Saint Martin", "MA": "Morocco", "MC": "Monaco", "UZ": "Uzbekistan", "MM": "Myanmar", "ML": "Mali", "MO": "Macao", "MN": "Mongolia", "MH": "Marshall Islands", "MK": "Macedonia", "MU": "Mauritius", "MT": "Malta", "MW": "Malawi", "MV": "Maldives", "MQ": "Martinique", "MP": "Northern Mariana Islands", "MS": "Montserrat", "MR": "Mauritania", "IM": "Isle of Man", "UG": "Uganda", "TZ": "Tanzania", "MY": "Malaysia", "MX": "Mexico", "IL": "Israel", "FR": "France", "IO": "British Indian Ocean Territory", "SH": "Saint Helena", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands", "FM": "Micronesia", "FO": "Faroe Islands", "NI": "Nicaragua", "NL": "Netherlands", "NO": "Norway", "NA": "Namibia", "VU": "Vanuatu", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria", "NZ": "New Zealand", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "CK": "Cook Islands", "XK": "Kosovo", "CI": "Ivory Coast", "CH": "Switzerland", "CO": "Colombia", "CN": "China", "CM": "Cameroon", "CL": "Chile", "CC": "Cocos Islands", "CA": "Canada", "CG": "Republic of the Congo", "CF": "Central African Republic", "CD": "Democratic Republic of the Congo", "CZ": "Czech Republic", "CY": "Cyprus", "CX": "Christmas Island", "CR": "Costa Rica", "CW": "Curacao", "CV": "Cape Verde", "CU": "Cuba", "SZ": "Swaziland", "SY": "Syria", "SX": "Sint Maarten", "KG": "Kyrgyzstan", "KE": "Kenya", "SS": "South Sudan", "SR": "Suriname", "KI": "Kiribati", "KH": "Cambodia", "KN": "Saint Kitts and Nevis", "KM": "Comoros", "ST": "Sao Tome and Principe", "SK": "Slovakia", "KR": "South Korea", "SI": "Slovenia", "KP": "North Korea", "KW": "Kuwait", "SN": "Senegal", "SM": "San Marino", "SL": "Sierra Leone", "SC": "Seychelles", "KZ": "Kazakhstan", "KY": "Cayman Islands", "SG": "Singapore", "SE": "Sweden", "SD": "Sudan", "DO": "Dominican Republic", "DM": "Dominica", "DJ": "Djibouti", "DK": "Denmark", "VG": "British Virgin Islands", "DE": "Germany", "YE": "Yemen", "DZ": "Algeria", "US": "United States", "UY": "Uruguay", "YT": "Mayotte", "UM": "United States Minor Outlying Islands", "LB": "Lebanon", "LC": "Saint Lucia", "LA": "Laos", "TV": "Tuvalu", "TW": "Taiwan", "TT": "Trinidad and Tobago", "TR": "Turkey", "LK": "Sri Lanka", "LI": "Liechtenstein", "LV": "Latvia", "TO": "Tonga", "LT": "Lithuania", "LU": "Luxembourg", "LR": "Liberia", "LS": "Lesotho", "TH": "Thailand", "TF": "French Southern Territories", "TG": "Togo", "TD": "Chad", "TC": "Turks and Caicos Islands", "LY": "Libya", "VA": "Vatican", "VC": "Saint Vincent and the Grenadines", "AE": "United Arab Emirates", "AD": "Andorra", "AG": "Antigua and Barbuda", "AF": "Afghanistan", "AI": "Anguilla", "VI": "U.S. Virgin Islands", "IS": "Iceland", "IR": "Iran", "AM": "Armenia", "AL": "Albania", "AO": "Angola", "AQ": "Antarctica", "AS": "American Samoa", "AR": "Argentina", "AU": "Australia", "AT": "Austria", "AW": "Aruba", "IN": "India", "AX": "Aland Islands", "AZ": "Azerbaijan", "IE": "Ireland", "ID": "Indonesia", "UA": "Ukraine", "QA": "Qatar", "MZ": "Mozambique"}	\N	SYSTEM	\N	2017-05-25 08:27:00	2017-05-25 08:27:00
59dc455aaf20b46eee	EMAIL_SUPPORT	support@r3mix.id	\N	barkah.hadi	\N	2017-10-10 10:58:18	2017-10-10 10:58:18
\.


--
-- Data for Name: app_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_group (id, name, access, category, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44980ef099e703	Administrator	[{"application":"58dd44b033808c34e2","module":"58ddfe43981a69ff67","role":["delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58ddfe4398b1cfa361","role":["create","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58ddfe43994be33717","role":["create","delete","modify","print"]},{"application":"58dd44b033808c34e2","module":"58ddfe43999e51d641","role":["create","modify","print"]},{"application":"58ddfe5a16f71146e4","module":"58e25786ab09edaf25","role":["create","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdc39d025cd","role":["print","view","modify","create","delete"]},{"application":"58dd44b033808c34e2","module":"58e2619fdce0b22e91","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdbc0e86754","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"58e2619fdc8ea69f0f","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58e261986cd2ddf3e3","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58e261986c60602eeb","role":["create","delete","print","view","modify"]},{"application":"58e30caa0d3fd14521","module":"58e30d8b70bf774067","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e31012f1243b761c","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e39da74b7d7578e7","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58e39da74a0d08512b","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2096dd60145a","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2095e155ebe1","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2098a69e835c","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f20977d583eb8","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5f2097fc72e888","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38b8bd689f8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38ac179b3f3","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38c17e01482","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38d5def3a10","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58ec5fd38cc45af55e","role":["create","delete","modify","view"]},{"application":"58e31012f065c9e35b","module":"58f0b85edc82d41b02","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"58f0b85ed9774d0e4c","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58ec63cb13ab70400d","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58ec63cb12f03a480b","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942db21ad8e32","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942da8127ccdb","role":["create","delete","modify","print","view"]},{"application":"58ddfe5a16f71146e4","module":"58f0b942d9bb19f09e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc887482d450bf","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc88714c5bfce8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fc8873509fe34d","role":[]},{"application":"58e31012f065c9e35b","module":"5902fc88720a4a94c8","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbfec513baa","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbc63854bd7","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5902fd8bbd6903c17e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e6955285336a618","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69553000303a57","role":[]},{"application":"58e31012f065c9e35b","module":"591e69552c767dd709","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552792feeec6","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552bb61b1e44","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552a9fe74375","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"591e69552d184ea908","role":["view"]},{"application":"58e31012f065c9e35b","module":"591e69552a04f04680","role":[]},{"application":"58e31012f065c9e35b","module":"591e69552dd3e7fe70","role":["view"]},{"application":"58e31012f065c9e35b","module":"591e69552b3a5c9148","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1ab2351736","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b22415fae3d","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1fea4f4613","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b23f6f121ad","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b191ae0dfbe","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1f518c7fe4","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b23669a6048","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1dbec8ebde","role":["print","view","assign","modify"]},{"application":"58e31012f065c9e35b","module":"5921bc7b207daffeb5","role":["view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b2113fce3c7","role":["view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b1e9a8c37c7","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5921bc7b22d4a6adc4","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e184ec61968","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1c1d98f4dc","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1ada634720","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1d0be301d0","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e17b03b19c8","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1a8e933f20","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1cbff4f4ce","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922402e19e5422cff","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1d60bcf090","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1b38cae843","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1b8b22e0c1","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922402e1a3fd0e682","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5922402e1c680500b3","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4b41847e2e","role":["create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4c9cf5aecc","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4eefb517d3","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4a83fbdebc","role":["assign","create","delete","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4c47191c8a","role":["modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4e73f43cad","role":["modify","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4b9e9d14c4","role":["assign","modify","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4fb4710dca","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4cf3003662","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4d7251bbec","role":["view"]},{"application":"58e31012f065c9e35b","module":"5922445a4bf24c8303","role":["delete","print","view"]},{"application":"58e31012f065c9e35b","module":"5922445a4dfe4b3105","role":["modify","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e26bd54353","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e34e7e6755","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e1dcf4f2fe","role":["create","delete","modify","print","view"]},{"application":"58dd44b033808c34e2","module":"592683e1e2e66c1978","role":["create","delete","modify","print","view"]},{"application":"592695ca3a789ad93f","module":"592695ca3b2150693e","role":["create","delete","modify","print","view"]},{"application":"app","module":"application","role":["create","delete","modify","print","view"]},{"application":"app","module":"config","role":["create","delete","modify","print","view"]},{"application":"app","module":"group","role":["create","delete","modify","print","view"]},{"application":"app","module":"role","role":["create","delete","modify","print","view"]},{"application":"crm","module":"contact","role":[]},{"application":"pajak","module":"pegawai","role":["create","delete","modify","print","view"]},{"application":"setting","module":"company","role":["create","delete","modify","print","view"]},{"application":"setting","module":"period","role":["create","delete","modify","print","view"]},{"application":"setting","module":"user","role":["create","delete","modify","print","view"]},{"application":"setting","module":"workflow","role":["create","delete","modify","print","view"]},{"application":"pajak","module":"objek","role":["create","delete","modify","view"]},{"application":"pajak","module":"pajak","role":["create","delete","modify","view"]},{"application":"product","module":"uom","role":["create","delete","modify","view","print"]},{"application":"product","module":"category","role":["create","delete","modify","view"]},{"application":"product","module":"attribute","role":["create","delete","modify","view"]},{"application":"purchase","module":"supplier","role":["create","delete","modify","view"]},{"application":"sales","module":"customer","role":["create","delete","modify","view"]},{"application":"product","module":"master","role":["create","delete","modify","view"]},{"application":"inventory","module":"warehouse","role":[]},{"application":"setting","module":"tax","role":["create","delete","modify","view"]},{"application":"setting","module":"currency","role":["create","delete","modify","view"]},{"application":"purchase","module":"requestorder","role":["create","delete","modify","view","change","duplicate","print"]},{"application":"purchase","module":"approvalorder","role":["create","delete","modify","view","change"]},{"application":"purchase","module":"purchaseorder","role":["create","delete","modify","view","change","print"]},{"application":"sales","module":"salesorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"deliveryorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"receiptorder","role":["create","delete","modify","view","change","print"]},{"application":"inventory","module":"incoming","role":["view","receipt"]},{"application":"inventory","module":"outgoing","role":["view","deliver"]},{"application":"inventory","module":"stock","role":[]},{"application":"inventory","module":"adjustment","role":["create","delete","modify","change","view"]},{"application":"inventory","module":"externaltransfer","role":["create","delete","modify","change","view"]},{"application":"inventory","module":"internaltransfer","role":["create","delete","modify","change","view"]},{"application":"chasier","module":"cashierorder","role":["modify","view"]},{"application":"chasier","module":"session","role":["create","delete","modify","view"]},{"application":"cashier","module":"cashierorder","role":["modify","view"]},{"application":"cashier","module":"session","role":[]},{"application":"purchase","module":"purchaselbs","role":[]},{"application":"inventory","module":"returnorder","role":["change","create","delete","modify","print","view"]},{"application":"report","module":"stockdaily","role":["print","view"]},{"application":"cashier","module":"bank","role":["create","delete","modify","view"]},{"application":"cashier","module":"edc","role":["create","delete","modify","view"]},{"application":"cashier","module":"surcharge","role":["create","delete","modify","view"]},{"application":"inventory","module":"incomingrequest","role":["transfer","view"]},{"application":"purchase","module":"requestnote","role":["change","create","delete","modify","print","view"]},{"application":"setting","module":"emailtemplate","role":["create","delete","modify","view"]},{"application":"dashboard","module":"general","role":["view"]},{"application":"cashier","module":"cashiertarget","role":["create","delete","modify","view"]},{"application":"cashier","module":"user","role":["create","modify","delete","view"]},{"application":"cashier","module":"shift","role":["modify","view"]},{"application":"report","module":"productdaily","role":["view"]},{"application":"report","module":"producthourly","role":["view"]},{"application":"report","module":"productsummaryhourly","role":["view"]},{"application":"report","module":"productsummary","role":["view"]},{"application":"sales","module":"sales-order","role":["create","delete","modify","print","view"]}]	["59a66f453b84383b92","59a66f45440dd81ee4","59a66f4545725368e0","59a66f454680d805c5","59a66f454771b543a7","59a66f45485db4b3a1","59a66f45495bf353c4","59a66f454a50c8c4ee","59a66f454b404dccd5","59a66f454c337716cb","59a66f454d2115d310"]	SYSTEM	admin	2017-03-30 17:47:04	2018-04-12 12:13:21
\.


--
-- Data for Name: app_module; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_module (id, application, code, name, role, created_by, updated_by, created_at, updated_at) FROM stdin;
592683e1e1dcf4f2fe	58dd44b033808c34e2	group	Group	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e26bd54353	58dd44b033808c34e2	application	Application	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e2e66c1978	58dd44b033808c34e2	role	Role	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
592683e1e34e7e6755	58dd44b033808c34e2	config	Parameter Settings	["create","modify","delete","view","print"]	SYSTEM	\N	2017-05-25 07:12:33	2017-05-25 07:12:33
5a9ac59ef0cedb87f9	58ddfe5a16f71146e4	user	User	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
5a9ac59ef14a1752ca	58ddfe5a16f71146e4	company	Company	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
5a9ac59ef1b2d02358	58ddfe5a16f71146e4	workflow	Workflow	["create","modify","delete","view","print"]	admin	\N	2018-03-03 22:56:14	2018-03-03 22:56:14
5ac4cf0fa096bfdada	5ac4ceae5113746795	uom	Satuan	["create","modify","delete","view","print"]	admin	\N	2018-04-04 20:11:43	2018-04-04 20:11:43
5ac4cf0fa11ae58839	5ac4ceae5113746795	attribute	Variant	["create","modify","delete","view","print"]	admin	\N	2018-04-04 20:11:43	2018-04-04 20:11:43
5ac4cf0fa1816c4c19	5ac4ceae5113746795	category	Category	["create","modify","delete","view","print"]	admin	\N	2018-04-04 20:11:43	2018-04-04 20:11:43
5ac4cf0fa1e3b2ea7a	5ac4ceae5113746795	master	Product	["create","modify","delete","view","print"]	admin	\N	2018-04-04 20:11:43	2018-04-04 20:11:43
5ac4ecff27d52b570d	5ac4cfdfc558c15a03	customer	Customer	["create","modify","delete","view","print"]	admin	\N	2018-04-04 22:19:27	2018-04-04 22:19:27
5ac4ecff2edf21fc12	5ac4cfdfc558c15a03	salesorder	Sales Order	["create","modify","delete","view","print"]	admin	\N	2018-04-04 22:19:27	2018-04-04 22:19:27
5ac6e7e4093290bee7	5ac6e7e408787420f4	warehouse	Warehouse	["create","modify","delete","view"]	admin	\N	2018-04-06 10:22:12	2018-04-06 10:22:12
5ac6e7e40a5d74c98e	5ac6e7e408787420f4	stock	Persediaan	["view"]	admin	\N	2018-04-06 10:22:12	2018-04-06 10:22:12
\.


--
-- Data for Name: app_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_role (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
58dd44f4eb306ef2a9	create	Create	SYSTEM	\N	2017-03-30 17:48:36	2017-03-30 17:48:36
58dd44fbf179d136da	modify	Modify	SYSTEM	\N	2017-03-30 17:48:43	2017-03-30 17:48:43
58dd4504ac7c82f91e	delete	Delete	SYSTEM	\N	2017-03-30 17:48:52	2017-03-30 17:48:52
58dd450da1a2797c45	view	View	SYSTEM	\N	2017-03-30 17:49:01	2017-03-30 17:49:01
58dd451335c69006ff	print	Print	SYSTEM	\N	2017-03-30 17:49:07	2017-03-30 17:49:07
\.


--
-- Data for Name: crm_contact; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY crm_contact (id, company, name, picture, address, city, postal_code, state, country, website, phone, mobile, fax, email, tax_no, type, active, pkp, term_of_payment, revision, created_by, updated_by, created_at, updated_at) FROM stdin;
5afac1bf3c7f002440	\N	Damian Sitorus	\N	JL. Pekayon Raya No 54	Bekasi	13332	\N	\N	\N	\N	\N	\N	\N	\N	customer	t	f	\N	1	admin	\N	2018-05-15 18:17:19	2018-05-15 18:17:19
\.


--
-- Data for Name: crm_contact_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY crm_contact_detail (id, contact, type, name, "position", address, city, postal_code, state, country, website, phone, mobile, fax, email, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_stock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_stock (id, product, uom, warehouse_location, date, begin_qty, plus_qty, min_qty, end_qty, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_stock_movement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_stock_movement (id, code, company, contact, pos, description, source, destination, company_destination, expected_date, reference, type, status, currency, currency_rate, printed, category, date, total_discount, total_price, total_tax, total, cashier_transaction_code, cashier_session, cashier_reference, cashier_reference_no, cashier_payment_type, cashier_bank, cashier_edc, cashier_card_number, cashier_surcharge, cashier_customer_email, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_stock_movement_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_stock_movement_comment (id, stock_movement, comment, attachment, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_stock_movement_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_stock_movement_log (id, stock_movement, status, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_stock_movement_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_stock_movement_product (id, stock_movement, reference, product, uom, qty, rcp_qty, do_qty, price, discount, tax, tax_amount, total, contact, status, expected_date, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_warehouse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_warehouse (id, code, name, address, city, postal_code, state, country, phone, fax, company, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: inventory_warehouse_location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inventory_warehouse_location (id, warehouse, name, active, pos, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY migrations (id, migration, batch) FROM stdin;
1	2014_10_12_100000_create_password_resets_table	1
2	2017_03_21_071326_create_setting_user_table	1
3	2017_03_23_064603_create_app_role_table	1
4	2017_03_30_021328_create_app_group_table	1
5	2017_03_30_024606_create_app_application_table	1
6	2017_03_30_024616_create_app_module_table	1
7	2017_03_30_035634_create_app_config_table	1
8	2017_03_31_085046_create_setting_company_table	1
9	2017_05_26_110842_create_setting_workflow_table	1
10	2017_04_03_091636_create_crm_contact_table	2
11	2017_04_03_092626_create_crm_contact_detail_table	2
12	2017_04_04_032351_create_setting_period_table	2
13	2017_05_26_110843_create_setting_currency_table	2
14	2017_05_26_110844_create_setting_tax_table	2
15	2017_07_27_102109_create_product_uom_category_table	2
16	2017_07_27_102115_create_product_uom_table	2
17	2017_07_27_132809_create_product_category_table	2
18	2017_07_27_144432_create_product_attribute_table	2
19	2017_07_27_144438_create_product_attribute_value_table	2
20	2017_07_31_081724_create_product_master_table	2
21	2017_07_31_111643_create_product_supplier_table	2
22	2017_07_31_164356_create_product_variant_table	2
23	2017_07_31_165751_create_product_product_table	2
24	2017_08_02_042600_create_product_bom_table	2
25	2017_08_02_102436_create_product_alternative_table	2
26	2017_08_02_174243_create_inventory_warehouse_table	2
27	2017_08_02_174250_create_inventory_warehouse_location_table	2
28	2017_08_04_070438_create_inventory_stock_movement_table	2
29	2017_08_04_070443_create_inventory_stock_movement_product_table	2
30	2017_08_04_070546_create_inventory_stock_movement_log_table	2
31	2017_08_04_070623_create_inventory_stock_table	2
32	2017_08_24_154740_create_inventory_stock_movement_comment_table	2
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 32, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY password_resets (email, token, created_at) FROM stdin;
barkah.hadi@gmail.com	$2y$10$qzEF.O7N3aCJRWEAnDdMW.hqwspPcT5nmS0YAUrJkMDsz5sHQPYz.	2017-03-30 15:13:44
\.


--
-- Data for Name: product_alternative; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_alternative (id, product_master, product, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_attribute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_attribute (id, name, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_attribute_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_attribute_value (id, attribute, name, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_bom; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_bom (id, product_master, product, uom, qty, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_category (id, name, parent, "left", "right", depth, revision, created_by, updated_by, created_at, updated_at) FROM stdin;
5ad614d415018cc03e	Single Door	\N	1	2	0	1	SYSTEM	\N	2018-04-17 22:37:56	2018-04-17 22:37:56
5ad614e2a18c7fc7e1	Double Door	\N	3	4	0	1	SYSTEM	\N	2018-04-17 22:38:10	2018-04-17 22:38:10
5ad614f2f12232bdfa	Special Door	\N	5	6	0	1	SYSTEM	\N	2018-04-17 22:38:26	2018-04-17 22:38:26
5ad615022e454378a5	Accessories	\N	7	8	0	1	SYSTEM	\N	2018-04-17 22:38:42	2018-04-17 22:38:42
5afac223b476973ebd	Pintu Satu	5afac2134901c05e10	10	11	1	1	SYSTEM	\N	2018-05-15 18:18:59	2018-05-15 18:18:59
5afac2134901c05e10	Door	\N	9	14	0	1	SYSTEM	\N	2018-05-15 18:18:43	2018-05-15 18:19:46
5afac25248d9425801	Pintu Dua	5afac2134901c05e10	12	13	1	1	SYSTEM	\N	2018-05-15 18:19:46	2018-05-15 18:19:46
\.


--
-- Data for Name: product_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_master (id, code, category, uom, name, type, barcode, picture, sale_price, cost_price, pos, tax, discount, active, revision, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_product (id, product, attribute, price, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_supplier (id, product, supplier, price, tax, discount, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_uom; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_uom (id, uom_category, code, name, type, ratio, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_uom_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_uom_category (id, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5ad61583c1e95d529f	Satuan	admin	\N	2018-04-17 22:40:51	2018-04-17 22:40:51
5ad6158d5d450e0e7f	Berat	admin	\N	2018-04-17 22:41:01	2018-04-17 22:41:01
5ad61599b000095bdc	Volume	admin	\N	2018-04-17 22:41:13	2018-04-17 22:41:13
5afac1fb696b7cdd19	Mandays	admin	\N	2018-05-15 18:18:19	2018-05-15 18:18:19
\.


--
-- Data for Name: product_variant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_variant (id, product, attribute, attribute_value, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: setting_company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_company (id, code, name, parent, "left", "right", depth, picture, address, city, postal_code, state, country, website, phone, fax, email, npwp, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a0e6e7f864bb09c64	DEV	Dev+ Test	\N	73	74	0	\N	Ruko Grand Galaxy City Block RSA 5 no 82	\N	\N	\N	\N	\N	085741129274	\N	\N	\N	t	SYSTEM	SYSTEM	2017-11-17 12:07:11	2017-12-13 10:18:56
\.


--
-- Data for Name: setting_currency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_currency (id, code, name, "default", active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: setting_period; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_period (id, year, month, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: setting_tax; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_tax (id, name, amount, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: setting_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_user (id, username, password, remember_token, fullname, nickname, email, picture, active, is_login, last_activity, count_wrong_password, "group", company, pin, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9ac484a01c81bf70	admin	$2a$10$PgKk/9FyhxJSsrmgshAdoe60FCU/pTOnXEm28cGLAaJnbdVcynusy	deOiENInZ5IUjvZye3h80KAHRucQdcZfOeKxtPRnSNs70Gxp6u1QzObM1GTe	Administrator	Admin	admin@gmail.com	\N	t	f	2018-05-16 07:17:25	0	["58dd44980ef099e703"]	["5a0e6e7f864bb09c64"]	0	SYSTEM	SYSTEM	2018-03-03 22:51:32	2018-05-16 07:17:25
\.


--
-- Data for Name: setting_workflow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY setting_workflow (id, code, name, workflow, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Name: app_application app_application_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_application
    ADD CONSTRAINT app_application_code_unique UNIQUE (code);


--
-- Name: app_application app_application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_application
    ADD CONSTRAINT app_application_pkey PRIMARY KEY (id);


--
-- Name: app_config app_config_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_config
    ADD CONSTRAINT app_config_code_unique UNIQUE (code);


--
-- Name: app_config app_config_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_config
    ADD CONSTRAINT app_config_pkey PRIMARY KEY (id);


--
-- Name: app_group app_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_pkey PRIMARY KEY (id);


--
-- Name: app_module app_module_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_module
    ADD CONSTRAINT app_module_pkey PRIMARY KEY (id);


--
-- Name: app_role app_role_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_role
    ADD CONSTRAINT app_role_code_unique UNIQUE (code);


--
-- Name: app_role app_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_role
    ADD CONSTRAINT app_role_pkey PRIMARY KEY (id);


--
-- Name: crm_contact_detail crm_contact_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crm_contact_detail
    ADD CONSTRAINT crm_contact_detail_pkey PRIMARY KEY (id);


--
-- Name: crm_contact crm_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crm_contact
    ADD CONSTRAINT crm_contact_pkey PRIMARY KEY (id);


--
-- Name: inventory_stock_movement inventory_stock_movement_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement
    ADD CONSTRAINT inventory_stock_movement_code_unique UNIQUE (code);


--
-- Name: inventory_stock_movement_comment inventory_stock_movement_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_comment
    ADD CONSTRAINT inventory_stock_movement_comment_pkey PRIMARY KEY (id);


--
-- Name: inventory_stock_movement_log inventory_stock_movement_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_log
    ADD CONSTRAINT inventory_stock_movement_log_pkey PRIMARY KEY (id);


--
-- Name: inventory_stock_movement inventory_stock_movement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement
    ADD CONSTRAINT inventory_stock_movement_pkey PRIMARY KEY (id);


--
-- Name: inventory_stock_movement_product inventory_stock_movement_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_product
    ADD CONSTRAINT inventory_stock_movement_product_pkey PRIMARY KEY (id);


--
-- Name: inventory_stock inventory_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock
    ADD CONSTRAINT inventory_stock_pkey PRIMARY KEY (id);


--
-- Name: inventory_warehouse_location inventory_warehouse_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_warehouse_location
    ADD CONSTRAINT inventory_warehouse_location_pkey PRIMARY KEY (id);


--
-- Name: inventory_warehouse inventory_warehouse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_warehouse
    ADD CONSTRAINT inventory_warehouse_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: product_alternative product_alternative_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_alternative
    ADD CONSTRAINT product_alternative_pkey PRIMARY KEY (id);


--
-- Name: product_attribute product_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_attribute
    ADD CONSTRAINT product_attribute_pkey PRIMARY KEY (id);


--
-- Name: product_attribute_value product_attribute_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_attribute_value
    ADD CONSTRAINT product_attribute_value_pkey PRIMARY KEY (id);


--
-- Name: product_bom product_bom_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_bom
    ADD CONSTRAINT product_bom_pkey PRIMARY KEY (id);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: product_master product_master_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_master
    ADD CONSTRAINT product_master_code_unique UNIQUE (code);


--
-- Name: product_master product_master_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_master
    ADD CONSTRAINT product_master_pkey PRIMARY KEY (id);


--
-- Name: product_product product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- Name: product_supplier product_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_supplier
    ADD CONSTRAINT product_supplier_pkey PRIMARY KEY (id);


--
-- Name: product_supplier product_supplier_product_supplier_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_supplier
    ADD CONSTRAINT product_supplier_product_supplier_unique UNIQUE (product, supplier);


--
-- Name: product_uom_category product_uom_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_uom_category
    ADD CONSTRAINT product_uom_category_pkey PRIMARY KEY (id);


--
-- Name: product_uom product_uom_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_uom
    ADD CONSTRAINT product_uom_code_unique UNIQUE (code);


--
-- Name: product_uom product_uom_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_uom
    ADD CONSTRAINT product_uom_pkey PRIMARY KEY (id);


--
-- Name: product_variant product_variant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_variant
    ADD CONSTRAINT product_variant_pkey PRIMARY KEY (id);


--
-- Name: setting_company setting_company_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_company
    ADD CONSTRAINT setting_company_code_unique UNIQUE (code);


--
-- Name: setting_company setting_company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_company
    ADD CONSTRAINT setting_company_pkey PRIMARY KEY (id);


--
-- Name: setting_currency setting_currency_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_currency
    ADD CONSTRAINT setting_currency_code_unique UNIQUE (code);


--
-- Name: setting_currency setting_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_currency
    ADD CONSTRAINT setting_currency_pkey PRIMARY KEY (id);


--
-- Name: setting_period setting_period_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_period
    ADD CONSTRAINT setting_period_pkey PRIMARY KEY (id);


--
-- Name: setting_period setting_period_year_month_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_period
    ADD CONSTRAINT setting_period_year_month_unique UNIQUE (year, month);


--
-- Name: setting_tax setting_tax_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_tax
    ADD CONSTRAINT setting_tax_pkey PRIMARY KEY (id);


--
-- Name: setting_user setting_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_user
    ADD CONSTRAINT setting_user_pkey PRIMARY KEY (id);


--
-- Name: setting_workflow setting_workflow_code_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_workflow
    ADD CONSTRAINT setting_workflow_code_unique UNIQUE (code);


--
-- Name: setting_workflow setting_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_workflow
    ADD CONSTRAINT setting_workflow_pkey PRIMARY KEY (id);


--
-- Name: setting_user username; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY setting_user
    ADD CONSTRAINT username UNIQUE (email);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_token_index ON password_resets USING btree (token);


--
-- Name: app_module app_module_application_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_module
    ADD CONSTRAINT app_module_application_foreign FOREIGN KEY (application) REFERENCES app_application(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: crm_contact crm_contact_company_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crm_contact
    ADD CONSTRAINT crm_contact_company_foreign FOREIGN KEY (company) REFERENCES setting_company(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: crm_contact_detail crm_contact_detail_contact_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crm_contact_detail
    ADD CONSTRAINT crm_contact_detail_contact_foreign FOREIGN KEY (contact) REFERENCES crm_contact(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock_movement_comment inventory_stock_movement_comment_stock_movement_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_comment
    ADD CONSTRAINT inventory_stock_movement_comment_stock_movement_foreign FOREIGN KEY (stock_movement) REFERENCES inventory_stock_movement(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock_movement inventory_stock_movement_company_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement
    ADD CONSTRAINT inventory_stock_movement_company_foreign FOREIGN KEY (company) REFERENCES setting_company(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock_movement_log inventory_stock_movement_log_stock_movement_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_log
    ADD CONSTRAINT inventory_stock_movement_log_stock_movement_foreign FOREIGN KEY (stock_movement) REFERENCES inventory_stock_movement(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock_movement_product inventory_stock_movement_product_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_product
    ADD CONSTRAINT inventory_stock_movement_product_product_foreign FOREIGN KEY (product) REFERENCES product_product(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: inventory_stock_movement_product inventory_stock_movement_product_stock_movement_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_product
    ADD CONSTRAINT inventory_stock_movement_product_stock_movement_foreign FOREIGN KEY (stock_movement) REFERENCES inventory_stock_movement(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock_movement_product inventory_stock_movement_product_uom_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock_movement_product
    ADD CONSTRAINT inventory_stock_movement_product_uom_foreign FOREIGN KEY (uom) REFERENCES product_uom(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: inventory_stock inventory_stock_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock
    ADD CONSTRAINT inventory_stock_product_foreign FOREIGN KEY (product) REFERENCES product_product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_stock inventory_stock_uom_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock
    ADD CONSTRAINT inventory_stock_uom_foreign FOREIGN KEY (uom) REFERENCES product_uom(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: inventory_stock inventory_stock_warehouse_location_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_stock
    ADD CONSTRAINT inventory_stock_warehouse_location_foreign FOREIGN KEY (warehouse_location) REFERENCES inventory_warehouse_location(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inventory_warehouse_location inventory_warehouse_location_warehouse_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inventory_warehouse_location
    ADD CONSTRAINT inventory_warehouse_location_warehouse_foreign FOREIGN KEY (warehouse) REFERENCES inventory_warehouse(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_alternative product_alternative_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_alternative
    ADD CONSTRAINT product_alternative_product_foreign FOREIGN KEY (product) REFERENCES product_product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_alternative product_alternative_product_master_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_alternative
    ADD CONSTRAINT product_alternative_product_master_foreign FOREIGN KEY (product_master) REFERENCES product_master(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_attribute_value product_attribute_value_attribute_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_attribute_value
    ADD CONSTRAINT product_attribute_value_attribute_foreign FOREIGN KEY (attribute) REFERENCES product_attribute(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_bom product_bom_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_bom
    ADD CONSTRAINT product_bom_product_foreign FOREIGN KEY (product) REFERENCES product_product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_bom product_bom_product_master_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_bom
    ADD CONSTRAINT product_bom_product_master_foreign FOREIGN KEY (product_master) REFERENCES product_master(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_master product_master_category_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_master
    ADD CONSTRAINT product_master_category_foreign FOREIGN KEY (category) REFERENCES product_category(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product_master product_master_uom_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_master
    ADD CONSTRAINT product_master_uom_foreign FOREIGN KEY (uom) REFERENCES product_uom(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: product_product product_product_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_product
    ADD CONSTRAINT product_product_product_foreign FOREIGN KEY (product) REFERENCES product_master(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_supplier product_supplier_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_supplier
    ADD CONSTRAINT product_supplier_product_foreign FOREIGN KEY (product) REFERENCES product_master(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_supplier product_supplier_supplier_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_supplier
    ADD CONSTRAINT product_supplier_supplier_foreign FOREIGN KEY (supplier) REFERENCES crm_contact(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_uom product_uom_uom_category_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_uom
    ADD CONSTRAINT product_uom_uom_category_foreign FOREIGN KEY (uom_category) REFERENCES product_uom_category(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_variant product_variant_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_variant
    ADD CONSTRAINT product_variant_product_foreign FOREIGN KEY (product) REFERENCES product_master(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

