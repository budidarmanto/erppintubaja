<?php

namespace App\Http\Controllers\Sales;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\ProductMaster;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class SalesOrder extends Controller{

  public $company;

  public function index() {

    $dateFrom =  Input::get('from');
    $dateTo   = Input::get('to');
    $sales   = Input::get('sales');
        
    if(empty($dateTo)){
        $dateTo = $dateFrom;
    }
    $dateTo = Carbon::parse($dateTo)->addDay(1)->toDateTimeString();
    
    $query = InventoryStockMovement::select([
      'setting_user.fullname',
      'crm_contact.name AS supplier',
      'inventory_stock_movement.*',
      'ism.code AS reference_code'
    ])->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'SO',
    ])
    ->leftJoin('setting_user', 'inventory_stock_movement.created_by', '=', 'setting_user.username')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->leftJoin('inventory_stock_movement as ism', 'ism.id', '=', 'inventory_stock_movement.reference')
    ->orderBy('inventory_stock_movement.code', 'DESC');
    
    if(!empty($dateFrom)) {
        $query = $query->whereBetween('inventory_stock_movement.date', [$dateFrom, $dateTo]);
    }
    
    if(!empty($sales)) {
        $query = $query->where('inventory_stock_movement.created_by', $sales);
    }

    $dg = Datagrid::source($query);
    $dg->title('Penjualan');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Penjualan', true);
    $dg->add('date', 'Tanggal', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('supplier', 'Pelangan', false);
    $dg->add('description', 'Keterangan');
    //$dg->add('total','Total');
    ///$dg->add('fullname','Dibuat Oleh');
	$dg->add('fullname','Salesman');
    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = SettingWorkflow::getOption('SO');
    return response()->json($datagrid);
  }

  public function create(){
    
    $form = $this->anyForm(new InventoryStockMovement());
    $defaultWorkflow = SettingWorkflow::workflowDefault('SO');
      $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('SO');
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'SO';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });

    $dataForm = $form->build();

    if($form->hasRequest()){

      $this->processDataAfterSave($form);
      InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);

      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['data']['code'] = InventoryStockMovement::generateId('SO');
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');
    $dataForm['optionDoorDirection']    = Params::getOption('OPTION_PRODUCT_DIRECTION');
	$dataForm['optionDoorInstallation'] = Params::getOption('OPTION_PRODUCT_INSTALLATION');
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $this->getDataProduct($id);
    $stockMovement = InventoryStockMovement::find($id);
    $form = $this->anyForm($stockMovement);
    $form->pre(function($data){
      unset($data['date']);
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataProduct'] = $this->getDataProduct($id);
    $dataForm['dataComment'] = $this->getDataComment($id);
    $reference = $dataForm['data']['reference'];
    if(!empty($reference)){
      $stockMoveReference = InventoryStockMovement::find($reference);
      if(!empty($stockMoveReference)){
        $dataForm['data']['reference'] = $stockMoveReference->code;
      }
    }									                    
    $dataForm['optionDoorDirection']    = Params::getOption('OPTION_PRODUCT_DIRECTION');
	$dataForm['optionDoorInstallation'] = Params::getOption('OPTION_PRODUCT_INSTALLATION');
    return response()->json($dataForm);
  }

  public function delete(){
    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status == $status){
        return false;
      }
      $stockMovement->status = $status;
      $stockMovement->save();
      InventoryStockMovementLog::addLog($id, $status);

      return response()->json([
        'status' => true,
        'dataLog' => $this->getDataComment($id)
      ]);
    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Penjualan');
    $form->add('code', 'No. Penjualan', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('reference', 'No. Pengajuan', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('diajukan', 'Diajukan Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('contact', 'Pelangan', 'select-ajax')->attributes([
      'readonly' => true,
      'data-live-search'=> true,
      'data-source' => '/options/customer'
    ])->rule('required');
    // $form->add('source', 'Asal', 'select')->attributes([
      // 'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    // ]);//->rule('required');
    $form->add('shipment_address', 'Alamat Pengiriman', 'textarea');
    $form->add('expected_date', 'Estimasi Pengiriman', 'date')->rule('required');
	$form->add('created_by', 'Salesman', 'text')->attributes([
      'readonly' => true
    ]);

    $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
    if($isCurrency){
      $form->add('currency', 'Mata Uang', 'select')->attributes([
        'ng-options' => 'item.id as item.text for item in response.optionCurrency'
      ])->rule('required');
      $form->add('currency_rate', 'Kurs', 'number');
    }
    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouse::getOption();
    $data['optionTax'] = SettingTax::getOption();
    //$data['listBarang'] = ProductMaster::getOption();
    //$data['listUom'] = ProductUom::getOptionSingle();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = SettingWorkflow::getOption('SO');
    return $data;
  }

  public function getDataProduct($id){
    return InventoryStockMovementProduct::getDataProduct($id);
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }
        $val['discount'] = (isset($val['discount'])) ? json_encode($val['discount']) : null;
        $expectedDateUnformatted = $val['expected_date'];
        $expectedDate = \DateTime::createFromFormat('d/m/Y', $expectedDateUnformatted);
        if($expectedDate !== false) {
            dd($val['expected_date']);
            $val['expected_date'] = $expectedDate->format('Y-m-d');
        }
        
        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();

    InventoryStockMovement::refreshTotal($form->model->id);
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'SO', 'Sales Order');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);

    return $data->all();
  }

}
