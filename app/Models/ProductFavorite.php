<?php


namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductFavorite extends DevplusModel
{
  protected $table = 'product_favorite';
  protected $fillable = ['id', 'company', 'product', 'date', 'sold', 'active'];
}
