<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\CrmContact;
use App\Models\SettingCompany;
use App\Models\SettingUser;
//use Request;
use Illuminate\Http\Request;

class Customer extends Controller {
    
    public function index(){
        $input = Input::all();
        $query = CrmContact::where('active', true)->where(function($q) use($input) {
            $q->where('created_by', $input['user'])->orWhere('updated_by',$input['user']);
        })->getQuery();
        $query = $query->orderBy('name', 'asc')->get();//->toArray();
        $dataCus = [];
        if(count($query) > 0){
            foreach($query as $valCus){
                $dataCus[] = [
                    'id'=> $valCus->id,
                    'company'=> $valCus->company,
                    'name'=> $valCus->name,
                    'picture'=> (empty($valCus->picture)) ? '' : url('/') .'/img/contact/'.$valCus->picture,
                    'address'=> $valCus->address,
                    'city'=> $valCus->city,
                    'postal_code'=> $valCus->postal_code,
                    'state'=> $valCus->state,
                    'country'=> $valCus->country,
                    'website'=> $valCus->website,
                    'phone'=> $valCus->phone,
                    'mobile'=> $valCus->mobile,
                    'fax'=> $valCus->fax,
                    'email'=> $valCus->email,
                    'tax_no'=> $valCus->tax_no,
                    'type'=> $valCus->type,
                    'active'=> $valCus->active,
                    'pkp'=> $valCus->pkp,
                    'term_of_payment'=> $valCus->term_of_payment,
                ];
            }
        }
        $data1['data'] = $dataCus; 
        return response()->json($data1);
    }
    
    public function detail($id = null) {
        if($id === null || empty($id)) {
            return response()->json(['data' => []]);
        }
        
        $dataCus = CrmContact::where('id', $id)->where('active', true)->first();
        $data = [
            'id'=> $dataCus->id,
            'company'=> $dataCus->company,
            'name'=> $dataCus->name,
            'picture'=> (empty($dataCus->picture)) ? '' : Request::root().'/img/contact/'.$dataCus->picture,
            'address'=> $dataCus->address,
            'city'=> $dataCus->city,
            'postal_code'=> $dataCus->postal_code,
            'state'=> $dataCus->state,
            'country'=> $dataCus->country,
            'website'=> $dataCus->website,
            'phone'=> $dataCus->phone,
            'mobile'=> $dataCus->mobile,
            'fax'=> $dataCus->fax,
            'email'=> $dataCus->email,
            'tax_no'=> $dataCus->tax_no,
            'type'=> $dataCus->type,
            'active'=> $dataCus->active,
            'pkp'=> $dataCus->pkp,
            'term_of_payment'=> $dataCus->term_of_payment,
        ];
        return response()->json($data);
    }
    
    public function create(Request $request) {	
        $name 			= $request->input('name');
        $picture 		= $request->file('picture');
        if($picture) {
            $picture_img = getimagesize($picture);
            if(floatval($picture->getClientSize()) > 2 * 1024 * 1024) {
                return response()->json([
                    'status' => false,
                    'message' => 'Ukuran maksimal gambar adalah 2MB.',
                ]);
            }
            if($picture_img[0] > 800 || $picture_img[1] > 800) {
                return response()->json([
                    'status' => false,
                    'message' => 'Panjang dan lebar maksimal gambar 800px.',
                ]);
            }
        }
        
        $address	   	= $request->input('address');
        $city			= $request->input('city');
        $postal_code    = $request->input('postal_code');
        $state			= $request->input('state');
        $country		= $request->input('country');
        $website		= $request->input('website');
        $phone			= $request->input('phone');
        $mobile			= $request->input('mobile');
        $fax			= $request->input('fax');
        $email			= $request->input('email');
        $tax_no			= $request->input('tax_no');
        $customer		= 'customer';
        $salesman 		= $request->input('salesman');

        $addcustomer = new CrmContact;
        $addcustomer->name    	= $name;
        $addcustomer->picture	= empty($picture) ? '' : $picture->getClientOriginalName();
        $addcustomer->address   = $address;
        $addcustomer->city		= $city;
        $addcustomer->postal_code= $postal_code;
        $addcustomer->state    	= $state;
        $addcustomer->country 	= $country;
        $addcustomer->website   = $website;
        $addcustomer->phone    	= $phone;
        $addcustomer->mobile 	= $mobile;
        $addcustomer->fax    	= $fax;
        $addcustomer->email    	= $email;
        $addcustomer->tax_no 	= $tax_no;
        $addcustomer->type 		= $customer;
        $addcustomer->created_by= $salesman;
		// try {
			// $success = $addcustomer->save();		
			// return response()->json([
				// 'status' => $success
			// ]);
		// }
		// catch(\Exception $e) {
			// return response()->json([
				// 'status' => false,
			// ]);
		// }
        $saved = $addcustomer->save();
        if($saved) {
            if(!empty($picture)) {
                $filename = $picture->getClientOriginalName();
                $uploaded = $picture->storeAs('img/contact', $filename, 'public_uploads');  
                if(!$uploaded) {
                    $addcustomer->delete();
                    return response()->json([
                        'status' => false,
                        'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                    ]);
                }
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);
    }
	
    public function update($id, Request $request) {
        $name 			= $request->input('name');
        $picture 		= $request->file('picture');
        if($picture) {
            $picture_img = getimagesize($picture);
            if(floatval($picture->getClientSize()) > 2 * 1024 * 1024) {
                return response()->json([
                    'status' => false,
                    'message' => 'Ukuran maksimal gambar adalah 2MB.',
                ]);
            }
            if($picture_img[0] > 800 || $picture_img[1] > 800) {
                return response()->json([
                    'status' => false,
                    'message' => 'Panjang dan lebar maksimal gambar 800px.',
                ]);
            }
        }
        $address	   	= $request->input('address');
        $city			= $request->input('city');
        $postal_code    = $request->input('postal_code');
        $state			= $request->input('state');
        $country		= $request->input('country');
        $website		= $request->input('website');
        $phone			= $request->input('phone');
        $mobile			= $request->input('mobile');
        $fax			= $request->input('fax');
        $email			= $request->input('email');
        $tax_no			= $request->input('tax_no');
		$salesman 		= $request->input('salesman');
	
        $updateCustomer = CrmContact::where('id', $id)
            ->update([
                'name'=>$name,
                'picture' => empty($picture) || $picture == false ? '' : $picture->getClientOriginalName(),
                'address'=> $address,
                'city'=> $city,
                'postal_code' => $postal_code,
                'state' => $state,
                'country' => $country,
                'website' => $website,
                'phone' => $phone,
                'mobile' => $mobile,
                'fax' => $fax,
                'email' => $email,
                'tax_no' => $tax_no,
                'updated_by' => $salesman
            ]);

        if($updateCustomer) {
            if(!empty($picture)) {
                $filename = $picture->getClientOriginalName();
                $uploaded = $picture->storeAs('img/contact', $filename, 'public_uploads');
                
                if(!$uploaded) {
                    return response()->json([
                        'status' => false,
                        'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
                    ]);
                }
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan pada server saat mengupload gambar.',
            ]);
        }

        return response()->json([
            'status' => true,
        ]);
    }
	
    public function optcustomer(){
        $response = [];
        $username = Input::get('username');
        $query = CrmContact::where('active', true)->getQuery();
        if(!empty($username)) {
            $query = $query->whereRaw('id in (select contact from inventory_stock_movement where created_by = ? or updated_by = ?) or created_by = ? or updated_by = ?', [$username, $username, $username, $username]);
        }
        $query = $query->orderBy('name', 'asc')->get();
        if(count($query) > 0){
            foreach($query as $val){
                $response[] = [
                    'key'	=> $val->id . '-'. $val->name,
                    'label'	=> $val->name
                ];
            }
        }	
        return response()->json($response);
    }
	
    public function getCustomer($id) {
        if($id === null || empty($id)) {
            return response()->json(['data' => []]);
        }
        $data = CrmContact::where('id', $id)->where('active', true)->first();									   
        $dataResponse = [
            'name'=>$data->name,
            'picture' => (empty($data->picture)) ? '' : Request::root().'/img/contact/'.$data->picture,
            'address'=> $data->address,
            'city'=> $data->city,
            'postal_code' => $data->postal_code,
            'state' => $data->state,
            'country' => $data->country,
            'website' => $data->website,
            'phone' => $data->phone,
            'mobile' => $data->mobile,
            'fax' => $data->fax,
            'email' => $data->email,
            'tax_no' => $data->tax_no
        ];
        return response()->json($dataResponse);
    }
}
