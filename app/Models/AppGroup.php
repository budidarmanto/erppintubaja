<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class AppGroup extends DevplusModel
{
  protected $table = 'app_group';
  protected $fillable = ['id', 'name', 'access', 'category'];

  public static function getOption(){
    $data = AppGroup::all();
    return Helper::toOption('id','name', $data);
  }
}
