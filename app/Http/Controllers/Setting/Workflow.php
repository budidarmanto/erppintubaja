<?php

namespace App\Http\Controllers\Setting;

use App\Models\SettingWorkflow;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Workflow extends Controller
{

  public function index() {

    $dg = Datagrid::source(SettingWorkflow::query());
    $dg->title(__('app.setting.workflow.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', __('app.setting.workflow.code'));
    $dg->add('name', __('app.setting.workflow.name'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new SettingWorkflow());
    $form->pre(function($input){
      $input['workflow'] = json_encode($input['workflow']);
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['dataWorkflow'] = [];
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(SettingWorkflow::find($id));
    $form->pre(function($input){
      $input['workflow'] = json_encode($input['workflow']);
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['dataWorkflow'] = json_decode($dataForm['data']['workflow']);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    SettingWorkflow::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.setting.workflow.title'));
    $form->add('code', __('app.setting.workflow.code'), 'text')->rule('required|max:25');
    $form->add('name', __('app.setting.workflow.name'), 'text')->rule('required|max:60');
    return $form;
  }

}
