<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="formReportHourly">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-8 col-md-offset-2">
              <div ng-repeat="field in response.fields">
                <form-builder field="@{{ field.name }}"></form-builder>
              </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
              <button type="submit" ng-click="download($event)" class="btn btn-sm default"><i class="fa fa-download"></i> Export</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
