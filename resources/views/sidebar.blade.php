<div class="page-sidebar navbar-collapse collapse">
    <div class="profile-sidebar">
      <div class="profile-userpic">
        @if(Auth::user()->authCompany()->picture == '')
          <img src="img/company-placeholder.png" class="img-responsive" alt="">
        @else
          <img src="img/company/{{ Auth::user()->authCompany()->picture }}" class="img-responsive" alt="">
        @endif
      </div>
      <div class="profile-sidebar-title">{{ Auth::user()->authCompany()->name }}</div>
    </div>
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <!-- <form class="sidebar-search sidebar-search-bordered" action="#" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form> -->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        @if(Auth::user()->hasApplication('dashboard'))
          @if(Auth::user()->hasModule('DashboardGeneral'))
          <li class="nav-item">
              <a href="#/dashboard/general">
                  <i class="icon-bar-chart"></i>
                  <span class="title">Dashboard</span>
              </a>
          </li>
          @endif
        @endif
        <!-- App -->
        @if(Auth::user()->hasApplication('app'))
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="icon-settings"></i>
                <span class="title">@lang('system.sidebar.app.name')</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('AppGroup'))
                <li>
                    <a href="#/app/group">
                        <span>@lang('system.sidebar.app.group')</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('AppApplication'))
                <li>
                    <a href="#/app/application">
                        <span>@lang('system.sidebar.app.application')</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('AppRole'))
                <li>
                    <a href="#/app/role">
                        <span>@lang('system.sidebar.app.role')</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('AppConfig'))
                <li>
                    <a href="#/app/config">
                        <span>@lang('system.sidebar.app.config')</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        <!-- End App -->

        <!-- Setting -->
        @if(Auth::user()->hasApplication('setting'))
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="icon-wrench"></i>
                <span class="title">@lang('system.sidebar.setting.name')</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('SettingUser'))
                <li>
                    <a href="#/setting/user">
                        <span>@lang('system.sidebar.setting.user')</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('SettingCompany'))
                <li>
                    <a href="#/setting/company">
                        <span>@lang('system.sidebar.setting.company')</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('SettingWorkflow'))
                <li>
                    <a href="#/setting/workflow">
                        <span>@lang('system.sidebar.setting.workflow')</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        <!-- End Setting -->

        <!-- Product -->
        @if(Auth::user()->hasApplication('product'))
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-dropbox"></i>
                <span class="title">Barang/Jasa</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('ProductUom'))
                <li>
                    <a href="#/product/uom">
                        <span>Satuan Ukuran</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('ProductAttribute') && Params::get('PRODUCT_VARIANT') == 1)
                <li>
                    <a href="#/product/attribute">
                        <span>Varian</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('ProductCategory'))
                <li>
                    <a href="#/product/category">
                        <span>Kategori Produk</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('ProductMaster'))
                <li>
                    <a href="#/product/master">
                        <span>Barang/Jasa</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        <!-- End Product -->

        <!-- Inventory -->
        @if(Auth::user()->hasApplication('inventory'))
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-cube"></i>
                <span class="title">Inventori</span>
                <span class="badge badge-danger" ng-hide="sidebarAlert.totalAllInventory <= 0">@{{ sidebarAlert.totalAllInventory }}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('InventoryWarehouse'))
                <li>
                    <a href="#/inventory/warehouse">
                        <span>Gudang</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('InventoryStock'))
                <li>
                    <a href="#/inventory/stock">
                        <span>Persediaan</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        <!-- End Inventory -->
        
        <!-- Sales -->
        @if(Auth::user()->hasApplication('sales'))
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-truck"></i>
                <span class="title">Penjualan</span>
                <span class="badge badge-danger" ng-hide="sidebarAlert.totalAllSales <= 0">@{{ sidebarAlert.totalAllSales }}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('SalesCustomer'))
                <li>
                    <a href="#/sales/customer">
                        <span>Pelanggan</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('SalesSalesorder'))
                <li>
                    <a href="#/sales/sales-order">
                        <span>Penjualan</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalSales <= 0">@{{ sidebarAlert.totalSales }}</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        <!-- End Sales -->
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
