<div class="portlet-title">
    <div class="caption font-green-sharp">
        <!-- <i class="icon-settings font-green-sharp"></i> -->
        <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
        <span class="caption-helper hide"></span>
    </div>

    <div class="actions">
      <a href="#/@{{ response.create }}" class="btn btn-sm btn-primary" ng-if="response.role.indexOf('create') != -1"> @lang('system.datagrid.header.create') <i class="fa fa-plus"></i></a>
    </div>
</div>
