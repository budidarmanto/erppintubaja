/* Setup blank page controller */
angular.module('DevplusApp').controller('DashboardGeneralController', ['$rootScope', '$scope', '$http', '$q', '$timeout','settings', 'response', function($rootScope, $scope, $http, $q, $timeout, settings, response) {

    
    $scope.data = response.data;
    $scope.selectedCompany = [];
    $scope.selectedStatus = [];
	
    $scope.dateFrom = null;
    $scope.dateTo = null;
    $scope.chartDate = null;
    $scope.selectAll = false;
    $scope.selectAllStatus = false;

    $scope.export_format = 'excel';

    $scope.totalSales = 0;
    $scope.totalTransaction = 0;
    $scope.averageSales = 0;
    $scope.targetSales = 0;
    $scope.targetAverageSales = 0;
    $scope.targetTransaction = 0;

    $scope.checkSangkuriang = true;
    $scope.checkNonSangkuriang = true;

    $scope.params = {
      company: '',
      status: '',
      category: '',
      from: '',
      to: ''
    };
    $scope.queryString;
    $scope.optCat = [];

    $scope.filterCategory = [];
    // $scope.filterreference = ['sangkuriang', 'nonsangkuriang'];
    // $scope.filterproduct = ['sangkuriang', 'nonsangkuriang'];

    $scope.dataChart = [];
    $scope.dataOrderReference = [];
    $scope.dataProductTopSales = [];
    $scope.dataSalesPercentage = [];
    $scope.dataSalesByStore = [];
	$scope.dataChasierTopSales = [];

    $scope.serialChart = null;

    $scope.initDateRangePicker = function () {
        if (!jQuery().daterangepicker) {
            return;
        }
        var from = moment();
        var to = moment();
        $scope.dateFrom = from.format('YYYY-MM-DD');
        $scope.dateTo = to.format('YYYY-MM-DD');
        $scope.chartDate = from.format('YYYY-MM-DD');

        $('#dashboard-report-range').daterangepicker({
            "ranges": {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "startDate": from.format('MM/DD/YYYY'),
            "endDate": to.format('MM/DD/YYYY'),
            "opens": ('right'),
        }, function(start, end, label) {
            if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                $('#dashboard-report-range span').html(start.format('MMM D') + ' - ' + end.format('MMM D'));
                // $('#chart-date span').html(start.format('MMMM D, YYYY'));
            }
        });
        if ($('#dashboard-report-range').attr('data-display-range') != '0') {
            $('#dashboard-report-range span').html(from.format('MMM D') + ' - ' + to.format('MMM D'));
            // $('#chart-date span').html(from.format('MMMM D, YYYY'));
        }

        $('#dashboard-report-range').show();

        $('#dashboard-report-range').on('apply.daterangepicker', function(ev, picker) {
            $scope.dateFrom = picker.startDate.format('YYYY-MM-DD');
            $scope.dateTo = picker.endDate.format('YYYY-MM-DD');
            $scope.chartDate = picker.startDate.format('YYYY-MM-DD');
            // console.log(moment($scope.dateFrom));
            // $('#chart-date').datepicker('option', 'minDate', new Date());
            // $('#chart-date').datepicker('option', 'maxDate', new Date());
            $scope.getData();
        });

        // $('#chart-date').datepicker({
        //   minDate: new Date(),
        //   maxDate: new Date()
        // }).on('changeDate', function(ev) {
        //     $(this).datepicker('hide');
        //     $(this).find('span').html(ev.format(0,"MM dd, yyyy"));
        //     $scope.chartDate = ev.format('yyyy-mm-dd');
        //     $scope.getData();
        // });
    };

    $scope.resizeWidgetValueFont = function () {
        $('.widget-thumb .widget-thumb-body').each(function () {
            var width = $(this).width();
            var statWidth = $(this).find('.widget-thumb-body-stat')[0].scrollWidth;
            while(statWidth > width) {
                var bodyStat =  $(this).find('.widget-thumb-body-stat');
                bodyStat.css({
                    'font-size': (parseInt(bodyStat.css('font-size')) - 2) + 'px !important'
                });
                statWidth = $(this).find('.widget-thumb-body-stat')[0].scrollWidth;
            }
        });
    };

    $scope.getSalesChartOptions = function(){
      var graphs = [];

      var getOptGraphs = function(kprice, kqty, title){
        return {
            "balloonText": "Sale: [["+kprice+"]] \n Quantity:[["+kqty+"]]",
            "balloonFunction": function(item, graph) {
              var result = graph.balloonText;
              for (var key in item.dataContext) {
                if (item.dataContext.hasOwnProperty(key) && !isNaN(item.dataContext[key])) {
                  var formated = item.dataContext[key].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  result = result.replace("[[" + key + "]]", formated);
                }
              }
              return result;
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "useLineColorForBulletBorder": true,
            "bulletColor": "#FFFFFF",
            "bulletSizeField": "[["+kprice+"]]",
            // "legendValueText": "Sale: [[sangkuriangAverageSalesPrice]]/[[sangkuriangAverageSalesQty]]",
            "title": title,
            "fillAlphas": 0,
            "valueField": kprice,
            "valueAxis": "salesPriceAxis"
        };
      }

      angular.forEach($scope.optionCategory, function(v, k){
        var optGraphs = getOptGraphs(k+'_price', k+'_qty', v);
        graphs.push(optGraphs);
      });

      var optGraphs = getOptGraphs('average_price', 'average_qty', 'Total');
      graphs.push(optGraphs);
      return {
        "type": "serial",
        "theme": "light",

        //"fontFamily": 'Open Sans',
        "color":    '#888888',
        "legend": {
            "equalWidths": true,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 40
        },
        "data": [],
        "valueAxes": [{
            "id": "salesPriceAxis",
            "axisAlpha": 0.3,
            "gridAlpha": 0,
            "axisColor": "#555555",
            "position": "left",
            "title": "Sales (IDR)",
            "autoGridCount": true,
        }],
        "graphs": graphs,
        "chartCursor": {
            "categoryBalloonDateFormat": "HH",
            "cursorAlpha": 0.1,
            "cursorColor": "#000000",
            "fullWidth": true,
            "valueBalloonsEnabled": false,
            "zoomable": false
        },
        "dataDateFormat": "JJ:NN",
        "categoryField": "hour",
        "categoryAxis": {
            "dateFormats": [{
                "period": "hh",
                "format": "JJ"
            }, {
                "period": "mm",
                "format": "JJ"
            }],
            "parseDates": false,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "gridCount": 50
        },
        "colors": ["#FCD202", "#6d2eea", "#B0DE09"]
      };
    }

// console.log($scope.optionCategory);
    $scope.salesChartOptions = $scope.getSalesChartOptions();

    $scope.salesReferenceChartOptions = {
        "type": "pie",
        "theme": "light",
        "autoMargins": false,
        "marginTop": 10,
        "marginBottom": 0,
        "marginLeft": 0,
        "marginRight": 0,
        "pullOutRadius": 0,

        "fontFamily": 'Open Sans',

        "color": "#fff",

        "data": [{
          "reference": "Direct",
          "transactions": 0
        }],
        "valueField": "transactions",
        "titleField": "reference",
        "labelRadius": -45,
        "labelText": "[[title]]\n[[percents]]%",
        "balloonText": "[[title]] ([[percents]]%)\n [[value]] penjualan",
        "colors": ["#177BBE", "#8EC449", "#169C41"]
    };

    $scope.salesPercentageChartOptions = {
        "type": "pie",
        "theme": "light",
        "autoMargins": false,
        "marginTop": 10,
        "marginBottom": 0,
        "marginLeft": 0,
        "marginRight": 0,
        "pullOutRadius": 0,

        "fontFamily": 'Open Sans',

        "color": "#fff",

        "data": [{
          "category": "Direct",
          "transactions": 0
        }],
        "valueField": "transactions",
        "titleField": "category",
        "labelRadius": -45,
        "labelText": "[[title]]\n[[percents]]%",
        "balloonText": "[[title]] ([[percents]]%)\n [[value]] penjualan",
        "colors": ["#6d2eea", "#FCD202"]
    };

    $scope.salesByStoreChartOptions = {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,
        "fontFamily": 'Open Sans',
        "color": '#888',
        "data": [],
        "valueAxes": [{
            "position": "left",
            "axisAlpha": 0,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "colorField": "color",
            "fillAlphas": 0.85,
            "lineAlpha": 0.1,
            "type": "column",
            "topRadius": 1,
            "valueField": "sales"
        }],
        "depth3D": 40,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "company",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
        },
    };

    // console.log($scope.salesByStoreChartOptions);

    var t;
    $scope.getData = function(onSuccess){
        clearTimeout(t);
        t = setTimeout(function(){
            App.startPageLoading({animate: true});

            // var productSangkuriang = ($scope.filterproduct.indexOf('sangkuriang') > -1) ? 1 : 0;
            // var productNonSangkuriang = ($scope.filterproduct.indexOf('nonsangkuriang') > -1) ? 1 : 0;
            // var referenceSangkuriang = ($scope.filterreference.indexOf('sangkuriang') > -1) ? 1 : 0;
            // var referenceNonSangkuriang = ($scope.filterreference.indexOf('nonsangkuriang') > -1) ? 1 : 0;
            // var percentageSangkuriang = ($scope.filterreference.indexOf('sangkuriang') > -1) ? 1 : 0;
            // var percentageNonSangkuriang = ($scope.filterreference.indexOf('nonsangkuriang') > -1) ? 1 : 0;



            var data = {
                company: $scope.selectedCompany,
                status: $scope.selectedStatus,
                from: $scope.dateFrom,
                to: $scope.dateTo,
                // chartDate: $scope.chartDate
                // productSangkuriang: productSangkuriang,
                // productNonSangkuriang: productNonSangkuriang,
                // referenceSangkuriang: referenceSangkuriang,
                // referenceNonSangkuriang: referenceNonSangkuriang,
                // percentageSangkuriang: percentageSangkuriang,
                // percentageNonSangkuriang: percentageNonSangkuriang,
            };

            var queryString = [];
            angular.forEach(data, function(val, key){
              if(val !== ''){
                if(key != 'company' && key != 'status' && key != 'category' && key != 'shift'){
                  queryString.push(key+'='+val);
                }else{
                  queryString.push(key+'='+val.join('|'));
                }
              }
            });

            $scope.queryString = '?'+queryString.join('&');

            //console.log($scope.queryString);

            $.ajax({
                method: 'post',
                url: 'dashboard/general/getdata',
                data: data,
                success: function(resp){
					//console.log(resp.dataChasierTopSales);
                    App.stopPageLoading({animate: true});
                    $scope.totalSales = resp.totalSales;
                    $scope.totalTransaction = resp.totalTransaction;
                    $scope.averageSales = resp.averageSales;
                    $scope.targetSales = resp.targetSales;
                    $scope.targetAverageSales = resp.targetAverageSales;
                    $scope.targetTransaction = resp.targetTransaction;
                    $scope.dataChart = resp.dataChart;
                    $scope.dataOrderReference = resp.dataOrderReference;
                    $scope.dataProductTopSales = resp.dataProductTopSales;
                    $scope.dataSalesPercentage = resp.dataSalesPercentage;
                    $scope.dataSalesByStore = resp.dataSalesByStore;
					
					$scope.dataChasierTopSales = resp.dataChasierTopSales;
					
                    $scope.$apply();
                    $scope.$broadcast('amCharts.updateData', resp.dataChart, 'sales_chart');
                    $scope.$broadcast('amCharts.updateData', resp.dataOrderReference, 'sales_reference_chart');
                    $scope.$broadcast('amCharts.updateData', resp.dataSalesPercentage, 'sales_percentage_chart');
                    $scope.$broadcast('amCharts.updateData', resp.dataSalesByStore, 'sales_by_store_chart');

                    if(typeof onSuccess != 'undefined'){
                        onSuccess();
                    }
                    setTimeout(function(){
                        $('a[title="JavaScript charts"]').remove();
                    }, 1);
                }
            });
        }, 100);
    }

    $scope.changeCheck = function(){
        $scope.getData();
    }

    $scope.companyChanged = function(companyId){
        var idx = $scope.selectedCompany.indexOf(companyId);

        if (idx > -1) {
            $scope.selectedCompany.splice(idx, 1);
        }
        else {
            $scope.selectedCompany.push(companyId);
        }
        $scope.getData();
    }
    
    $scope.statusChanged = function(statusCode){
        var idx = $scope.selectedStatus.indexOf(statusCode);

        if (idx > -1) {
            $scope.selectedStatus.splice(idx, 1);
        }
        else {
            $scope.selectedStatus.push(statusCode);
        }
        $scope.getData();
    }

    $scope.categoryChanged = function(categoryId){
        var idx = $scope.selectedCategory.indexOf(categoryId);

        if (idx > -1) {
            $scope.selectedCategory.splice(idx, 1);
        }
        else {
            $scope.selectedCategory.push(categoryId);
        }
setTimeout(function(){
  if($scope.selectedCategory.length <= 0){
    angular.forEach($scope.optionCategory, function(v, k){
      $scope.selectedCategory.push(k);
    });
  };
  $scope.getData();
}, 500)


    }
    
    $scope.shiftChanged = function(shift){
        var idx = $scope.selectedShift.indexOf(shift);

        if (idx > -1) {
            $scope.selectedShift.splice(idx, 1);
        }
        else {
            $scope.selectedShift.push(shift);
        }
        $scope.getData();
    }

    $scope.checkAll = function(){
      $scope.selectedCompany = [];

      if($scope.selectAll){
        angular.forEach($scope.data.listCompany, function(value, key){
          $scope.selectedCompany.push(value.id);
        });
      }

      $scope.getData();
    }
    
    $scope.checkAllStatus = function(){
      $scope.selectedStatus = [];

      if($scope.selectAllStatus){
        angular.forEach($scope.data.statuses, function(value, key){
          $scope.selectedStatus.push(value.code);
        });
      }

      $scope.getData();
    }

    // $scope.changeSelect = function(){
    //   $scope.getData();
    // }

    $scope.init = function(){
        $scope.selectedCompany.push($scope.data.currentCompany);
        $scope.getData();
        $('.selectpicker').selectpicker('refresh');
        setTimeout(function(){
            $('a[title="JavaScript charts"]').remove();
        }, 500);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
        $scope.init();
        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;

        $scope.initDateRangePicker();
    });
}]);
