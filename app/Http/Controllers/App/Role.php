<?php

namespace App\Http\Controllers\App;

use App\Models\AppRole;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Role extends Controller
{

  public function index() {

    $dg = Datagrid::source(AppRole::query());
    $dg->title(__('app.app.role.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', __('app.app.role.code'));
    $dg->add('name', __('app.app.role.name'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new AppRole());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(AppRole::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    AppRole::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.app.role.title'));
    $form->add('code', __('app.app.role.code'), 'text')->rule('required|max:60');
    $form->add('name', __('app.app.role.name'), 'text')->rule('required|max:60');
    return $form;
  }

}
