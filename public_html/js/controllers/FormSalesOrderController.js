/* Setup blank page controller */
angular.module('DevplusApp').controller('FormSalesOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {
  $scope.response = response;
  $scope.data = response.data.length == 0 ? {} : response.data;
  
  if(response.action == ''){
    window.location.hash = 'no-access';
  };
  $scope.dataProduct = response.dataProduct;
  $scope.dataComment = response.dataComment;
  $scope.optionTax = response.optionTax;
  $scope.optionWorkflow = response.optionWorkflow;
  $scope.propertyName = '';
  $scope.reverse = true;
  $scope.sortBy = function(propertyName) {
    angular.forEach($scope.dataProduct, function(v){
      v.qty = parseFloat(v.qty);
      v.price = parseFloat(v.price);
    });
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
  };

  $scope.getSortClass = function(propertyName){
    var cls = "sorting";
    if(propertyName == this.propertyName){
      if($scope.reverse){
        cls = "sorting_desc";
      }else{
        cls = "sorting_asc";
      }
    }
    return cls;
  };


  $scope.save = function(data, $event, success){
    var form = $($event.currentTarget).parents('form');

    if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
      data.dataProduct = $scope.dataProduct;

      var btn = Ladda.create($event.currentTarget);
      btn.start();
      $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

      $http.post(response.link, data).then(function(resp){
        // add a little bit delay
        setTimeout(function(){
          if(resp.data.status){
            if(response.action == 'create'){
              $scope.data = {};
              window.location.hash = response.back;
            }
            toastr['success']("Data berhasil disimpan!", "Saved");
            $scope.dataProduct = resp.data.dataProduct;
            $scope.refreshSidebarAlert();
            if(typeof success != 'undefined'){
              success();
            }
          }else{
            toastr['error']("Data gagal disimpan!", "Failed");
          }
          btn.stop();
          $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
        }, 1000); // delay 1 sec
      });
    }
  }

  $scope.saveAndReturn = function(data, $event){
    $scope.save(data, $event, function(){
      window.location.hash = response.back;
    });
  }

  $scope.onDelete = function(){
    $scope.data = {};
    $location.url(response.back);
  }

  $scope.getStatus = function(status){

    var workflow = $filter('filter')($scope.optionWorkflow, {
      id: status
    }, true);
    if(workflow.length > 0){
      return workflow[0];
    }

    return [];
  }

  $scope.changeStatus = function(status, $event){
    $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
      $scope.save($scope.data, $event, function(){
        var btn = Ladda.create($event.currentTarget);
        btn.start();

        $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
          setTimeout(function(){
            $scope.$apply(function(){
                $scope.data.status = status;
                $scope.initAccess();
                $scope.dataComment = resp.data.dataComment;
            });
            $scope.refreshSidebarAlert();
            btn.stop();
            swl();
          }, 1000);
        });
      });
    });
  }

  $scope.countTotal = function(product){
    var price = 0;
    var discount = 0;
    var tax = 0;
    var subtotal = 0;

    if(product.status != 2){
      price = product.price * product.qty;

      var lastdisc = price;
      var discount = 0;
      angular.forEach(product.discount, function (value, key) {
        var curdisc = lastdisc * (parseFloat(value) / 100);
        discount += curdisc;
        lastdisc -= curdisc;
      });

      priceAfterDiscount = price - discount;
      tax = priceAfterDiscount * (product.tax_amount / 100);
      subtotal = priceAfterDiscount + tax;
    }

    return {
      price: price,
      discount: discount,
      tax: tax,
      subtotal: subtotal
    }
  }

  $scope.getSubTotal = function(product){
    return $scope.countTotal(product).subtotal;
  }

  $scope.getTotal = function(){

    var totalPrice = 0;
    var totalDiscount = 0;
    var totalTax = 0;
    var totalAll = 0;

    angular.forEach($scope.dataProduct, function(product, key){
      var total = $scope.countTotal(product);
      totalPrice += total.price;
      totalDiscount += total.discount;
      totalTax += total.tax;
      totalAll += total.subtotal;
    });

    return {
      price: totalPrice,
      discount: totalDiscount,
      tax: totalTax,
      total: totalAll
    }
  }

  /* PopUp Barang/Jasa */
  $scope.showPopupProduct = function(dataProduct){

    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'modalProduct.html',
      controller: 'ModalProductCtrl',
      keyboard: false,
      size: 'md',
      resolve: {
        params: function () {
          return {
            data: (dataProduct || {}),
            optionDoorDirection: $scope.response.optionDoorDirection,
            optionDoorInstallation: $scope.response.optionDoorInstallation
          };
        }
      }
    });

    modalInstance.result.then(function (data) {
        console.log(data);
      if(dataProduct){
        $scope.$apply(function(){
            $scope.dataProduct[$scope.dataProduct.indexOf(dataProduct)] = data;
        });
      }else{
        $scope.dataProduct.push(data);
      }
      
      $scope.saveExpectedDate($scope.data.expected_date, data);
    });
  }

  $scope.deleteProduct = function(bom){
    $scope.dataProduct.splice($scope.dataProduct.indexOf(bom), 1);
  }
  /* End PopUp Barang/Jasa */

  $scope.showDiscount = function(discount){
    var dtDiscount = [];
    for(var i in discount){
      if(isNaN(discount[i])){
        continue;
      }
      dtDiscount.push(discount[i]+'%');
    }
    return dtDiscount.join('+');
  }
  
  $scope.showTotalDiscount = function(product) {
      var total = $scope.countTotal(product);
      return total.discount;
  }

  $scope.saveExpectedDate = function ($data, product) {
    var idx = $scope.dataProduct.indexOf(product);
    $scope.dataProduct[idx].expected_date = $data;
  }

  // $scope.taxChanged = function($data, product){
    // var idx = $scope.dataProduct.indexOf(product);
    // if($data == null){
      // $scope.dataProduct[idx].tax = '';
    // }

    // var tax = $filter('filter')($scope.optionTax, {
      // id: $data
    // }, true);
    // if(tax.length > 0 && $data != null && $data != ''){
      // $scope.dataProduct[idx].tax_amount = tax[0].amount;
    // }else{
      // $scope.dataProduct[idx].tax_amount = 0;
    // }
  // }

  $scope.addComment = function(){
    if($scope.comment.trim() == ''){
      $scope.comment = '';
      return;
    }
    $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
      $scope.dataComment = resp.data;
      $scope.comment = '';
    });
  }

  $scope.$on('$viewContentLoaded', function() {
      // initialize core components
      setTimeout(function(){
        App.initAjax();
      }, 1)

      // set default layout mode
      $rootScope.settings.layout.pageContentWhite = true;
      $rootScope.settings.layout.pageBodySolid = false;
      $rootScope.settings.layout.pageSidebarClosed = true;
  });

  $scope.initAccess = function(){

    if($scope.data.status != '1SR' && $scope.response.action != 'create'){
      $scope.response.action = 'view';
    }
  }

  $scope.initAccess();
}]);

/* Product Popup Controller */
angular.module('DevplusApp').controller('ModalProductCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', function($scope,  $uibModalInstance, $filter, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionUom = {};
  $scope.optionDoorDirection = params.optionDoorDirection;
  $scope.optionDoorInstallation = params.optionDoorInstallation;
  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'product', label: 'Product', type: 'select-ajax', attributes: {required: '', maxlength: 100, 'data-live-search': true, 'data-source': '/options/product', 'ng-change': 'changeProduct()', 'id': 'productBom'}},
    {name: 'qty', label: 'Jumlah', type: 'number', attributes: {required: ''}},
    {name: 'uom', label: 'Satuan Ukuran', type: 'select', attributes: {required: '', 'ng-options': 'item.id as item.name for item in optionUom', 'id': 'uomBom'}},
	{name: 'door_height', label: 'Tinggi Pintu', type: 'number', attributes: {required: ''}},
	{name: 'door_height_pic', label: 'Jumlah', type: 'image', attributes: { moveTo : 'img/product-order'}},
	{name: 'door_width', label: 'Lebar Pintu', type: 'number', attributes: {required: ''}},
	{name: 'door_width_pic', label: 'Jumlah', type: 'image', attributes: { moveTo : 'img/product-order'}},
	{name: 'frame_height', label: 'Tinggi Frame', type: 'number', attributes: {required: ''}},
	{name: 'frame_height_pic', label: 'Jumlah', type: 'image', attributes: { moveTo : 'img/product-order'}},
	{name: 'frame_width', label: 'Lebar Frame', type: 'number', attributes: {required: ''}},
	{name: 'frame_width_pic', label: 'Jumlah', type: 'image', attributes: { moveTo : 'img/product-order'}},
	{name: 'door_direction', label: 'Door Direction', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionDoorDirection'}},
	{name: 'door_installation', label: 'Door Installation', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionDoorInstallation'}},
  ];



  $scope.changeProduct = function(){
    console.log($scope.data.product);
    $scope.optionUom = [];
    if(typeof $scope.data.product != 'undefined'){
      $.get('/options/product-uom/'+$scope.data.product).then(function(res){
        $scope.$apply(function(){
            $scope.optionUom = res;
            $scope.data.uom = $scope.optionUom[0].id;
        });

        setTimeout(function(){
            $('.bs-select').selectpicker('refresh');
        }, 100);
      });

    }

  }
  $scope.changeProduct();

  $scope.save = function(){
    $scope.data.name = $('select#productBom option:selected').text();
    $scope.data.uom_name = $('select#uomBom option:selected').text();
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
/* End Product Popup Controller */

