<div class="table-responsive">
  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th ng-repeat="column in response.columns">
          <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
          @{{ column.label }}
          </div>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="dt in response.data">
        <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]" ng-click="select(dt)"></td>
      </tr>
    </tbody>
  </table>
</div>
<p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>
