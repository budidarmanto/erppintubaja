<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class SettingEmailTemplate extends DevplusModel
{
    protected $table = 'setting_email_template';
    protected $fillable = ['id','name', 'template'];

    public static function getOption(){
      $data = SettingEmailTemplate::all();
      return Helper::toOption('id', 'name', $data);
    }
}
