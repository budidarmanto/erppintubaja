<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class CrmContact extends DevplusModel
{

  protected $table = 'crm_contact';
  protected $fillable = ['id', 'company', 'name', 'picture', 'address', 'city', 'postal_code', 'state', 'country', 'website', 'phone', 'mobile', 'fax', 'email', 'tax_no', 'type', 'active', 'pkp', 'revision', 'reference', 'term_of_payment'];

  public function contactDetail()
  {
    return $this->hasMany('App\Models\CrmContactDetail', 'contact');
  }

  public static function getOption($model = null){
    if($model == null){
        $data = CrmContact::where('active', 1)->get();
    }else{
      $data = $model->get();
    }

    return Helper::toOption('id','name',$data);
  }

  public static function getOptionSupplier($model = null){
    if($model == null){
        $data = CrmContact::where('active', 1)->get();
    }else{
      $data = $model->get();
    }

    $opt = [];
    if(count($data) > 0){
      foreach($data as $val){
        $opt[] = [
          'id' => $val['id'],
          'text' => $val['name'],
          'pkp' => $val['pkp']
        ];
      }
    }

    return $opt;
  }

  public static function getOptionCompany(){
    return CrmContact::getOption(CrmContact::where(['active' => 1, 'type' => 'company'])->get());
  }

  public static function getOptionIndividual(){
    return CrmContact::getOption(CrmContact::where(['active' => 1, 'type' => 'individual'])->get());
  }
}
