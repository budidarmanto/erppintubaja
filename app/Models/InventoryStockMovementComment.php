<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use App\Models\SettingUser;
use Helper;
use Auth;
use Carbon\Carbon;

class InventoryStockMovementComment extends DevplusModel
{
  protected $table = 'inventory_stock_movement_comment';
  protected $fillable = ['id', 'stock_movement', 'comment', 'attachment', 'active'];

  public static function getData($stockMovementId){

    $dataComment = InventoryStockMovementComment::select([
      'inventory_stock_movement_comment.id',
      'inventory_stock_movement_comment.comment',
      'inventory_stock_movement_comment.attachment',
      'inventory_stock_movement_comment.created_at',
      'setting_user.username',
      'setting_user.fullname',
      'setting_user.nickname',
      'setting_user.picture',
    ])
    ->join('setting_user', 'setting_user.username', '=', 'inventory_stock_movement_comment.created_by')
    ->where([
      'inventory_stock_movement_comment.stock_movement' => $stockMovementId,
      'inventory_stock_movement_comment.active' => true
    ])->orderBy('inventory_stock_movement_comment.created_at', 'DESC')->get()->toArray();

    $dataComment = collect($dataComment)->map(function($data){
      $data['name'] = (empty($data['nickname'])) ? $data['fullname'] : $data['nickname'];
      $data['date'] = strtotime($data['created_at']);
      $data['created_at'] = Carbon::parse($data['created_at'])->format('d/m/Y H:i');
      $data['color'] = '#ccc';
      return $data;
    });

    return $dataComment;

  }
}
