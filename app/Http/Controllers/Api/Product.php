<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Devplus\Helper\Helper;
use Params;

class Product extends Controller
{
    public function index() {
        $query = ProductProduct::getQuery();
        $query->where('product_master.active', true);
        $dataProduct = $query->get()->toArray();

        $data = ProductProduct::renderProductAttribute($dataProduct);
    
        $xml = '<page>';
        
        if(count($data) > 0){
            foreach($data as $val){
                $image = (empty($val['picture'])) ? '' : Request::root().'/img/product/'.$val['picture'];
                $productName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
                $productCode = htmlspecialchars($val['code'], ENT_XML1, 'UTF-8');
                $url = Request::root().'/api/product-detail/'.$val['id'];
                // dd($val);
                $uom = ProductUom::find($val['uom']);
                $ratio = $uom->ratio;
                $price = $val['sale_price'] / $uom->ratio;

                $xml .= '<product id="'.$val['id'].'" code="'.$productCode.'" barcode="'.$val['barcode'].'" name="'.$productName.'" price="'.$price.'" category_id="'.$val['category'].'" image="'.$image.'" url="'.$url.'" revision="'.$val['revision'].'"/>';
            }
        }
        $xml .= '</page>';

        header('Content-type: text/xml');
        echo $xml;
        exit;
    }

    public function json(){
        $query = ProductMaster::getQuery();
        $dataProduct = $query->get()->toArray();

        $data = ProductMaster::renderProductAttribute($dataProduct);
        $responseArray = [];
        if(count($data) > 0){
            foreach($data as $val){
                $image = (empty($val['picture'])) ? '' : Request::root().'/img/product/'.$val['picture'];
                $productName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
                $productCode = htmlspecialchars($val['code'], ENT_XML1, 'UTF-8');
                $url = Request::root().'/api/product-detail/'.$val['id'];
                // dd($val);
                $uom = ProductUom::find($val['uom']);
                $ratio = $uom->ratio;
                if(empty($ratio) || $ratio == 0){
                  $ratio = 1;
                }
                $price = $val['sale_price'] / $ratio;

                $responseArray[] = [
                    'id' => $val['id'],
                    'code' => $productCode,
                    'barcode' => $val['barcode'],
                    'name' => $productName,
                    'price' => $price,
                    'category_id' => $val['category'],
                    'image' => $image,
                    'url' => $url,
                    'updated_at' => $val['updated_at'],
                    'color' => $val['color'],
                ];
            }
        }
        $responseArrayData =[];
        $responseArrayData['data'] = $responseArray;

        return response()->json($responseArrayData);
    }
  
    public function optproduct(){
        $keyword = Input::get('q');

        $dataProduct = ProductProduct::getData(function($model) use($keyword){
            $model->where('product_master.name', 'ilike', '%'.$keyword.'%')
            ->orWhere('product_product.id', $keyword);
            return $model;
        });
        $opt = Helper::toOption('id', 'name', $dataProduct, 'key', 'label');
        for($i = 0; $i < count($opt); $i++) {
            $opt[$i]['key'] = $opt[$i]['key'] . '|' . $opt[$i]['label'];
        }
        return response()->json($opt);
    }
	
    public function optpDoorDirection(){
        $query = Params::getOption('OPTION_PRODUCT_DIRECTION'); 
        if(count($query) > 0){
            foreach($query as $key => $val){
                $response[] = [
                    'key'	=> $val['id'] . '-'. $val['text'],
                    'label'	=>  $val['text']
                ];
            }
        }	
        return response()->json($response);
    }
	
    public function optpDoorInstallation(){
        $query = Params::getOption('OPTION_PRODUCT_INSTALLATION');
        if(count($query) > 0){
            foreach($query as $key => $val){
                $response[] = [
                    'key' => $val['id'] . '-'. $val['text'],
                    'label' =>  $val['text']
                ];
            }
        }	
        return response()->json($response);
    }
	
    public function optUom(){
        $query = ProductUom::getOptionSingle();
        if(count($query) > 0){
            foreach($query as $key => $val){
                $response[] = [
                    'key' => $val['id'] . '-'. $val['text'],
                    'label' =>  $val['text']
                ];
            }
        }	

        return response()->json($response);
    }
}
