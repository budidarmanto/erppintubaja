/* Setup blank page controller */
angular.module('DevplusApp').controller('FormWorkflowController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    }
    $scope.dataWorkflow = $scope.response.dataWorkflow || [];
    $scope.templateWorkflow = {
       code: '',
       description: '',
       next: [],
       default: '',
       color: '',
       label: ''
    }
    $scope.optionWorkflowNext = [];

    $scope.showPopupWorkflow = function(dataWorkflow){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalWorkflow.html',
        controller: 'ModalWorkflowCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataWorkflow || {}),
              optionWorkflowNext: $scope.optionWorkflowNext
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        var dt = angular.copy($scope.templateWorkflow);
        dt.code = data.code || '';
        dt.description = data.description || '';
        dt.next = data.next || [];
        dt.color = data.color || '';
        dt.default = data.default || false;
        dt.label = data.label || '';

        if(dataWorkflow){
          $scope.dataWorkflow[$scope.dataWorkflow.indexOf(dataWorkflow)] = dt;
        }else{
          $scope.dataWorkflow.push(dt);
        }
        console.log(dt);
        $scope.toggleChecked(dt);
        $scope.renderOptionWorkflowNext();
      });
    }

    $scope.toggleChecked = function(workflow){
      if(workflow.default == false || typeof workflow.default == 'undefined'){
        return;
      }
      var workflowIndex = $scope.dataWorkflow.indexOf(workflow);
      angular.forEach($scope.dataWorkflow, function(value, key){
        if(key == workflowIndex){
          value.default = true;
        }else{
          value.default = false;
        }
      });
    }

    $scope.renderOptionWorkflowNext = function(){
        $scope.optionWorkflowNext = [];
        angular.forEach($scope.dataWorkflow, function(value){
          if(value.code == ''){
            return;
          }
            $scope.optionWorkflowNext.push({
              id: value.code,
              text: value.description
            });
        });

    }
    $scope.renderOptionWorkflowNext();

    $scope.deleteWorkflow = function(workflow){
      $scope.dataWorkflow.splice($scope.dataWorkflow.indexOf(workflow), 1);
      $scope.renderOptionWorkflowNext();
    }

    $scope.getWorkflowNext = function(workflow){
      var str = [];
      if(typeof workflow.next != 'undefined' && workflow.next.length > 0){
        angular.forEach(workflow.next, function(value){
          var dt = $filter('filter')($scope.dataWorkflow, {
            code : value
          }, true);
          if(dt.length > 0){
            str.push(dt[0].description);
          }
        });
      }

      return str.join(', ');
    }

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.workflow = $scope.dataWorkflow;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);


angular.module('DevplusApp').controller('ModalWorkflowCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Tahapan'
  };
  $scope.data = params.data;
  $scope.optionWorkflowNext = params.optionWorkflowNext;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'code', label: 'Kode', type: 'text', attributes: {required: '', maxlength: 100}},
    {name: 'description', label: 'Nama Tahapan', type: 'text', attributes: {required: '', maxlength: 100}},
    {name: 'next', label: 'Tahapan Selanjutnya', type: 'multiple', attributes: {'ng-options': 'item.id as item.text for item in optionWorkflowNext'}},
    {name: 'label', label: 'Status Label', type: 'text'},
    {name: 'color', label: 'Warna', type: 'text', attributes: {'default-position' : 'bottom right', 'class' : 'input-sm form-control colorpicker'}},
    {name: 'default', label: 'Tahapan Utama', type: 'switch'},
  ];

  $scope.save = function(){
    $scope.data.color = $('.colorpicker').val();
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
    $('.colorpicker').colorpicker({
            format: 'hex'
    });
  }, 1);
}]);
