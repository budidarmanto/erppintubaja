<div class="row">
    <div class="col-md-12 page-404">
      <div class="number font-warning"> <i class="icon-lock"></i> </div>
        <div class="details">
            <h3>@lang('system.error.no_access.oops')</h3>
            <p> @lang('system.error.no_access.permission')
                <br/>
                <a href="#"> @lang('system.error.no_access.return') </a> . </p>
        </div>
    </div>
</div>
