<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class SettingWorkflow extends DevplusModel
{
  protected $table = 'setting_workflow';
  protected $fillable = ['id', 'code', 'name', 'workflow'];

  public function getWorkflowDefault(){

    $data = [];
    if(!empty($this->workflow)){
      $workflow = json_decode($this->workflow, 1);
      foreach($workflow as $val){
        if($val['default']){
          $data = $val;
        }
      }
    }
    return $data;
  }

  public static function workflowDefault($code){

    $workflow = SettingWorkflow::where('code', $code)->first();
    if(!empty($workflow)){
      return $workflow->getWorkflowDefault();
    }

    return false;

  }

  public static function getOption($code){

    $option = [];
    $workflow = SettingWorkflow::where('code', $code)->first();
    if(!empty($workflow)){
      $listWorkflow = json_decode($workflow->workflow, true);
      if(!empty($listWorkflow)){
        foreach($listWorkflow as $val){
          $next = [];
          if(!empty($val['next'])){
            foreach($val['next'] as $nval){
              $dataNext = collect($listWorkflow)->filter(function($n) use ($nval){
                return ($n['code'] == $nval);
              })->first();
              if(!empty($dataNext)){
                // dd($dataNext);
                $next[] = [
                  'id' => $dataNext['code'],
                  'text' => $dataNext['description'],
                  'color' => $dataNext['color'],
                  'label' => (isset($dataNext['label'])) ? $dataNext['label'] : $dataNext['description']
                ];
              }
            }
          }
          $option[] = [
            'id' => $val['code'],
            'text' => $val['description'],
            'color' => $val['color'],
            'next' => $next,
            'label' => (isset($val['label'])) ? $val['label'] : $val['description']
          ];
        }
      }
    }

    return $option;
  }
}
