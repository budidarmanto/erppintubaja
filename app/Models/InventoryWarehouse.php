<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class InventoryWarehouse extends DevplusModel
{
  protected $table = 'inventory_warehouse';
  protected $fillable = ['id', 'code', 'name', 'address', 'city', 'postal_code', 'state', 'country', 'phone', 'fax', 'company', 'active'];

  public static function getOption($model = null){
    $company = Helper::currentCompany();
    $dataWarehouse = [];
    if($model == null){
        $data = InventoryWarehouse::orderBy('name')->get();
    }else{
      $data = $model->orderBy('name')->get();
    }

    foreach($data as $val){
      $listCompany = json_decode($val->company, true);
      if(!empty($listCompany) && in_array($company, $listCompany)){
        $dataWarehouse[] = $val;
      }
    }

    return Helper::toOption('id', 'name', $dataWarehouse);
  }

  public static function getWarehouseCompany($company){
    $data = InventoryWarehouse::orderBy('name')->get();
    $dataWarehouse = [];
    foreach($data as $val){
      $listCompany = json_decode($val->company, true);
      if(!empty($listCompany) && in_array($company, $listCompany)){
        $dataWarehouse[] = $val;
      }
    }

    return $dataWarehouse;
  }
}
