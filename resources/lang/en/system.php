<?php

return [

  'datagrid' => [
    'filter' => [
      'search' => 'Search',
    ],
    'header' => [
      'create' => 'Create',
    ],
    'massButton' => [
      'withSelected' => 'With Selected',
      'delete' => 'Delete',
    ],
    'status' => [
      'showing' => 'Showing',
      'to' => 'to',
      'from' => 'from',
      'entries' => 'entries',
      'perpage' => 'Perpage'
    ],
    'table' => [
      'action' => 'Action',
      'modify' => 'Modify',
      'delete' => 'Delete',
      'no_record' => 'No Record'
    ]
  ],
  'form' => [
    'action' => [
      'discard' => 'Discard',
      'back' => 'Back',
      'delete' => 'Delete',
      'savereturn' => 'Save and Return',
      'save' => 'Save'
    ]
  ],
  'sidebar' => [
    'app' => [
      'name' => 'System Configuration',
      'group' => 'Group Autorization',
      'application' => 'Applications',
      'role' => 'Roles',
      'config' => 'Parameter Settings'
    ],
    'crm' => [
      'name' => 'CRM',
      'contact' => 'Contacts'
    ],
    'simhu' => [
      'name' => 'SIMHU',
      'master' => [
        'name' => 'Master',
        'jenis_pekerjaan' => 'Job Type',
        'project' => 'Project',
        'name' => 'Master',
        'uraian' => 'Analysis',
        'category' => 'Category',
        'header' => 'Header',
      ],
      'report' => 'Report',
    ],
    'setting' => [
      'name' => 'Settings',
      'user' => 'Users',
      'company' => 'Company',
      'period' =>'Period',
      'workflow' =>'Workflow'
    ],
    'accounting' => [
      'name' => 'Accounting',
      'period' => 'Period',
      'coa' => 'Chart of Account',
      'journal' =>'Journal',
      'report' =>'Report Structure',
      'laporan' => 'Report',
      'management' => 'Management Report',
      'custom' => 'Custom Report',
      'daily' => 'Daily Journal',
      'trialbalance' => 'Trial Balance',
      'generalledger' => 'General Ledger'
    ]
  ],
  'error' => [
    '500' => [
      'title' => '500',
      'opps' => 'Oops! Something went wrong.',
      'comeback' => 'We are fixing it! Please come back in a while.'
    ],
    'no_access' => [
      'oops' => 'Oops! No Access.',
      'permission' => "You don't have permission to access this page!",
      'return' => 'Return home'
    ],
    '404' => [
      'oops' => "Oops! You're lost.",
      'find' => "We can not find the page you're looking for.",
      'return' => 'Return home'
    ]
  ]



];
