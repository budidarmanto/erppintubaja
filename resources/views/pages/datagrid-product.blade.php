<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            @include('pages.partials.datagrid.datagrid-header')
            <div class="portlet-body">
              <div class="row  margin-bottom-10">
                <div class="col-md-6 text-right col-md-offset-6">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-12">
                  <div class="table-filter row">
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm right">
                        <i class="fa fa-search"></i>
                        <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                      </div>
                    </div>
                    <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
                        <select ng-model="category" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionCategory" ng-change="categoryChanged()">
                          <option value="">-- Semua Kategori --</option>
                        </select>
                    </div>
                    <div class="col-md-2" style="padding-left: 2;">
                        <select ng-model="type" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionType" ng-change="typeChanged()">
                          <option value="">-- Semua Tipe --</option>
                        </select>
                    </div>
                  </div>
                </div>
              </div>
              @include('pages.partials.datagrid.datagrid-table')
              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    @include('pages.partials.datagrid.datagrid-mass-button')
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
