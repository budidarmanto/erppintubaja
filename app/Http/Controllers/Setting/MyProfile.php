<?php

namespace App\Http\Controllers\Setting;

use App\Models\SettingUser;
use App\Models\SettingCompany;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\AppGroup;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Helper;
use Carbon\Carbon;
use Auth;

class MyProfile extends Controller
{

  public function index() {

    $settingUser = new SettingUser();
    $select = $settingUser->getFillable();
    unset($select[array_search('password', $select)]);

    $form = $this->anyForm(SettingUser::select($select)->find(Auth::user()->id));
    $form->pre(function($input){

      $data = [
        'username' => $input['username'],
        'fullname' => $input['fullname'],
        'nickname' => $input['nickname'],
        'picture' => $input['picture'],
      ];

      return $data;
    });

    if($form->hasRequest()){
      $validateUsername = SettingUser::where('username', Input::get('username'))->where('id', '!=', $form->model->id)->first();

      if(!empty($validateUsername)){
        return response()->json([
          'status' => false,
          'messages' => 'Username sudah digunakan!'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionCompany'] = SettingCompany::getOption();
    $dataForm['optionGroup'] = AppGroup::getOption();
    $dataForm['data']['group'] = json_decode($dataForm['data']['group']);
    $dataForm['data']['company'] = json_decode($dataForm['data']['company']);
    $dataForm['action'] = 'modify';
    return response()->json($dataForm);
  }

  public function changePassword(){
    $input = Input::all();
    $oldPassword = $input['old_password'];
    $newPassword = $input['new_password'];

    $user = SettingUser::find(Auth::user()->id);

    if (Hash::check($oldPassword, $user->password) && !empty($input['new_password'])) {

      SettingUser::where('id', $user->id)->update([
        'password' => Hash::make($input['new_password'])
      ]);

      return response()->json([
        'status' => true,
        'messages' => 'Password berhasil diubah!'
      ]);

    }else{

      return response()->json([
        'status' => false,
        'messages' => 'Password yang anda masukan salah!'
      ]);

    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Profil Saya');
    $form->add('username', __('app.setting.user.title'), 'text')->rule('required|max:25');
    $form->add('fullname', __('app.setting.user.fullname'), 'text')->rule('required|max:50');
    $form->add('nickname', __('app.setting.user.nickname'), 'text')->rule('max:50');

    $form->add('group', __('app.setting.user.group'), 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionGroup',
      'disabled' => true
    ]);
    $form->add('company', __('app.setting.user.company'), 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCompany',
      'disabled' => true
    ]);
    $form->add('email', __('app.setting.user.email'), 'email')->rule('required|email|max:255')->attributes([
      'readonly' => true
    ]);

    $form->add('picture', __('app.setting.user.picture'), 'image')->attributes([
      'moveTo' => 'img/user'
    ]);

    return $form;
  }
}
