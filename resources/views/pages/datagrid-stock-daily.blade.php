<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                  <a href="@{{ response.link+'?'+( (queryString == '') ? 'export=1' : queryString+'&export=1' ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
                </div>
            </div>
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6" style="padding-left: 2;">
                    Gudang : <select ng-model="warehouse" class="input-sm" ng-options="item.id as item.text for item in response.optionWarehouse" ng-change="warehouseChanged()" ng-init="warehouse = response.optionWarehouse[0].id" style="width: 200px;">
                      <!-- <option value="">-- Semua Tipe --</option> -->
                    </select>
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-12">
                  <div class="table-filter row">
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm right">
                        <i class="fa fa-search"></i>
                        <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                      </div>
                    </div>
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm left">
                        <i class="fa fa-calendar right"></i>
                        <input type="text" ng-model="date" class="form-control input-sm date-picker" placeholder="Tanggal" ng-change="dateChanged($event)" id="dateStockDaily">
                      </div>
                    </div>
                    <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
                        <select ng-model="category" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionCategory" ng-change="categoryChanged()">
                          <option value="">-- Semua Kategori --</option>
                        </select>
                    </div>
                    <div class="col-md-2" style="padding-left: 2;">
                        <select ng-model="type" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionType" ng-change="typeChanged()">
                          <option value="">-- Semua Tipe --</option>
                        </select>
                    </div>

                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table table-hover dataTable table-bordered" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th rowspan="2" style="vertical-align: middle;">Kode Produk</th>
                      <th rowspan="2" style="vertical-align: middle;">Nama Produk</th>
                      <th rowspan="2" style="vertical-align: middle; width: 100px;">Stok Awal</th>
                      <th colspan="2" style="text-align: center;">Barang Masuk</th>
                      <th colspan="2" style="text-align: center;">Barang Keluar</th>
                      <th rowspan="2" style="vertical-align: middle; width: 100px;">Stok Akhir</th>
                      <th rowspan="2" style="vertical-align: middle; width: 100px;">Satuan Ukuran</th>
                    </tr>
                    <tr>
                      <th style="width: 100px; text-align: center;">Penerimaan</th>
                      <th style="width: 100px; text-align: center;">Penyesuaian</th>
                      <th style="width: 100px; text-align: center;">Pengiriman</th>
                      <th style="width: 100px; text-align: center;">Penjualan Kasir</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="dt in response.data">
                      <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td>
                      <td style="text-align: right;">@{{ dt['begin_qty'] | number:2 }}</td>
                      <td style="text-align: right;">@{{ dt['total_receipt'] | number:2 }}</td>
                      <td style="text-align: right;">@{{ dt['total_adjustment'] | number:2 }}</td>
                      <td style="text-align: right;">@{{ dt['total_delivery'] | number:2 }}</td>
                      <td style="text-align: right;">@{{ dt['total_chasier'] | number:2 }}</td>
                      <td style="text-align: right;">@{{ dt['end_qty'] | number:2 }}</td>
                      <td>@{{ dt['uom_name'] }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>

              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    @include('pages.partials.datagrid.datagrid-mass-button')
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
