<?php

namespace Devplus\Helper;
use Auth;

class Helper{

  public static function currentCompany(){
    return Auth::user()->authCompany()->id;
  }

  public static function toFlatOption($id, $text, $data){
    $options = [];
    if(count($data) > 0){
      foreach($data as $val){
        $options[$val[$id]] = $val[$text];
      }
    }

    return $options;
  }

  public static function toOption($id, $text, $data, $paramId = 'id', $paramText = 'text', $modifierFn = null){
    $option = [];
    foreach($data as $val){
      if(isset($val[$id]) && isset($val[$text])){
        $data = [];
        $data[$paramId] = $val[$id];
        $data[$paramText] = $val[$text];

        if (is_a($modifierFn, '\Closure')) {
          $data[$paramText] = call_user_func_array($modifierFn, [$val]);
        }

        $option[] = $data;
      }
    }

    return $option;
  }

  public static function responseNoAccess(){
    return response()->json([
      'status' => 'noAccess'
    ]);
  }

  public static function generateId($query = null, $field = '', $prefix = '', $len = 4, $multiple = 1) {

      if($query == null){
        return;
      }

      $qfield = $field;
      $qfield = $qfield.'::text';
      $qwhere = "$qfield LIKE '" . $prefix . "%'";

      $query = $query->whereRaw($qwhere)->first();

      if (!empty($query)) {
          $id = preg_replace('/' . $prefix . '/', '', $query->{$field});
          $id = (int) $id;
          $id += $multiple;

          $id = (string) $id;

          $id = str_pad($id, $len,"0",STR_PAD_LEFT);

          return $prefix . $id;
      } else {
          $id = 0;
          $id += $multiple;
          $id = (string) $id;

          while (strlen($id) <= ($len - 1)) {
              $id = '0' . $id;
          }
          return $prefix . $id;
      }
    }

    public static function splitCamelCase($camelCaseString) {
        $re = '/(?<=[a-z])(?=[A-Z])/x';
        $a = preg_split($re, $camelCaseString);
        return $a;
    }

    public static function displayTable($data = []){

      $column = array_keys($data[0]);

      $table = '<style>table tr td, table tr th{padding: 5px; border: 1px solid #ccc;}</style><table style="font-size: 12px;" border="1" cellspacing="0" cellpadding="0">';
      $table .= '<tr>';
      foreach($column as $col){
        $table .= '<th>'.$col.'</th>';
      }
      $table .= '</tr>';

      foreach($data as $val){
        $table .= '<tr>';
        $dataVal = array_values($val);
        foreach($dataVal as $vval){
            $table .= '<td>'.$vval.'</td>';
        }
        $table .= '</tr>';
      }

      $table .= '</table>';
      echo $table;
      exit();

    }
}
