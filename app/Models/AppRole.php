<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class AppRole extends DevplusModel
{
    protected $table = 'app_role';
    protected $fillable = ['id', 'code', 'name'];


    public static function getOption(){
      $data = AppRole::all();
      return Helper::toOption('code','name',$data);
    }
}
