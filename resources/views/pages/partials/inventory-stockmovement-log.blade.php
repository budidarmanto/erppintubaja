<div class="portlet light">
  <div class="portlet-title">
    <div class="caption">Log Status</div>
  </div>
  <div class="portlet-body">
    <div class="timeline timeline-log">
      <div class="timeline-item" ng-repeat="log in dataLog">
        <div class="timeline-badge">
          <div class="timeline-badge-userpic" style="border-color: @{{ log.color }};">
            <img src="@{{ (log.picture != '' && log.picture != null) ? 'img/user/'+log.picture : 'img/user-placeholder.jpg' }}"/>
          </div>
        </div>
        <div class="timeline-body" style="border:1px solid @{{ log.color }}">
          <div class="timeline-body-arrow" style="
border-color: transparent @{{ log.color }} transparent transparent;
"></div>
          <div class="timeline-body-head">
            <div class="timeline-body-head-caption">
              <a href="javascript:;" class="timeline-body-title" style="color: @{{ log.color }}">@{{ log.status_label }}</a> - <span class="font-grey-cascade">@{{ log.name }}</span>
              <span class="timeline-body-time font-grey-cascade">@{{ log.created_at }}</span>
            </div>
          </div>
          <div class="timeline-body-content">
            <span class="font-grey-cascade" compile-template ng-bind-html="log.description"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
