<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Auth;

use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;

class InventoryStockMovement extends DevplusModel
{
    protected $table = 'inventory_stock_movement';
    protected $fillable = ['id', 'code', 'company', 'contact', 'pos', 'description', 'source', 'destination', 'company_destination', 'expected_date', 'reference', 'type', 'status', 'currency', 'currency_rate', 'printed', 'category', 'date',
     'total', 'created_by', 'total_discount', 'total_price', 'total_tax', 'cashier_session', 'cashier_transaction_code', 'cashier_reference', 'cashier_reference_no', 'cashier_payment_type', 'cashier_bank', 'cashier_edc', 'cashier_surcharge', 'cashier_customer_email', 'cashier_card_number','shipment_address'];

    public static function generateId($type = '', $company = null){

        $companyCode = '';
      if($company != null){
        $company = SettingCompany::find($company);
        if(!empty($company)){
          $companyId = $company->id;
          $companyCode = $company->code;
        }
      }else{
        $companyId = Auth::user()->authCompany()->id;
        $companyCode = Auth::user()->authCompany()->code;
      }

      $prefix = $companyCode.'.'.$type.date('ym');
      $query = InventoryStockMovement::where('type', $type)->where('company', $companyId)->orderBy('code', 'DESC');
      return Helper::generateId($query, 'code', $prefix);

    }

    public static function generateIdCashier($type = '', $company = null){

      if($company != null){
        $company = SettingCompany::find($company);
        if(!empty($company)){
          $companyId = $company->id;
          $companyCode = $company->code;
        }
      }else{
        $companyId = Auth::user()->authCompany()->id;
        $companyCode = Auth::user()->authCompany()->code;
      }

      $prefix = $companyCode.'.'.$type.'.'.date('ymd');
      $query = InventoryStockMovement::where('type', $type)->where('company', $companyId)->orderBy('code', 'DESC');
      return Helper::generateId($query, 'code', $prefix, 5);

    }

    public static function refreshTotal($stockMovementId){
      $stockMovementProduct = InventoryStockMovementProduct::where('stock_movement', $stockMovementId)->get();

      $totalPrice = 0;
      $totalDiscount = 0;
      $totalTax = 0;
      $totalAll = 0;

      if(count($stockMovementProduct) > 0){
        foreach($stockMovementProduct as $val){
          if($val->status != 2){
            $price = $val->price * $val->qty;

            $lastDisc = $price;
            $discount = 0;
            $dataDiscount = json_decode($val['discount'], true);
            if(count($dataDiscount) > 0){
              foreach($dataDiscount as $vdisc){
                $currDisc = $lastDisc * (floatval($vdisc) / 100);
                $discount += $currDisc;
                $lastDisc -= $currDisc;
              }
            }

            $priceAfterDiscount = $price - $discount;
            $tax = $priceAfterDiscount * ($val['tax_amount'] / 100);
            $subtotal = $priceAfterDiscount + $tax;

            $totalPrice += $price;
            $totalDiscount += $discount;
            $totalTax += $tax;
            $totalAll += $subtotal;
          }
        }
      }

      InventoryStockMovement::where('id', $stockMovementId)->update([
        'total' => $totalAll,
        'total_discount' => $totalDiscount,
        'total_price' => $totalPrice,
        'total_tax' => $totalAll,
      ]);
    }

    // only called in getDataStock()
    // Get warehouse id depends on transaction type
    public static function getWarehouseTransaction($data, $dataWarehouseLocation = null){

      if(empty($dataWarehouseLocation)){
        $dataWarehouseLocation = InventoryWarehouseLocation::getOption();
      }

      $type = $data['type'];

      $wh_id = null;

      switch($type){
        case 'PO' :
        case 'TF' : $wh_id = $data['destination'];
                    break;
        case 'SO' :
        case 'ET' :
        case 'RT' :
                    $wh_id = $data['source'];
                    break;
        case 'RC' : $wh_loc_id = $data['destination'];
                    $wh_id = collect($dataWarehouseLocation)->filter(function($value) use ($wh_loc_id){
                      return $value['id'] == $wh_loc_id;
                    })->first();
                    if(!empty($wh_id)){
                      $wh_id = $wh_id['warehouse'];
                    }
                    break;
        case 'DO' :
        case 'AD' :
        case 'IT' :
        case 'CH' :
                    $wh_loc_id = $data['source'];
                    $wh_id = collect($dataWarehouseLocation)->filter(function($value) use ($wh_loc_id){
                      return $value['id'] == $wh_loc_id;
                    })->first();
                    if(!empty($wh_id)){
                      $wh_id = $wh_id['warehouse'];
                    }
                    break;
        default   : $wh_id = null;
                    break;
      }

      return $wh_id;
    }


}
