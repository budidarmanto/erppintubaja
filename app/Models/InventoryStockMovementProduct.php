<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Carbon\Carbon;
use App\Models\ProductProduct;
use App\Models\CrmContact;

class InventoryStockMovementProduct extends DevplusModel
{
    protected $table = 'inventory_stock_movement_product';
    protected $fillable = ['id', 'stock_movement', 'reference', 'product', 'uom', 'qty', 'rcp_qty', 'do_qty', 'price', 'discount', 
	                      'tax', 'tax_amount', 'total', 'contact', 'status', 'expected_date','door_height','door_height_pic','door_width',
						  'door_width_pic','frame_height','frame_height_pic','frame_width','frame_width_pic','door_direction','door_installation'];

    public static function getDataProduct($stockMovementId){
      $stockMoveProduct = InventoryStockMovementProduct::where('stock_movement', $stockMovementId)->get()->toArray();

      $listProductId = collect($stockMoveProduct)->pluck('product')->unique()->toArray();
      $data = [];
      if(!empty($listProductId)){
        $dataProduct = ProductProduct::getData(function($query) use ($listProductId) {
          $query->whereIn('product_product.id', $listProductId);
          return $query;
        });
        $dataProduct = ProductProduct::getRelatedData($dataProduct);

        foreach($stockMoveProduct as $key => $val){

          $product = collect($dataProduct)->filter(function($dt) use($val){
            return ($dt['id'] == $val['product']);
          })->first();
		
          if(!empty($product)){

            // Search Supplier
            $supplier = collect($product['optionSupplier'])->where('id', $val['contact'])->first();
            $supplierName = '';
            if(!empty($supplier)){
              $supplierName = $supplier['text'];
            }else{
              $supplier = CrmContact::find($val['contact']);
              if(!empty($supplier)){
                  $supplierName = $supplier->name;
                  $product['optionSupplier'][] = [
                    'id' => $supplier->id,
                    'text' => $supplier->name,
                    'price' => 0,
                  ];
              }
            }

            // Search Uom
            $uomName = '';
            $uom = collect($product['optionUom'])->where('id', $val['uom'])->first();
            if(!empty($uom)){
              $uomName = $uom['text'];
            }


            $data[] = [
              'id' => $val['id'],
              'product' => $val['product'],
              'name' => $product['name'],
              'qty' => $val['qty'],
              'rcp_qty' => $val['rcp_qty'],
              'do_qty' => $val['do_qty'],
              'uom' => $val['uom'],
              'uom_name' => $uomName,
              'reference' => $val['reference'],
              'supplier' => $val['contact'],
              'supplier_name' => $supplierName,
              'expected_date' => Carbon::parse($val['expected_date'])->format('Y-m-d'),
              'price' => $val['price'],
              'discount' => json_decode($val['discount'], true),
              'status' => $val['status'],
              'tax' => $val['tax'],
              'tax_amount' => $val['tax_amount'],
              'optionSupplier' => $product['optionSupplier'],
              'optionUom' => $product['optionUom'],
              'alternative' => $product['alternative'],
			  'door_height'=> $val['door_height'],
			  'door_height_pic'=> $val['door_height_pic'],
			  'door_width'=> $val['door_width'],
			  'door_width_pic'=> $val['door_width_pic'],
			  'frame_height'=> $val['frame_height'],
			  'frame_height_pic'=> $val['frame_height_pic'],
			  'frame_width'=> $val['frame_width'],
			  'frame_width_pic'=> $val['frame_width_pic'],
			  'door_direction'=> $val['door_direction'],
			  'door_installation'=> $val['door_installation'],			  
			  
			  
            ];
          }
        }

      }

      return $data;
    }
}
