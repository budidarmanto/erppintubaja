<?php

namespace Devplus\FormBuilder;

class Fields{

  public $label;
  public $type;
  public $source;
  public $rule;
  public $help;
  public $attributes = [];

  public function __construct($source, $label, $type){
    $this->source = $source;
    $this->type = $type;
    $this->label = $label;

    return $this;
  }

  public function rule($rule){
    $this->rule = $rule;

    // convert rule to attributes for javascript validation
    $validation = [];
    $arrRule = explode('|', $this->rule);

    foreach($arrRule as $val){
      $arrVal = explode(':', $val);
      $param = $arrVal[0];
      $value = (isset($arrVal[1])) ? $arrVal[1] : '';

      if($this->type != 'number' && $param == 'min'){
        $param = 'minlength';
      }else if($this->type != 'number' && $param == 'max'){
        $param = 'maxlength';
      }else if($param == 'same'){
        $param = 'equalTo';
        $value = "input[name='".$value."']";
      }else if($param == 'numeric'){
        $param = 'number';
      }

      $validation[$param] = $value;
    }
    $this->attributes($validation);

    return $this;
  }

  public function help($help){
    $this->help = $help;
    return $this;
  }

  public function attributes($attributes){
    if(isset($attributes['class']) && $attributes['class'] != ''){
      $attributes['class'] = $this->attributes['class'].' '.$attributes['class'];
    }

    $this->attributes = array_merge($this->attributes, $attributes);
    return $this;
  }

  public function render(){
    return [
      'name' => $this->source,
      'label' => $this->label,
      'type' => $this->type,
      'attributes' => $this->attributes,
      'help' => $this->help,
    ];
  }

}
