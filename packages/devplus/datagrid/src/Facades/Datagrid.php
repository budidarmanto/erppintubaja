<?php

namespace Devplus\Datagrid\Facades;

use Illuminate\Support\Facades\Facade;

class Datagrid extends Facade{

  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor()
  {
      return 'datagrid';
  }

}
