<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockMovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stock_movement', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('code', 25);
          $table->string('company', 25);
          $table->string('contact', 25)->nullable();
          $table->string('pos', 25)->nullable();
          $table->text('description')->nullable();
          $table->string('source', 25)->nullable();
          $table->string('destination', 25)->nullable();
          $table->string('company_destination', 25)->nullable();
          $table->timestamp('expected_date')->nullable();
          $table->string('reference', 100)->nullable();
          $table->string('type', 5)->nullable();
          $table->string('status', 10)->nullable();
          $table->string('currency', 25)->nullable();
          $table->decimal('currency_rate', 18, 2)->nullable();
          $table->integer('printed')->default(0);
          $table->string('category', 25)->nullable();
          $table->timestamp('date');
          $table->decimal('total_discount',18,2)->default(0);
          $table->decimal('total_price',18,2)->default(0);
          $table->decimal('total_tax',18,2)->default(0);
          $table->decimal('total',18,2)->default(0);
          $table->string('cashier_transaction_code', 100)->nullable();
          $table->string('cashier_session', 100)->nullable();
          $table->string('cashier_reference', 25)->nullable();
          $table->string('cashier_reference_no', 25)->nullable();
          $table->string('cashier_payment_type', 25)->nullable();
          $table->string('cashier_bank', 25)->nullable();
          $table->string('cashier_edc', 25)->nullable();
          $table->string('cashier_card_number', 25)->nullable();
          $table->decimal('cashier_surcharge', 18, 2)->default(0)->nullable();
          $table->string('cashier_customer_email', 25)->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');
          $table->unique('code');

          $table->foreign('company')
                  ->references('id')->on('setting_company')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stock_movement');
    }
}
