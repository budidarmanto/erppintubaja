<?php

namespace Devplus\Datagrid;
use Carbon\Carbon;
use Input;
use Request;

class Columns{

  public $source;
  public $label;
  public $sortable;
  public $sorting;
  public $renderFn;
  public $options = [];
  public $dateFormat = '';
  public $numberFormat = false;

  public function __construct($source, $label, $sortable = true){
    $this->source = $source;
    $this->label = $label;
    $this->sortable = $sortable;

    $sort = Input::get('sort');
    $sorting = '';
    $param = $source;

    if($sort == $source || $sort == '-'.$source){
      $sorting = 'asc';
      if($sort == '-'.$source){
        $sorting = 'desc';
      }
      $this->sorting = $sorting;
    }

    return $this;
  }

  public function render($renderFn){
    $this->renderFn = $renderFn;
    return $this;
  }

  public function options($options){
    $this->options = $options;
    return $this;
  }

  public function date($format = ''){
    if(!empty($format)){
      $this->dateFormat = $format;
    }else{
      $this->dateFormat = 'd/m/Y';
    }
    return $this;
  }

  public function number($decimals = 2){
    $this->numberFormat = $decimals;
    return $this;
  }

  public function build($data = []){

    $arrData = [];
    if(count($data) > 0){
      foreach($data as $valData){

      if (is_a($this->renderFn, '\Closure') /*&& isset($valData[$this->source]) */) {
          $valData[$this->source] = call_user_func_array($this->renderFn, [$valData]);
        }
        if(count($this->options) > 0){
          $optionKey = $valData[$this->source];
          if(isset($this->options[$optionKey])){
            $valData[$this->source] = $this->options[$optionKey];
          }
        }
        if(!empty($this->dateFormat)){
          $valData[$this->source] = Carbon::parse($valData[$this->source])->format($this->dateFormat);
        }

        if($this->numberFormat !== false){
          $valData[$this->source] = number_format($valData[$this->source], $this->numberFormat);
        }

        $arrData[] = $valData;
      }
    }

    $column =  [
      'source' => $this->source,
      'label' => $this->label,
      'sortable' => $this->sortable,
      'sorting' => $this->sorting,
    ];

    return [$column, $arrData];

  }

}
