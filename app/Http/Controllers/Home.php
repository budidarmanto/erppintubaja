<?php

namespace App\Http\Controllers;

use App\Models\SettingUser;
use App\Models\SettingCompany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use Session;

class Home extends Controller {

    public function index() {
      
      $theme = env('APP_THEME', 'squirrel');
      return view('layout', compact('theme'));
    }

    public function login() {
      $theme = env('APP_THEME', 'squirrel');
        return view('auth.login', compact('theme'));
    }

    public function dologin() {
        $input = Input::all();

        $rules = [
            'username' => 'required',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $rules);
        // If Validation Fails
        if ($validator->fails()) {

            return redirect()->route('AuthLogin')->withErrors("Username atau password salah!")->withInput(Input::except('password'));
        } else {
            $user = SettingUser::where('username', $input['username'])->orWhere('email', $input['username'])->first();

            if ($user) {

              if($user->active != 1){
                return redirect()->route('AuthLogin')->withErrors("User tidak aktif");
              }

                if (Hash::check($input['password'], $user->password)) {

                    $session = [
                        'username' => $user->username,
                        'password' => $input['password']
                    ];

                    $remember = true;
                    if (Auth::attempt($session, true)) {

                        return redirect()->intended('/');
                    } else {
                        // no action
                        //dd("failed attempt");
                    }
                } else {
                    // no action
                }
            }

            return redirect()->route('AuthLogin')->withErrors("Username atau password salah!");
        }
    }

    public function forgotPassword() {
      $theme = env('APP_THEME', 'squirrel');
        return view('auth.forgot-password', compact('theme'));
    }

    public function doForgotPassword() {
        $email = Input::get('email');

        $user = SettingUser::where('email', $email)->first();

        if ($user) {

            $resetPasswordLink = route('AuthReset') . '?token=' . Crypt::encrypt($user->username . '|' . time());
            $content = view('mail.forgot-password', ['link' => $resetPasswordLink, 'user' => $user])->render();
            return response($content);
            Mail::send('mail.forgot-password', ['link' => $resetPasswordLink, 'user' => $user], function($message) use ($user) {
                $message->to($user->email, $user->username)->subject('Konfirmasi perubahan password');
            });

            return redirect()->route('AuthForgotSuccess');
        } else {
            return redirect()->route('AuthForgot')->withErrors("Email yang anda masukan tidak terdaftar!");
        }
    }

    public function forgotPasswordSuccess() {
      $theme = env('APP_THEME', 'squirrel');
        return view('auth.forgot-success', compact('theme'));
    }

    public function resetPassword() {

        $token = Input::get('token');

        try {
          $decrypt = decrypt($token);
        } catch (\Exception $e) {
          return response("Invalid Token");
        }

        list($username, $time) = explode('|', $decrypt);
        $nexttime = mktime(date('H', $time), date('i', $time) + 30, date('s', $time), date('n', $time), date('j', $time), date('Y', $time));

        if ($nexttime >= time()) {
            $view = View::make('auth.reset-password')->render();
            return $view;
        } else {
            return response("Invalid Token");
        }

    }

    public function doResetPassword() {
        $token = Input::get('token');
        try {
          $decrypt = decrypt($token);
        } catch (\Exception $e) {
          return response("Invalid Token");
        }
        list($username, $time) = explode('|', $decrypt);

        $input = Input::all();
        $rules = [
            'new_password' => 'required|min:6',
            'new_password_confirm' => 'required|min:6|same:new_password',
        ];
        $validator = Validator::make($input, $rules);
        $password = Input::get('new_password');
        // If Validation Fails
        if ($validator->fails()) {
            if(strlen($password) < 6){
              $msg = 'Password minimal 6 karakter!';
            }else{
              $msg = 'Password tidak sama!';
            }
            return redirect()->back()->withErrors($msg);
        } else {
            if (!empty($password)) {
                $encrypt_password = \Illuminate\Support\Facades\Hash::make($password);
                $user = SettingUser::where('username', $username)->update([
                    'password' => $encrypt_password,
                ]);
                if($user){
                    return redirect()->route('AuthLogin')->with('success', 'Password anda berhasil diubah.');
                }
            } else {

            }
        }
    }

    public function logout() {

        Auth::logout();
        Session::forget('company');
        return redirect()->route('AuthLogin');
    }
}
