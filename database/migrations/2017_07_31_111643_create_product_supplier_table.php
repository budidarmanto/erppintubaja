<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_supplier', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('product', 25);
            $table->string('supplier', 25);
            $table->decimal('price', 18, 2)->default(0);
            $table->string('tax', 25)->nullable();
            $table->decimal('discount',18,2)->default(0);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique(['product', 'supplier']);

            $table->foreign('product')
                  ->references('id')->on('product_master')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('supplier')
                  ->references('id')->on('crm_contact')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_supplier');
    }
}
