<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmContactDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('crm_contact_detail', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('contact', 25);
          $table->char('type', 2);
          $table->string('name', 50);
          $table->string('position', 50)->nullable();
          $table->text('address')->nullable();
          $table->string('city', 50)->nullable();
          $table->string('postal_code', 5)->nullable();
          $table->string('state', 50)->nullable();
          $table->string('country', 50)->nullable();
          $table->string('website', 255)->nullable();
          $table->string('phone', 50)->nullable();
          $table->string('mobile', 50)->nullable();
          $table->string('fax', 50)->nullable();
          $table->string('email', 255)->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('contact')
                ->references('id')->on('crm_contact')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_contact_detail');
    }
}
